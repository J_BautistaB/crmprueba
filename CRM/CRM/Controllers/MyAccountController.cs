﻿using CRM.Entity;
using CRM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;

namespace CRM.Controllers
{
    public class MyAccountController : Controller
    {
        //
        // GET: /MyAccount/

        public ActionResult Login()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Login l, string ReturnUrl="" )
        {
            using (CRMEntities b = new CRMEntities())
            {
                var user = b.USERS.Where(a => a.USERNAME.Equals(l.Username) && a.USER_PASSWORD.Equals(l.Password)).FirstOrDefault();
                if (user != null && user.ESTADO_USER.Equals(true)) {
                    FormsAuthentication.SetAuthCookie(user.USERNAME, l.RememberMe);
                    if (Url.IsLocalUrl(ReturnUrl))
                    {
                        return Redirect(ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
            }
            ModelState.Remove("Username");
            ModelState.Remove("Password");
            return View();
        }

        public ActionResult Logout() {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "MyAccount");
        }
    }
}
