﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Areas.Ventas.Models
{
    public class ProductoModel
    {
        public int idproducto { get; set; }
        public int idCatalogo { get; set; }
        public string catalogo { get; set; }
        public string codigo { get; set; }
        public string nombre { get; set; }
        public string modelo { get; set; }
        public string categoria { get; set; }
        public string subcategoria1 { get; set; }
        public string subcategoria2 { get; set; }
        public string marca { get; set; }
        public float precio { get; set; }
        public int? garantia { get; set; }
        public int? stock { get; set; }
        public int? stockminimo { get; set; }
        public double descuento { get; set; }
        public int stockcatalogo { get; set; }

    }
}