﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Areas.Ventas.Models
{
    public class ClienteModel
    {
        public int ID_CLIENTE { get; set; }
        public string NOMBRE { get; set; }
        public string APELLIDO { get; set; }
        public string DIRECCION { get; set; }
        public string DISTRITO { get; set; }
        public string CORREO { get; set; }
        public string TELEFONO { get; set; }
    }
}