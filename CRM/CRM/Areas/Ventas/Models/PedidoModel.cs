﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CRM.Entity;

namespace CRM.Areas.Ventas.Models
{
    public class PedidoModel
    {
        public string ID_CLIENTE { get; set; }
        public string NOMBRE { get; set; }
        public string APELLIDO { get; set; }
        public string EMAIL { get; set; }
        public string TELEFONO { get; set; }
        public string DIRECCION { get; set; }
        public string DISTRITO { get; set; }
        public DateTime FECHAENTREGA { get; set; }
        public DateTime FECHAENVIO { get; set; }
        public string TIPO_PEDIDO { get; set; }
        public string PRIORIDAD_P { get; set; }
        public List<DETALLE_PEDIDO> producto { get; set; }
    }
}