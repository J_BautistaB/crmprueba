﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM.Entity;
using CRM.Areas.Ventas.Models;

namespace CRM.Areas.Ventas.Controllers
{
    public class CatalogoController : Controller
    {
        private CRMEntities db = new CRMEntities();

        public ActionResult Vista(int? id) {
            ViewData["catalogo"] = id;
            var productoModel = from ca in db.CATALOGO
                                join ct in db.CATALOGO_PRODUCTO on ca.ID_CATALOGO equals ct.ID_CATALOGO
                                join pr in db.PRODUCTO on ct.ID_PRODUCTO equals pr.ID_PRODUCTO
                                join ka in db.CATEGORIA on pr.ID_CATEGORIA equals ka.ID_CATEGORIA
                                join aq in db.SUBCATEGORIA1 on pr.ID_SUBCATEGORIA1 equals aq.ID_SUBCATEGORIA1
                                join er in db.SUBCATEGORIA2 on pr.ID_SUBCATEGORIA2 equals er.ID_SUBCATEGORIA2
                                where ca.ID_CATALOGO == id
                                select new ProductoModel{  
                                    idproducto= pr.ID_PRODUCTO,  
                                    catalogo = ca.DESCRIPCIONTEMPORADA,
                                    idCatalogo=ca.ID_CATALOGO,                                                               
                                     codigo = pr.CODIGO_PRODUCTO,
                                     nombre = pr.NOMBRE_PRODUCTO,
                                     modelo = pr.MODELO_PRODUCTO,
                                     categoria = ka.NOMBRE_CATEGORIA,
                                     subcategoria1 = aq.NOMBRE_SUBCATEGORIA1,
                                     subcategoria2 = er.NOMBRE_SUBCATEGORIA2,
                                     marca = pr.MARCA,
                                     precio = (float)pr.PRECIO_PRODUCTO,
                                     garantia =pr.GARANTIA,                                   
                                     descuento = ct.DESCUENTO,
                                     stock = ct.STOCK
                                };

            

            //@ViewBag.parametro = id;
            //var productos = new List<PRODUCTO>();
            //var catalogo_producto = new List<CATALOGO_PRODUCTO>();
            //var catalogo = new List<CATALOGO>();
            

            //try {
            //    //catalogo = db.CATALOGO.Include(x => x.CATALOGO_PRODUCTO.);
            //        //catalogo = context.CATALOGO.Include("catalogo_producto").Where(x => x.ID_CATALOGO == id).First();
                
            //} catch (Exception e){
            //    throw new Exception(e.Message);
            //}
            return View(productoModel.ToList());
        }

        public List<CATALOGO> Listar() {
            var catalogos = new List<CATALOGO>();
            try {
                using (var context = new CRMEntities()) {
                    catalogos = context.CATALOGO.ToList();
                }
            }
            catch (Exception e) {
                throw new Exception(e.Message);
            }
            return catalogos;
        }

        //
        // GET: /Ventas/Catalogo/

        public ActionResult Index()
        {
            return View(db.CATALOGO.ToList());
        }

        //
        // GET: /Ventas/Catalogo/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Ventas/Catalogo/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CATALOGO catalogo)
        {
            if (ModelState.IsValid)
            {
                db.CATALOGO.Add(catalogo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(catalogo);
        }

        //
        // GET: /Ventas/Catalogo/Edit/5
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            CATALOGO catalogo = db.CATALOGO.Find(id);
            if (catalogo == null)
            {
                return HttpNotFound();
            }
            return View(catalogo);
        }

        //
        // POST: /Ventas/Catalogo/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CATALOGO catalogo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(catalogo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(catalogo);
        }

        //
        // GET: /Ventas/Catalogo/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CATALOGO catalogo = db.CATALOGO.Find(id);
            if (catalogo == null)
            {
                return HttpNotFound();
            }
            return View(catalogo);
        }

        //
        // POST: /Ventas/Catalogo/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CATALOGO catalogo = db.CATALOGO.Find(id);
            db.CATALOGO.Remove(catalogo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}