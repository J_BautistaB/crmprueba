﻿using CRM.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM.Areas.Ventas.Controllers
{
    public class InicioController : Controller
    {
        public ActionResult Index()
        {            
            return View();
        }
    }
}
