﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM.Entity;
using CRM.Areas.Ventas.Models;
using System.Net.Mail;
using System.Text;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace CRM.Areas.Ventas.Controllers
{
    public class PedidoController : Controller
    {
        private CRMEntities db = new CRMEntities();

        //
        // GET: /Ventas/Pedido/

        public ActionResult Index()
        {
            var pedido = db.PEDIDO;
            return View(pedido.ToList());
        }

        public ActionResult Proforma()
        {
            var model = new ClienteProductoModel();
            model.producto = ListaProducto();
            model.cliente = ListaCliente("-------");
            return View(model);
        }

        [HttpPost]
        public ActionResult Correo(string html,string mail)
        {
            try
            {
                    var fromAddress = new MailAddress("crm@fisi.com", "COTIZACION");
                    var toAddress = new MailAddress(mail.Trim(), "Usuario");
                    const string subject = "COTIZACION";
                    const string body = "";

                    var smtp = new SmtpClient
                    {
                        Host = "smtp.live.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential("crm_ventas_unmsm@hotmail.com", "ventasunmsm123")
                    };
                
                using (var message = new MailMessage(fromAddress, toAddress) { Subject = subject, Body = body })
                    {
                        message.IsBodyHtml = true;
                        message.Body = "<htm><body>"+ html + "</body></html>";
                    ServicePointManager.ServerCertificateValidationCallback =delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors){ return true; };
                    smtp.Send(message);
                    }

                    return this.Json(new { msg = "ok" });
            }
            catch (Exception e)
            {
                return this.Json(new { msg = "false" });
            }
        }

        public ActionResult BuscaCliente(string nombre)
        {
            if (nombre == "") { nombre = "--------"; }
            var model = ListaCliente(nombre);
            return PartialView("ListaCliente", model);
        }

        //
        // GET: /Ventas/Pedido/Create

        public ActionResult Create()
        {
            var model = new ClienteProductoModel();
            model.producto =ListaProducto();
            model.cliente = ListaCliente("-------"); 
            return View(model);
        }

        //
        // POST: /Ventas/Pedido/Create

        [HttpPost]
        public ActionResult Create(PedidoModel pedido)
        {
            try {
                if (ModelState.IsValid)
                {
                    PEDIDO insertPedido = new PEDIDO();
                    insertPedido.DIRECCION = pedido.DIRECCION.Trim();
                    insertPedido.ID_EMPLEADO = null;
                    insertPedido.FECHAENTREGA = pedido.FECHAENTREGA;
                    insertPedido.FECHAENVIO = pedido.FECHAENVIO;
                    insertPedido.ID_CLIENTE = Convert.ToInt32(pedido.ID_CLIENTE);
                    insertPedido.PRIORIDAD_P = pedido.PRIORIDAD_P.Trim();
                    insertPedido.TIPO_PEDIDO = pedido.TIPO_PEDIDO.Trim();
                    insertPedido.DISTRITO = pedido.DISTRITO.Trim();

                    insertPedido.ID_PEDIDO = 0;
                    insertPedido.FECHAPEDIDO = DateTime.Now;
                    insertPedido.FORMAENVIO = "";
                    insertPedido.CARGO = 0.0;
                    insertPedido.DEPARTATAMENTO = "";
                    insertPedido.CODIGOPOSTAL = "";

                    db.PEDIDO.Add(insertPedido);
                    db.SaveChanges();
                    var id = insertPedido.ID_PEDIDO;

                    if (pedido.producto.Count() > 0)
                    {
                        foreach (DETALLE_PEDIDO item in pedido.producto)
                        {
                            DETALLE_PEDIDO itemDetalle = new DETALLE_PEDIDO();
                            itemDetalle.CANTIDAD = item.CANTIDAD;
                            itemDetalle.ID_CATALOGO = item.ID_CATALOGO;
                            itemDetalle.ID_PRODUCTO = item.ID_PRODUCTO;
                            itemDetalle.ID_PEDIDO = id;
                            db.DETALLE_PEDIDO.Add(itemDetalle);
                            db.SaveChanges();
                        }
                    }
                }
                 return this.Json(new { msg = "ok" });
            } catch (Exception e) {
                return this.Json(new { msg = "false" });
            }
            
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public IEnumerable<ProductoModel> ListaProducto()
        {
            var producto = from ca in db.CATALOGO
                           join ct in db.CATALOGO_PRODUCTO on ca.ID_CATALOGO equals ct.ID_CATALOGO
                           join pr in db.PRODUCTO on ct.ID_PRODUCTO equals pr.ID_PRODUCTO
                           join ka in db.CATEGORIA on pr.ID_CATEGORIA equals ka.ID_CATEGORIA
                           join aq in db.SUBCATEGORIA1 on pr.ID_SUBCATEGORIA1 equals aq.ID_SUBCATEGORIA1
                           join er in db.SUBCATEGORIA2 on pr.ID_SUBCATEGORIA2 equals er.ID_SUBCATEGORIA2
                           select new ProductoModel
                           {
                               idproducto = pr.ID_PRODUCTO,
                               idCatalogo = ca.ID_CATALOGO,
                               catalogo = ca.DESCRIPCIONTEMPORADA,
                               codigo = pr.CODIGO_PRODUCTO,
                               nombre = pr.NOMBRE_PRODUCTO,
                               modelo = pr.MODELO_PRODUCTO,
                               categoria = ka.NOMBRE_CATEGORIA,
                               subcategoria1 = aq.NOMBRE_SUBCATEGORIA1,
                               subcategoria2 = er.NOMBRE_SUBCATEGORIA2,
                               marca = pr.MARCA,
                               precio = (float)pr.PRECIO_PRODUCTO,
                               garantia = pr.GARANTIA,
                               descuento = ct.DESCUENTO,
                               stock = ct.STOCK
                           };
            return producto;
        }
        public IEnumerable<ClienteModel> ListaCliente(string nombre) {

            var distrito = "";
            var cliente = from m in db.PERSONA
                          join i in db.INTERLOCUTOR_COMERCIAL on m.ID_PERSONA equals i.ID_PERSONA
                          //join t in db.TELEFONO on i.ID_INTERLOCUTOR equals t.ID_INTERLOCUTOR
                          //join c in db.CORREO on i.ID_INTERLOCUTOR equals c.ID_INTERLOCUTOR
                          //join d in db.DIRECCION on i.ID_INTERLOCUTOR equals d.ID_INTERLOCUTOR
                          where m.NOMBRES.Contains(nombre)
                          select new ClienteModel
                          {
                            ID_CLIENTE = i.ID_INTERLOCUTOR
                            , NOMBRE = m.NOMBRES
                            , APELLIDO = m.AP_PATERNO
                            //, DIRECCION = d.DIRECCION1
                            //,DISTRITO = distrito
                            //,CORREO = c.EMAIL
                            //,TELEFONO = t.NUMERO
                          };
            return cliente;
        }

        private Boolean ValidarCertificado(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
    }
}