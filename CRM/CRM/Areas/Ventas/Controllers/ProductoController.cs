﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM.Entity;
using CRM.Areas.Ventas.Models;

namespace CRM.Areas.Ventas.Controllers
{
    public class ProductoController : Controller
    {
        private CRMEntities db = new CRMEntities();

        //
        // GET: /Ventas/Producto/

        public ActionResult Index(int id)
        {
            ViewData["catalogo"] = id;
            var noProducto = from b in db.CATALOGO_PRODUCTO where b.ID_CATALOGO == id select b;
            var filtroProducto = db.PRODUCTO.Where(a => !noProducto.Any(b => b.ID_PRODUCTO == a.ID_PRODUCTO));
            var producto = from ca in filtroProducto
                                join ka in db.CATEGORIA on ca.ID_CATEGORIA equals ka.ID_CATEGORIA
                                join aq in db.SUBCATEGORIA1 on ca.ID_SUBCATEGORIA1 equals aq.ID_SUBCATEGORIA1
                                join er in db.SUBCATEGORIA2 on ca.ID_SUBCATEGORIA2 equals er.ID_SUBCATEGORIA2
                                select new ProductoModel
                                {
                                    idproducto=ca.ID_PRODUCTO,
                                    idCatalogo = id,
                                    catalogo = "",
                                    codigo = ca.CODIGO_PRODUCTO,
                                    nombre = ca.NOMBRE_PRODUCTO,
                                    modelo = ca.MODELO_PRODUCTO,
                                    categoria = ka.NOMBRE_CATEGORIA,
                                    subcategoria1 = aq.NOMBRE_SUBCATEGORIA1,
                                    subcategoria2 = er.NOMBRE_SUBCATEGORIA2,
                                    marca = ca.MARCA,
                                    precio = (float)ca.PRECIO_PRODUCTO,
                                    garantia = ca.GARANTIA,
                                    descuento = 0,
                                    stock = 0
                                };
            return View(producto.ToList());
        }

        //
        // GET: /Ventas/Producto/Details/5
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GuardarProducto(ProductoCatalogoModel json)
        {
            
            if (json.text.Length > 1){
                string[] lista = json.text.Split('-');
                foreach (string item in lista) {
                    if (item.Length > 0){
                        CATALOGO_PRODUCTO itemCatalogo = new CATALOGO_PRODUCTO();
                        itemCatalogo.ID_CATALOGO = json.catalogoid;
                        itemCatalogo.ID_PRODUCTO = Convert.ToInt32(item);
                        itemCatalogo.STOCK = 0;
                        itemCatalogo.DESCUENTO = 0.0;
                        db.CATALOGO_PRODUCTO.Add(itemCatalogo);
                        db.SaveChanges();
                    }
                }
                
                return this.Json(new { msg = "ok" });
            }
            else {
                return this.Json(new { msg = "false" });
            }           
        }

        //
        // GET: /Ventas/Catalogo/Delete/5

        public ActionResult Delete(string id)
        {
            string[] llave = id.Split('-');
            CATALOGO_PRODUCTO catalogo = db.CATALOGO_PRODUCTO.Find(Convert.ToInt32(llave[0]), Convert.ToInt32(llave[1]));
            if (catalogo == null)
            {
                return HttpNotFound();
            }
            return View(catalogo);
        }

        //
        // POST: /Ventas/Catalogo/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            string[] llave = id.Split('-');
            CATALOGO_PRODUCTO catalogo = db.CATALOGO_PRODUCTO.Find(Convert.ToInt32(llave[0]), Convert.ToInt32(llave[1]));
            db.CATALOGO_PRODUCTO.Remove(catalogo);
            db.SaveChanges();
            return RedirectToAction("Vista","Catalogo", new { id = Convert.ToInt32(llave[0]) });
        }

        public ActionResult Edit(string id)
        {
            string[] llave = id.Split('-');
            int idCatalogo = Convert.ToInt32(llave[0]);
            int idProducto = Convert.ToInt32(llave[1]);
            var producto = from cap in db.CATALOGO_PRODUCTO
                           join ca in db.CATALOGO on cap.ID_CATALOGO equals ca.ID_CATALOGO
                           join pr in db.PRODUCTO on cap.ID_PRODUCTO equals pr.ID_PRODUCTO
                           join ka in db.CATEGORIA on pr.ID_CATEGORIA equals ka.ID_CATEGORIA
                           join aq in db.SUBCATEGORIA1 on pr.ID_SUBCATEGORIA1 equals aq.ID_SUBCATEGORIA1
                           join er in db.SUBCATEGORIA2 on pr.ID_SUBCATEGORIA2 equals er.ID_SUBCATEGORIA2
                           where cap.ID_CATALOGO == idCatalogo && cap.ID_PRODUCTO== idProducto
                           select new ProductoModel
                           {
                               idproducto = pr.ID_PRODUCTO,
                               idCatalogo = Convert.ToInt32(cap.ID_CATALOGO),
                               catalogo = ca.DESCRIPCIONTEMPORADA,
                               codigo = pr.CODIGO_PRODUCTO,
                               nombre = pr.NOMBRE_PRODUCTO,
                               modelo = pr.MODELO_PRODUCTO,
                               categoria = ka.NOMBRE_CATEGORIA,
                               subcategoria1 = aq.NOMBRE_SUBCATEGORIA1,
                               subcategoria2 = er.NOMBRE_SUBCATEGORIA2,
                               marca = pr.MARCA,
                               precio = (float)pr.PRECIO_PRODUCTO,
                               garantia = pr.GARANTIA,
                               descuento = cap.DESCUENTO,
                               stock = cap.STOCK
                           };
            var resultado = producto.FirstOrDefault();
            if (resultado == null)
            {
                return HttpNotFound();
            }
            return View(resultado);
        }

        //
        // POST: /Ventas/Catalogo/Delete/5

        [HttpPost]
        public ActionResult Edit(ProductoModel producto)
        {
            CATALOGO_PRODUCTO catalogo = db.CATALOGO_PRODUCTO.Find(Convert.ToInt32(producto.idCatalogo), Convert.ToInt32(producto.idproducto));
            catalogo.DESCUENTO = producto.descuento;
            catalogo.STOCK = (int)producto.stock;
            db.Entry(catalogo).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Vista", "Catalogo", new { id = Convert.ToInt32(producto.idCatalogo) });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}