﻿using CRM.Areas.Clientes.Models;
using CRM.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Transactions;

namespace CRM.Areas.Clientes.AccesoDatos
{
    public class InterlocutorAccess
    {
        #region Singleton

        static InterlocutorAccess() { }
        private InterlocutorAccess() { }
        private static InterlocutorAccess _instancia = new InterlocutorAccess();
        public static InterlocutorAccess Instancia { get { return _instancia; } }

        #endregion

        public int RegistrarInterlocutorPersona(ClienteNaturalModel c)
        {
            int interlocutor = 0;
            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    //using (TransactionScope transaction = new TransactionScope())
                    //{
                        int p = PersonaAccess.Instancia.RegistrarPersonaNatural(c.Nombres, c.ApePaterno, c.ApeMaterno, c.DNI, c.Nacimiento, c.Genero, Convert.ToInt32(c.EstadoCivil), 4);
                        conexion.USP_INTERLOCUTOR_COMERCIAL_REGISTRAR(p, c.IdTienda, null, 2, null, null, null, null, true, null);
                        var res =
                        from i in conexion.INTERLOCUTOR_COMERCIAL
                        join pe in conexion.PERSONA on i.ID_PERSONA equals pe.ID_PERSONA
                        where pe.DNI == c.DNI
                        select new
                        {
                            i.ID_INTERLOCUTOR
                        };
                        interlocutor = res.FirstOrDefault().ID_INTERLOCUTOR;

                        foreach (var item in c.ListaCorreos)
                        {
                            GeneralAccess.Instancia.RegistrarCorreo(interlocutor, item);
                        }
                        foreach (var item in c.ListaTelefonos)
                        {
                            GeneralAccess.Instancia.RegistrarTelefono(interlocutor, item);
                        }
                        foreach (var item in c.ListaDirecciones)
                        {
                            GeneralAccess.Instancia.RegistrarDireccion(interlocutor, item);
                        }
                        foreach (var item in c.ListaRelaciones)
                        {
                            GeneralAccess.Instancia.RegistrarRelacion(interlocutor, item);
                        }
                    //    transaction.Complete();
                    //}
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return interlocutor;
        }

        public int RegistrarInterlocutorEmpresa(EmpresaModel e)
        {
            int interlocutor = 0;
            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    //using (TransactionScope transaction = new TransactionScope())
                    //{
                        int p = PersonaAccess.Instancia.RegistrarPersonaEmpresa(e.RazonSocial, e.RUC, e.RubroEmpresa, 5);
                        conexion.USP_INTERLOCUTOR_COMERCIAL_REGISTRAR(p, e.IdTienda, null, 2, null, null, e.PaginaWeb, null, true, null);
                        var res =
                        from i in conexion.INTERLOCUTOR_COMERCIAL
                        join pe in conexion.PERSONA on i.ID_PERSONA equals pe.ID_PERSONA
                        where pe.RUC == e.RUC
                        select new
                        {
                            i.ID_INTERLOCUTOR
                        };
                        interlocutor = res.FirstOrDefault().ID_INTERLOCUTOR;

                        foreach (var item in e.ListaCorreos)
                        {
                            GeneralAccess.Instancia.RegistrarCorreo(interlocutor, item);
                        }
                        foreach (var item in e.ListaTelefonos)
                        {
                            GeneralAccess.Instancia.RegistrarTelefono(interlocutor, item);
                        }
                        foreach (var item in e.ListaDirecciones)
                        {
                            GeneralAccess.Instancia.RegistrarDireccion(interlocutor, item);
                        }
                        foreach (var item in e.ListaRelaciones)
                        {
                            GeneralAccess.Instancia.RegistrarRelacion(interlocutor, item);
                        }
                    //    transaction.Complete();
                    //}
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return interlocutor;
        }

        public int RegistrarInterlocutorEmpleado(EmpleadoModel c)
        {
            int interlocutor = 0;
            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    //using (TransactionScope transaction = new TransactionScope())
                    //{
                        int p = PersonaAccess.Instancia.RegistrarPersonaNatural(c.Nombres, c.ApePaterno, c.ApeMaterno, c.DNI, c.Nacimiento, c.Genero, Convert.ToInt32(c.EstadoCivil), 4);
                        conexion.USP_INTERLOCUTOR_COMERCIAL_REGISTRAR(p, c.IdTienda, c.IdTienda, 2, c.TipoEmpleado, c.AreaEmpleado, null, null, null, true);
                        var res =
                        from i in conexion.INTERLOCUTOR_COMERCIAL
                        join pe in conexion.PERSONA on i.ID_PERSONA equals pe.ID_PERSONA
                        where pe.DNI == c.DNI
                        select new
                        {
                            i.ID_INTERLOCUTOR
                        };
                        interlocutor = res.FirstOrDefault().ID_INTERLOCUTOR;

                        foreach (var item in c.ListaCorreos)
                        {
                            GeneralAccess.Instancia.RegistrarCorreo(interlocutor, item);
                        }
                        foreach (var item in c.ListaTelefonos)
                        {
                            GeneralAccess.Instancia.RegistrarTelefono(interlocutor, item);
                        }
                        foreach (var item in c.ListaDirecciones)
                        {
                            GeneralAccess.Instancia.RegistrarDireccion(interlocutor, item);
                        }
                        foreach (var item in c.ListaRelaciones)
                        {
                            GeneralAccess.Instancia.RegistrarRelacion(interlocutor, item);
                        }
                    //    transaction.Complete();
                    //}
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return interlocutor;
        }

        public int EditarInterlocutorPersona(int id, ClienteNaturalModel c, bool? esInteresado, bool? esCLiente, bool? esEmpleado)
        {
            int interlocutor = 0;
            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    //using (TransactionScope transaction = new TransactionScope())
                    //{
                        var per = conexion.INTERLOCUTOR_COMERCIAL.Where(x => x.ID_INTERLOCUTOR == c.ID).Single();
                        int p = PersonaAccess.Instancia.EditarPersonaNatural(per.PERSONA.ID_PERSONA, c.Nombres, c.ApePaterno, c.ApeMaterno, c.DNI, c.Nacimiento, c.Genero, Convert.ToInt32(c.EstadoCivil), 4);

                        conexion.USP_INTERLOCUTOR_COMERCIAL_ACTUALIZAR(id, p, c.IdTienda, null, 2, null, null, null, esInteresado, esCLiente, esEmpleado);
                        var res =
                        from i in conexion.INTERLOCUTOR_COMERCIAL
                        join pe in conexion.PERSONA on i.ID_PERSONA equals pe.ID_PERSONA
                        where pe.DNI == c.DNI
                        select new
                        {
                            i.ID_INTERLOCUTOR
                        };
                        interlocutor = res.FirstOrDefault().ID_INTERLOCUTOR;

                        GeneralAccess.Instancia.EliminarRelaciones(interlocutor);

                        foreach (var item in c.ListaCorreos)
                        {
                            GeneralAccess.Instancia.RegistrarCorreo(interlocutor, item);
                        }
                        foreach (var item in c.ListaTelefonos)
                        {
                            GeneralAccess.Instancia.RegistrarTelefono(interlocutor, item);
                        }
                        foreach (var item in c.ListaDirecciones)
                        {
                            GeneralAccess.Instancia.RegistrarDireccion(interlocutor, item);
                        }
                        foreach (var item in c.ListaRelaciones)
                        {
                            GeneralAccess.Instancia.RegistrarRelacion(interlocutor, item);
                        }
                    //    transaction.Complete();
                    //}
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return interlocutor;
        }

        public int EditarInterlocutorEmpresa(int id, EmpresaModel e, bool? esInteresado, bool? esCLiente)
        {
            int interlocutor = 0;
            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    //using (TransactionScope transaction = new TransactionScope())
                    //{
                        int p = PersonaAccess.Instancia.EditarPersonaEmpresa(id, e.RazonSocial, e.RUC, e.RubroEmpresa, 5);
                        conexion.USP_INTERLOCUTOR_COMERCIAL_ACTUALIZAR(id, p, 1, null, 2, null, null, e.PaginaWeb, esInteresado, esCLiente, null);  //EMPLEADO TRUE?
                        var res =
                        from i in conexion.INTERLOCUTOR_COMERCIAL
                        join pe in conexion.PERSONA on i.ID_PERSONA equals pe.ID_PERSONA
                        where pe.RUC == e.RUC
                        select new
                        {
                            i.ID_INTERLOCUTOR
                        };
                        interlocutor = res.FirstOrDefault().ID_INTERLOCUTOR;

                        GeneralAccess.Instancia.EliminarRelaciones(interlocutor);

                        foreach (var item in e.ListaCorreos)
                        {
                            GeneralAccess.Instancia.RegistrarCorreo(interlocutor, item);
                        }
                        foreach (var item in e.ListaTelefonos)
                        {
                            GeneralAccess.Instancia.RegistrarTelefono(interlocutor, item);
                        }
                        foreach (var item in e.ListaDirecciones)
                        {
                            GeneralAccess.Instancia.RegistrarDireccion(interlocutor, item);
                        }
                        foreach (var item in e.ListaRelaciones)
                        {
                            GeneralAccess.Instancia.RegistrarRelacion(interlocutor, item);
                        }
                    //    transaction.Complete();
                    //}
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return interlocutor;
        }

        public int EditarInterlocutorEmpleado(int id, EmpleadoModel c)
        {
            int interlocutor = 0;
            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    //using (TransactionScope transaction = new TransactionScope())
                    //{
                        int p = PersonaAccess.Instancia.EditarPersonaNatural(c.identPersona, c.Nombres, c.ApePaterno, c.ApeMaterno, c.DNI, c.Nacimiento, c.Genero, Convert.ToInt32(c.EstadoCivil), 4);

                        conexion.USP_INTERLOCUTOR_COMERCIAL_ACTUALIZAR(id, p, c.IdTienda, c.IdTienda, 2, c.TipoEmpleado, c.AreaEmpleado, null, null, true, null);
                        var res =
                        from i in conexion.INTERLOCUTOR_COMERCIAL
                        join pe in conexion.PERSONA on i.ID_PERSONA equals pe.ID_PERSONA
                        where pe.DNI == c.DNI
                        select new
                        {
                            i.ID_INTERLOCUTOR
                        };
                        interlocutor = res.FirstOrDefault().ID_INTERLOCUTOR;

                        GeneralAccess.Instancia.EliminarRelaciones(interlocutor);

                        foreach (var item in c.ListaCorreos)
                        {
                            GeneralAccess.Instancia.RegistrarCorreo(interlocutor, item);
                        }
                        foreach (var item in c.ListaTelefonos)
                        {
                            GeneralAccess.Instancia.RegistrarTelefono(interlocutor, item);
                        }
                        foreach (var item in c.ListaDirecciones)
                        {
                            GeneralAccess.Instancia.RegistrarDireccion(interlocutor, item);
                        }
                        foreach (var item in c.ListaRelaciones)
                        {
                            GeneralAccess.Instancia.RegistrarRelacion(interlocutor, item);
                        }
                    //    transaction.Complete();
                    //}
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return interlocutor;
        }

        public List<EmpleadoModel> ListaEmpleados(string busqueda)
        {
            List<EmpleadoModel> lista = new List<EmpleadoModel>();

            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    List<INTERLOCUTOR_COMERCIAL> list = new List<INTERLOCUTOR_COMERCIAL>();
                    list = conexion.INTERLOCUTOR_COMERCIAL
                    .Where(x => x.PERSONA.IDP_TIPO_PERSONA == 4)
                    .Where(X => X.ES_EMPLEADO == 1)
                    .Where(x => x.PERSONA.NOMBRES.Contains(busqueda) || x.PERSONA.AP_PATERNO.Contains(busqueda) || x.PERSONA.AP_MATERNO.Contains(busqueda) || x.PERSONA.DNI.Contains(busqueda))
                    .ToList();

                    foreach (var item in list)
                    {
                        EmpleadoModel em = new EmpleadoModel();
                        em.ID = item.ID_INTERLOCUTOR;
                        em.ApeMaterno = item.PERSONA.AP_MATERNO;
                        em.ApePaterno = item.PERSONA.AP_PATERNO;
                        em.Nombres = item.PERSONA.NOMBRES;
                        em.DNI = item.PERSONA.DNI;
                        em.identPersona = item.PERSONA.ID_PERSONA;
                        em.IdTienda = Convert.ToInt32(item.ID_TIENDA);

                        lista.Add(em);
                    }
                }
                catch
                {
                    lista = null;
                }
            }
            return lista;
        }


        public List<ClienteNaturalModel> ListaClientesNaturales(string busqueda)
        {
            List<ClienteNaturalModel> listaClientes = new List<ClienteNaturalModel>();

            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    var lista = conexion.USP_INTERLOCUTOR_NATURAL_OBTENER(0, busqueda);
                    foreach (var item in lista)
                    {
                        ClienteNaturalModel c = new ClienteNaturalModel();
                        c.ID = item.ID_INTERLOCUTOR;
                        c.ApePaterno = item.AP_PATERNO;
                        c.ApeMaterno = item.AP_MATERNO;
                        c.Nombres = item.NOMBRES;
                        c.DNI = item.DNI;

                        listaClientes.Add(c);
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return listaClientes;
        }

        public ClienteNaturalModel ObtenerClienteNatural(int id)
        {
            ClienteNaturalModel c = new ClienteNaturalModel();

            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    var item = conexion.USP_INTERLOCUTOR_COMERCIAL_OBTENER(id, null, null, null, null, null, null, null, true, null, "", 4).FirstOrDefault();
                    var per = conexion.USP_PERSONA_OBTENER(item.ID_PERSONA, 0).FirstOrDefault();
                    var ltel = GeneralAccess.Instancia.ListaTelefono(id);
                    var lcorreo = GeneralAccess.Instancia.ListaCorreo(id);
                    var ldirec = GeneralAccess.Instancia.ListaDireccion(id);
                    var lrel = GeneralAccess.Instancia.ListaRelacion(id);

                    c.IdTienda = item.ID_TIENDA;
                    c.ID = id;
                    c.identPersona = per.ID_PERSONA;
                    c.Nombres = per.NOMBRES;
                    c.DNI = item.DNI;
                    c.ApeMaterno = per.AP_MATERNO;
                    c.ApePaterno = per.AP_PATERNO;
                    c.EstadoCivil = Convert.ToString(per.IDP_ESTADO_CIVIL);
                    c.Genero = per.GENERO;
                    c.Nacimiento = per.NACIMIENTO;
                    c.ListaTelefonos = ltel;
                    c.ListaRelaciones = lrel;
                    c.ListaCorreos = lcorreo;
                    c.ListaDirecciones = ldirec;
                }
                catch
                {
                    c = null;
                }
            }
            return c;
        }

        public EmpresaModel ObtenerEmpresa(int id)
        {
            EmpresaModel c = new EmpresaModel();

            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    var item = conexion.INTERLOCUTOR_COMERCIAL.Where(x => x.ID_INTERLOCUTOR == id).Single();
                    //var item = conexion.USP_INTERLOCUTOR_COMERCIAL_OBTENER(id, 0, 0, 0, 0, 0, 0, null, true, null, "", 5).FirstOrDefault();
                    var per = conexion.PERSONA.Where(x => x.ID_PERSONA == item.ID_PERSONA).Where(x => x.IDP_TIPO_PERSONA == 5).Single();
                    //var per = conexion.USP_PERSONA_OBTENER(item.ID_PERSONA, 0).FirstOrDefault();
                    var ltel = GeneralAccess.Instancia.ListaTelefono(id);
                    var lcorreo = GeneralAccess.Instancia.ListaCorreo(id);
                    var ldirec = GeneralAccess.Instancia.ListaDireccion(id);
                    var lrel = GeneralAccess.Instancia.ListaRelacion(id);

                    c.ID = item.ID_INTERLOCUTOR;
                    c.RazonSocial = per.RAZON_SOCIAL;
                    c.RUC = per.RUC;
                    c.RubroEmpresa = per.IDP_RUBRO_EMPRESA.ToString();
                    c.PaginaWeb = item.PAGINA_WEB;
                    c.ListaTelefonos = ltel;
                    c.ListaRelaciones = lrel;
                    c.ListaCorreos = lcorreo;
                    c.ListaDirecciones = ldirec;
                }
                catch
                {
                    c = null;
                }
            }
            return c;
        }

        public EmpleadoModel ObtenerEmpleado(int id)
        {
            EmpleadoModel c = new EmpleadoModel();

            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    var item = conexion.USP_INTERLOCUTOR_COMERCIAL_OBTENER(id, null, null, null, null, null, null, null, null, true, "", 4).FirstOrDefault();
                    var per = conexion.USP_PERSONA_OBTENER(item.ID_PERSONA, 0).FirstOrDefault();
                    var ltel = GeneralAccess.Instancia.ListaTelefono(id);
                    var lcorreo = GeneralAccess.Instancia.ListaCorreo(id);
                    var ldirec = GeneralAccess.Instancia.ListaDireccion(id);
                    var lrel = GeneralAccess.Instancia.ListaRelacion(id);

                    c.IdTienda = item.ID_TIENDA;
                    c.ID = id;
                    c.identPersona = per.ID_PERSONA;
                    c.Nombres = per.NOMBRES;
                    c.DNI = item.DNI;
                    c.ApeMaterno = per.AP_MATERNO;
                    c.ApePaterno = per.AP_PATERNO;
                    c.EstadoCivil = Convert.ToString(per.IDP_ESTADO_CIVIL);
                    c.Genero = per.GENERO;
                    c.Nacimiento = per.NACIMIENTO;

                    c.AreaEmpleado = item.AREA_EMPLEADO;
                    c.TipoEmpleado = item.TIPO_EMPLEADO;

                    c.ListaTelefonos = ltel;
                    c.ListaRelaciones = lrel;
                    c.ListaCorreos = lcorreo;
                    c.ListaDirecciones = ldirec;
                }
                catch
                {
                    c = null;
                }
            }
            return c;
        }

        public List<InterlocutorComercialModel> ListaInterlocutor(int tipopersona, string busqueda)
        {
            List<InterlocutorComercialModel> listaClientes = new List<InterlocutorComercialModel>();

            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    var lista = conexion.USP_INTERLOCUTOR_COMERCIAL_OBTENER(0, 0, 0, 0, 0, 0, 0, null, null, null, busqueda, tipopersona);
                    foreach (var item in lista)
                    {
                        InterlocutorComercialModel c = new InterlocutorComercialModel();
                        c.ID = item.ID_INTERLOCUTOR;
                        c.Nombre = item.NOMBRE;
                        c.Documento = item.TIPO_DOCUMENTO;
                        if (c.Documento == "DNI")
                        {
                            c.NumDocumento = item.DNI;
                        }
                        else
                        {
                            c.NumDocumento = item.RUC;
                        }
                        //c.Direccion = item.DIRECCIONES;
                        //c.Telefono = item.TELEFONOS;
                        //c.Correo = item.CORREOS;
                        listaClientes.Add(c);
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return listaClientes;
        }

        public List<InterlocutorComercialModel> ListaInterlocutor(BusquedaICModel B)
        {
            List<InterlocutorComercialModel> listaClientes = new List<InterlocutorComercialModel>();

            using (CRMEntities conexion = new CRMEntities())
            {
                List<INTERLOCUTOR_COMERCIAL> lista = new List<INTERLOCUTOR_COMERCIAL>();
                if (B.TipoPersona != null)
                {
                    switch (B.TipoPersona)
                    {
                        case 4:
                            switch (B.TipoRol)
                            {
                                case 1:
                                    //Buscando persona natural con rol cliente
                                    lista = conexion.INTERLOCUTOR_COMERCIAL
                                        .Where(x => x.PERSONA.IDP_TIPO_PERSONA == 4)
                                        .Where(x => x.ES_CLIENTE == 1)
                                        .Where(x => x.ES_INTERESADO == 0 || x.ES_INTERESADO == null)
                                        .Where(x => x.PERSONA.NOMBRES.Contains(B.PalabraClave) || x.PERSONA.AP_PATERNO.Contains(B.PalabraClave) || x.PERSONA.AP_MATERNO.Contains(B.PalabraClave) || x.PERSONA.DNI.Contains(B.PalabraClave))
                                        .ToList();
                                    break;
                                case 2:
                                    //Buscando persona natural con rol empleado
                                    lista = conexion.INTERLOCUTOR_COMERCIAL
                                        .Where(x => x.PERSONA.IDP_TIPO_PERSONA == 4)
                                        .Where(X => X.ES_EMPLEADO == 1)
                                        .Where(x => x.PERSONA.NOMBRES.Contains(B.PalabraClave) || x.PERSONA.AP_PATERNO.Contains(B.PalabraClave) || x.PERSONA.AP_MATERNO.Contains(B.PalabraClave) || x.PERSONA.DNI.Contains(B.PalabraClave))
                                        .ToList();
                                    break;
                                case 3:
                                    //Buscando persona natural con rol interesado
                                    lista = conexion.INTERLOCUTOR_COMERCIAL
                                        .Where(X => X.ES_INTERESADO == 1)
                                        .Where(x => x.PERSONA.NOMBRES.Contains(B.PalabraClave) || x.PERSONA.AP_PATERNO.Contains(B.PalabraClave) || x.PERSONA.AP_MATERNO.Contains(B.PalabraClave) || x.PERSONA.DNI.Contains(B.PalabraClave) || x.PERSONA.RAZON_SOCIAL.Contains(B.PalabraClave) || x.PERSONA.RUC.Contains(B.PalabraClave))
                                        .ToList();
                                    break;
                            }
                            foreach (var item in lista)
                            {
                                InterlocutorComercialModel c = new InterlocutorComercialModel();
                                c.ID = item.ID_INTERLOCUTOR;
                                switch (item.PERSONA.IDP_TIPO_PERSONA)
                                {
                                    case 4:
                                        c.Nombre = item.PERSONA.NOMBRES + " " + item.PERSONA.AP_PATERNO + " " + item.PERSONA.AP_MATERNO;
                                        c.Documento = "DNI";
                                        c.NumDocumento = item.PERSONA.DNI;
                                        break;
                                    case 5:
                                        c.Nombre = item.PERSONA.RAZON_SOCIAL;
                                        c.Documento = "RUC";
                                        c.NumDocumento = item.PERSONA.RUC;
                                        break;
                                }

                                c.Direccion = item.DIRECCION.FirstOrDefault() != null ? item.DIRECCION.FirstOrDefault().DIRECCION1 : "-";
                                c.Telefono = item.TELEFONO.FirstOrDefault() != null ? item.TELEFONO.FirstOrDefault().NUMERO : "-";
                                c.Correo = item.CORREO.FirstOrDefault() != null ? item.CORREO.FirstOrDefault().EMAIL : "-";
                                c.TipoRol = B.TipoRol;
                                listaClientes.Add(c);
                            }
                            break;
                        case 5:
                            //Listando empresas
                            lista = conexion.INTERLOCUTOR_COMERCIAL
                                .Where(x => x.PERSONA.IDP_TIPO_PERSONA == 5)
                                .Where(x => x.ES_CLIENTE == 1)
                                .Where(x => x.PERSONA.RAZON_SOCIAL.Contains(B.PalabraClave) || x.PERSONA.RUC.Contains(B.PalabraClave))
                                .ToList();
                            foreach (var item in lista)
                            {
                                InterlocutorComercialModel c = new InterlocutorComercialModel();
                                c.ID = item.ID_INTERLOCUTOR;
                                c.Nombre = item.PERSONA.RAZON_SOCIAL;
                                c.Documento = "RUC";
                                c.NumDocumento = item.PERSONA.RUC;
                                c.Direccion = item.DIRECCION.FirstOrDefault() != null ? item.DIRECCION.FirstOrDefault().DIRECCION1 : "-";
                                c.Telefono = item.TELEFONO.FirstOrDefault() != null ? item.TELEFONO.FirstOrDefault().NUMERO : "-";
                                c.Correo = item.CORREO.FirstOrDefault() != null ? item.CORREO.FirstOrDefault().EMAIL : "-";
                                c.TipoRol = B.TipoRol;
                                listaClientes.Add(c);
                            }
                            break;
                    }

                }
                else
                {
                    //var lista = conexion.INTERLOCUTOR_COMERCIAL
                    //    .ToList();
                }
            }
            return listaClientes;
        }
    }

}