﻿using CRM.Areas.Clientes.Models;
using CRM.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Transactions;

namespace CRM.Areas.Clientes.AccesoDatos
{
    public class GeneralAccess
    {
        #region Singleton

        static GeneralAccess() { }
        private GeneralAccess() { }
        private static GeneralAccess _instancia = new GeneralAccess();
        public static GeneralAccess Instancia { get { return _instancia; } }

        #endregion

        public IEnumerable<SelectListItem> ListaTipo(int tipo)
        {
            List<SelectListItem> listado = new List<SelectListItem>();

            var list = ListaParametro(0, tipo, 0, 1);
            foreach (var item in list)
            {
                SelectListItem sl = new SelectListItem();
                sl.Value = Convert.ToString(item.ID_PARAMETRO);
                sl.Text = item.DESCRIPCION;
                listado.Add(sl);
            }
            return listado;

        }

        public List<PARAMETRO> ListaParametro(int identparametro, int identtipo, int identpadre, int estado)
        {
            List<PARAMETRO> parametros = new List<PARAMETRO>();

            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    var lista = conexion.USP_PARAMETRO_OBTENER(identparametro, identtipo, identpadre, estado);
                    foreach (var item in lista)
                    {
                        PARAMETRO p = new PARAMETRO();
                        p.ID_PARAMETRO = item.ID_PARAMETRO;
                        p.DESCRIPCION = item.DESCRIPCION;
                        p.ID_TIPO_PARAMETRO = item.ID_TIPO_PARAMETRO;
                        p.ID_PADRE = item.ID_PADRE;
                        p.ESTADO = item.ESTADO;

                        parametros.Add(p);
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return parametros;
        }


        #region listar generales

        public List<TelefonoModel> ListaTelefono(int interlocutor)
        {
            List<TelefonoModel> lista = new List<TelefonoModel>();

            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    var l = conexion.USP_TELEFONO_OBTENER(0, interlocutor, 0, 0, 0);
                    foreach (var item in l)
                    {
                        TelefonoModel x = new TelefonoModel();

                        x.ID = item.ID_TELEFONO;
                        //x.identInterlocutor = item.ID_INTERLOCUTOR;
                        x.IDOperador = item.IDP_OPERADOR;
                        x.IDTipoTelefono = item.IDP_TIPO_TELEFONO;
                        x.TipoTelefono = item.TIPO_TELEFONO;
                        x.Numero = item.NUMERO;

                        lista.Add(x);
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return lista;
        }

        public List<CorreoModel> ListaCorreo(int interlocutor)
        {
            List<CorreoModel> lista = new List<CorreoModel>();

            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    var l = conexion.USP_CORREO_OBTENER(0, interlocutor, "");
                    foreach (var item in l)
                    {
                        CorreoModel x = new CorreoModel();

                        x.ID = item.ID_CORREO;
                        //x.identInterlocutor = item.ID_INTERLOCUTOR;
                        x.Email = item.EMAIL;

                        lista.Add(x);
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return lista;
        }

        public List<DireccionModel> ListaDireccion(int interlocutor)
        {
            List<DireccionModel> lista = new List<DireccionModel>();

            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    var l = conexion.USP_DIRECCION_OBTENER(0, interlocutor);
                    foreach (var item in l)
                    {
                        DireccionModel x = new DireccionModel();

                        x.ID = item.ID_DIRECCION;
                        //x.identInterlocutor = item.ID_INTERLOCUTOR;
                        x.NombreDireccion = item.DIRECCION;
                        x.ID_Departamento = Convert.ToInt32(item.IDP_DEPARTAMENTO);
                        x.Nombre_Departamento = item.DEPARTAMENTO_DESCRIPCION;
                        x.ID_Provincia = Convert.ToInt32(item.IDP_PROVINCIA);
                        x.Nombre_Provincia = item.PROVINCIA_DESCRIPCION;
                        x.ID_Distrito = Convert.ToInt32(item.IDP_DISTRITO);
                        x.Nombre_Distrito = item.DISTRITO_DESCRIPCION;
                        lista.Add(x);
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return lista;
        }

        public List<RelacionModel> ListaRelacion(int interlocutor)
        {
            List<RelacionModel> lista = new List<RelacionModel>();

            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    var l = conexion.USP_RELACION_OBTENER(0, interlocutor);
                    foreach (var item in l)
                    {
                        RelacionModel x = new RelacionModel();

                        x.ID = item.ID_RELACION;
                        //x.identInterlocutor = item.ID_INTERLOCUTOR;
                        x.IDRelacionado = Convert.ToInt32(item.ID_INTERLOCUTOR_RELACIONADO);
                        //x.IDTipoRelacion = Convert.ToInt32(item.IDP_RELACION);
                        x.NombreRelacion = item.DESCRIPCION;
                        x.NombreRelacionado = item.NOMBRE_RELACIONADO;
                        lista.Add(x);
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return lista;
        }

        #endregion

        public int RegistrarTelefono(int intelocutor, TelefonoModel t)
        {
            using (CRMEntities conexion = new CRMEntities())
            {
                    try
                    {
                    
                            conexion.USP_TELEFONO_REGISTRAR(intelocutor, Convert.ToInt32(t.Numero), t.IDTipoTelefono, null);
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }

            }
            return 1;
        }

        public void EliminarRelaciones(int interlocutor)
        {
            using (CRMEntities conexion = new CRMEntities())
            {

                    try
                    {
                        var deletet = conexion.TELEFONO.Where(TELEFONO => TELEFONO.ID_INTERLOCUTOR == interlocutor);
                        foreach (var x in deletet)
                        {
                            conexion.TELEFONO.Remove(x);
                        }
                        var deletec = conexion.CORREO.Where(CORREO => CORREO.ID_INTERLOCUTOR == interlocutor);
                        foreach (var x in deletec)
                        {
                            conexion.CORREO.Remove(x);
                        }
                        var deleted = conexion.DIRECCION.Where(DIRECCION => DIRECCION.ID_INTERLOCUTOR == interlocutor);
                        foreach (var x in deleted)
                        {
                            conexion.DIRECCION.Remove(x);
                        }
                        var deleter = conexion.RELACION.Where(RELACION => RELACION.ID_INTERLOCUTOR == interlocutor);
                        foreach (var x in deleter)
                        {
                            conexion.RELACION.Remove(x);
                        }
                        conexion.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
            }
        }

        public int RegistrarCorreo(int intelocutor, CorreoModel c)
        {
            using (CRMEntities conexion = new CRMEntities())
            {
                    try
                    {
                        conexion.USP_CORREO_REGISTRAR(intelocutor, c.Email);
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
            }
            return 1;
        }

        public int RegistrarDireccion(int interlocutor, DireccionModel d)
        {
            using (CRMEntities conexion = new CRMEntities())
            {

                    try
                    {
                        conexion.USP_DIRECCION_REGISTRAR(interlocutor, d.ID_Distrito, d.ID_Provincia, d.ID_Departamento, d.NombreDireccion);
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }

            }
            return 1;
        }
        public int RegistrarRelacion(int interlocutor, RelacionModel r)
        {
            using (CRMEntities conexion = new CRMEntities())
            {

                    try
                    {
                        conexion.USP_RELACION_REGISTRAR(interlocutor, r.IDRelacionado, r.IDRelacion);
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }

            }
            return 1;
        }
    }

}