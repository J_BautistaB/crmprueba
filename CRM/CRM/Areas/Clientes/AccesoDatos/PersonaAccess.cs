﻿using CRM.Areas.Clientes.Models;
using CRM.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Areas.Clientes.AccesoDatos
{
    public class PersonaAccess
    {
        #region Singleton

        static PersonaAccess() { }
        private PersonaAccess() { }
        private static PersonaAccess _instancia = new PersonaAccess();
        public static PersonaAccess Instancia { get { return _instancia; } }

        #endregion

        //public int RegistrarPersonaNatural(ClienteNaturalModel c)
        public int RegistrarPersonaNatural(string Nombres, string ApePaterno, string ApeMaterno, string DNI, DateTime? Nacimiento, string Genero = "", int EstadoCivil = 0, int tipoPersona = 4)
        {
            int r;
            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    conexion.USP_PERSONA_REGISTRAR(Nombres, ApePaterno, ApeMaterno, DNI, Nacimiento, Genero, EstadoCivil, null, null, null, tipoPersona);

                    var res =
                    from p in conexion.PERSONA
                    where p.DNI == DNI
                    select new
                    {
                        p.ID_PERSONA
                    };
                    r = res.FirstOrDefault().ID_PERSONA;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return r;
        }

        public int EditarPersonaNatural(int id, string Nombres, string ApePaterno, string ApeMaterno, string DNI, DateTime? Nacimiento, string Genero = "", int EstadoCivil = 0, int tipoPersona = 4)
        {
            int r;
            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    conexion.USP_PERSONA_ACTUALIZAR(id, Nombres, ApePaterno, ApeMaterno, DNI, Nacimiento, Genero, EstadoCivil, null, null, null);

                    var res =
                    from p in conexion.PERSONA
                    where p.DNI == DNI
                    select new
                    {
                        p.ID_PERSONA
                    };
                    r = res.FirstOrDefault().ID_PERSONA;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return r;
        }
        public int RegistrarPersonaEmpresa(string RazonSocial, string RUC, string RubroEmpresa, int tipoPersona = 5)
        {
            int r;
            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    conexion.USP_PERSONA_REGISTRAR(null, null, null, null, null, null, null, RazonSocial, RUC, Convert.ToInt32(RubroEmpresa), tipoPersona);

                    var res =
                    from p in conexion.PERSONA
                    where p.RUC == RUC
                    select new
                    {
                        p.ID_PERSONA
                    };
                    r = res.FirstOrDefault().ID_PERSONA;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return r;
        }

        public int EditarPersonaEmpresa(int id, string RazonSocial, string RUC, string RubroEmpresa, int tipoPersona = 5)
        {
            int r;
            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    conexion.USP_PERSONA_ACTUALIZAR(id, null, null, null, null, null, null, null, RazonSocial, RUC, Convert.ToInt32(RubroEmpresa));

                    var res =
                    from p in conexion.PERSONA
                    where p.RUC == RUC
                    select new
                    {
                        p.ID_PERSONA
                    };
                    r = res.FirstOrDefault().ID_PERSONA;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return r;
        }
    }


}