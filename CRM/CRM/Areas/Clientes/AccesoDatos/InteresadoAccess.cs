﻿using CRM.Areas.Clientes.AccesoDatos;
using CRM.Areas.Clientes.Models;
using CRM.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Transactions;

namespace CRM.Areas.Clientes
{
    public class InteresadoAccess
    {
        #region Singleton

        static InteresadoAccess() { }
        private InteresadoAccess() { }
        private static InteresadoAccess _instancia = new InteresadoAccess();
        public static InteresadoAccess Instancia { get { return _instancia; } }

        #endregion

        public InteresadoModel ObtenerInteresado(int id)
        {
            InteresadoModel c = new InteresadoModel();

            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    var item = conexion.USP_INTERLOCUTOR_COMERCIAL_OBTENER(id, null, null, null, null, null, null, true, null, null, "", null).FirstOrDefault();
                    var per = conexion.USP_PERSONA_OBTENER(item.ID_PERSONA, 0).FirstOrDefault();
                    var ltel = GeneralAccess.Instancia.ListaTelefono(id).FirstOrDefault();
                    var lcorreo = GeneralAccess.Instancia.ListaCorreo(id).FirstOrDefault();
                    var ldirec = GeneralAccess.Instancia.ListaDireccion(id).FirstOrDefault();
                    var lrel = GeneralAccess.Instancia.ListaRelacion(id).FirstOrDefault();

                    c.IdTienda = item.ID_TIENDA;
                    c.ID = id;
                    c.TipoDePersona = per.IDP_TIPO_PERSONA;

                    if (per.IDP_TIPO_PERSONA == 4)
                    {
                        c.Nombre = per.NOMBRES;
                        c.TipoDocumento = "DNI";
                    }
                    else
                    {
                        c.Nombre = per.RAZON_SOCIAL;
                        c.TipoDocumento = "RUC";
                    }

                    c.DNI = item.DNI;
                    c.RUC = item.RUC;

                    if (lcorreo != null)
                    {
                        c.Correo = lcorreo.Email;
                    }

                    if (ldirec != null)
                    {
                        c.Direccion = ldirec.NombreDireccion;
                        c.IDDepartamento = ldirec.ID_Departamento;
                        c.IDProvincia = ldirec.ID_Provincia;
                        c.IDDistrito = ldirec.ID_Distrito;
                    }

                    if (ltel != null)
                    {
                        c.IDTipoTelefono = ltel.IDTipoTelefono;
                        c.Telefono = ltel.Numero;
                    }
                }
                catch
                {
                    c = null;
                }
            }
            return c;
        }

        public List<InteresadoModel> listarInteresadosPersona(string clave)
        {
            List<InteresadoModel> list = new List<InteresadoModel>();

            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    var lista = conexion.USP_INTERLOCUTOR_COMERCIAL_OBTENER(null, null, null, null, null, null, null, true, null, null, clave, 4);
                    foreach (var item in lista)
                    {
                        InteresadoModel c = new InteresadoModel();
                        c.ID = item.ID_INTERLOCUTOR;
                        c.Nombre = item.NOMBRE;
                        c.TipoDocumento = item.TIPO_DOCUMENTO;
                        c.DNI = item.DNI;
                        list.Add(c);
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return list;
        }

        public List<InteresadoModel> listarInteresadosEmpresa(string clave)
        {
            List<InteresadoModel> list = new List<InteresadoModel>();

            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    var lista = conexion.USP_INTERLOCUTOR_COMERCIAL_OBTENER(null, null, null, null, null, null, null, true, null, null, clave, 5);
                    foreach (var item in lista)
                    {
                        InteresadoModel c = new InteresadoModel();
                        c.ID = item.ID_INTERLOCUTOR;
                        c.Nombre = item.NOMBRE;
                        c.TipoDocumento = item.TIPO_DOCUMENTO;
                        c.RUC = item.RUC;
                        list.Add(c);
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return list;
        }

        public int RegistrarInteresadoPersona(InteresadoModel c)
        {
            int interlocutor = 0;
            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    //using (TransactionScope transaction = new TransactionScope())
                    //{
                        int p = PersonaAccess.Instancia.RegistrarPersonaNatural(c.Nombre, "", "", c.DNI, null);
                        conexion.USP_INTERLOCUTOR_COMERCIAL_REGISTRAR(p, c.IdTienda, null, 2, null, null, null, true, null, null);
                        var res =
                        from i in conexion.INTERLOCUTOR_COMERCIAL
                        join pe in conexion.PERSONA on i.ID_PERSONA equals pe.ID_PERSONA
                        where pe.DNI == c.DNI
                        select new
                        {
                            i.ID_INTERLOCUTOR
                        };
                        interlocutor = res.FirstOrDefault().ID_INTERLOCUTOR;

                        if (c.Correo != null)
                        {
                            CorreoModel correo = new CorreoModel();
                            correo.Email = c.Correo;
                            GeneralAccess.Instancia.RegistrarCorreo(interlocutor, correo);
                        }
                        if (c.Telefono != null)
                        {
                            TelefonoModel fone = new TelefonoModel();
                            fone.Numero = c.Telefono;
                            fone.IDTipoTelefono = Convert.ToInt32(c.IDTipoTelefono);
                            GeneralAccess.Instancia.RegistrarTelefono(interlocutor, fone);
                        }
                        if (c.Direccion != null)
                        {
                            DireccionModel direc = new DireccionModel();
                            direc.ID_Departamento = Convert.ToInt32(c.IDDepartamento);
                            direc.ID_Distrito = Convert.ToInt32(c.IDDistrito);
                            direc.ID_Provincia = Convert.ToInt32(c.IDProvincia);
                            direc.NombreDireccion = c.Direccion;
                            GeneralAccess.Instancia.RegistrarDireccion(interlocutor, direc);
                        }
                    //    transaction.Complete();
                    //}

                    interlocutor = 0;
                }
                catch (Exception e)
                {
                    interlocutor = 1;
                    throw e;
                }
            }
            return interlocutor;
        }

        public int RegistrarInteresadoEmpresa(InteresadoModel c)
        {
            int interlocutor = 0;
            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    //using (TransactionScope transaction = new TransactionScope())
                    //{
                        int p = PersonaAccess.Instancia.RegistrarPersonaEmpresa(c.Nombre, c.RUC, null, 5);
                        conexion.USP_INTERLOCUTOR_COMERCIAL_REGISTRAR(p, 1, null, 2, null, null, null, true, null, null);
                        var res =
                        from i in conexion.INTERLOCUTOR_COMERCIAL
                        join pe in conexion.PERSONA on i.ID_PERSONA equals pe.ID_PERSONA
                        where pe.RUC == c.RUC
                        select new
                        {
                            i.ID_INTERLOCUTOR
                        };
                        interlocutor = res.FirstOrDefault().ID_INTERLOCUTOR;

                        if (c.Correo != null)
                        {
                            CorreoModel correo = new CorreoModel();
                            correo.Email = c.Correo;
                            GeneralAccess.Instancia.RegistrarCorreo(interlocutor, correo);
                        }
                        if (c.Telefono != null)
                        {
                            TelefonoModel fone = new TelefonoModel();
                            fone.Numero = c.Telefono;
                            fone.IDTipoTelefono = Convert.ToInt32(c.IDTipoTelefono);
                            GeneralAccess.Instancia.RegistrarTelefono(interlocutor, fone);
                        }
                        if (c.Direccion != null)
                        {
                            DireccionModel direc = new DireccionModel();
                            direc.ID_Departamento = Convert.ToInt32(c.IDDepartamento);
                            direc.ID_Distrito = Convert.ToInt32(c.IDDistrito);
                            direc.ID_Provincia = Convert.ToInt32(c.IDProvincia);
                            direc.NombreDireccion = c.Direccion;
                            GeneralAccess.Instancia.RegistrarDireccion(interlocutor, direc);
                        }

                        interlocutor = 0;
                    //    transaction.Complete();
                    //}
                }
                catch (Exception e)
                {
                    interlocutor = 1;
                    throw e;
                }

            }
            return interlocutor;
        }
    }

}