﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using CRM.Areas.Clientes.AccesoDatos;

namespace CRM.Areas.Clientes.Models
{
    public class EmpresaModel : ListasCombo
    {
        public EmpresaModel()
        {
            ListaComboTelefono = GeneralAccess.Instancia.ListaTipo(7);
            ListaComboRelacionEmpresa = GeneralAccess.Instancia.ListaTipo(5);
            ListaComboDepartamentos = GeneralAccess.Instancia.ListaTipo(11);
            ListaComboRubroEmpresa = GeneralAccess.Instancia.ListaTipo(6);
            ListaComboProvincias = new List<SelectListItem>();
            ListaComboDistritos = new List<SelectListItem>();

            ListaRelaciones = new List<RelacionModel>();
            ListaTelefonos = new List<TelefonoModel>();
            ListaCorreos = new List<CorreoModel>();
            ListaDirecciones = new List<DireccionModel>();
        }

        public int ID { get; set; }

        public int IdTienda { get; set; }

        [Required(ErrorMessage = "* El campo 'RUC' es obligatorio.")]
        [StringLength(11, MinimumLength = 11, ErrorMessage = "* El RUC debe tener 11 dígitos")]
        [RegularExpression(@"^\d+$", ErrorMessage = "* El campo 'RUC' solo admite números.")]
        [Display(Name = "RUC *")]
        public string RUC { get; set; }

        [Required(ErrorMessage = "* El campo 'Razón Social' es obligatorio.")]
        [Display(Name = "Razón Social *")]
        [StringLength(50, ErrorMessage = "* El número de caracteres de {0} debe ser al menos {2}.", MinimumLength = 3)]
        public string RazonSocial { get; set; }

        [Required(ErrorMessage = "* Debe seleccionar un Rubro de esta lista.")]
        public string RubroEmpresa { get; set; }

        [Url]
        [Display(Name = "Página Web")]
        public string PaginaWeb { get; set; }

        public bool Importado { get; set; }

        public IEnumerable<RelacionModel> ListaRelaciones { get; set; }

        public IEnumerable<TelefonoModel> ListaTelefonos { get; set; }

        public IEnumerable<CorreoModel> ListaCorreos { get; set; }

        public IEnumerable<DireccionModel> ListaDirecciones { get; set; }
    }
}