﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using CRM.Areas.Clientes.AccesoDatos;

namespace CRM.Areas.Clientes.Models
{
    public class ClienteNaturalModel : ListasCombo
    {
        public ClienteNaturalModel()
        {
            ListaComboGenero = ObtenerListaComboGenero();
            ListaComboTelefono = GeneralAccess.Instancia.ListaTipo(7);
            ListaComboRelacionCliente = GeneralAccess.Instancia.ListaTipo(4);
            ListaComboEstadoCivil = GeneralAccess.Instancia.ListaTipo(12);
            ListaComboDepartamentos = GeneralAccess.Instancia.ListaTipo(11);
            ListaComboProvincias = new List<SelectListItem>();
            ListaComboDistritos = new List<SelectListItem>();

            ListaRelaciones = new List<RelacionModel>();
            ListaTelefonos = new List<TelefonoModel>();
            ListaCorreos = new List<CorreoModel>();
            ListaDirecciones = new List<DireccionModel>();
        }

        public int ID { get; set; }

        public int identPersona { get; set; }

        public int IdTienda { get; set; }

        [Required(ErrorMessage = "* El campo 'DNI' es obligatorio.")]
        [StringLength(8, MinimumLength = 8, ErrorMessage = "* El DNI debe tener 8 dígitos")]
        [RegularExpression(@"^\d+$", ErrorMessage = "* El campo 'DNI' solo admite números.")]
        [Display(Name = "DNI *")]
        public string DNI { get; set; }

        [Required(ErrorMessage = "* El campo 'Nombres' es obligatorio.")]
        [Display(Name = "Nombres *")]
        [StringLength(50, ErrorMessage = "* El número de caracteres de {0} debe ser al menos {2}.", MinimumLength = 3)]
        public string Nombres { get; set; }

        [Required(ErrorMessage = "* El campo 'Apellido Paterno' es obligatorio.")]
        [Display(Name = "Apellido Paterno *")]
        [StringLength(50, ErrorMessage = "* El número de caracteres de {0} debe ser al menos {2}.", MinimumLength = 3)]
        public string ApePaterno { get; set; }

        [Required(ErrorMessage = "* El campo 'Apellido Materno' es obligatorio.")]
        [Display(Name = "Apellido Materno *")]
        [StringLength(50, ErrorMessage = "* El número de caracteres de {0} debe ser al menos {2}.", MinimumLength = 3)]
        public string ApeMaterno { get; set; }

        [Required(ErrorMessage = "* El campo 'Nacimiento' es obligatorio ")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "F. Nacimiento *")]
        public DateTime? Nacimiento { get; set; }

        [Required(ErrorMessage = "* Debe seleccionar un Género en esta lista.")]
        public string Genero { get; set; }

        [Required(ErrorMessage = "* Debe seleccionar un Estado Civil en esta lista.")]
        public string EstadoCivil { get; set; }

        public bool Importado { get; set; }

        public IEnumerable<RelacionModel> ListaRelaciones { get; set; }

        public IEnumerable<TelefonoModel> ListaTelefonos { get; set; }

        public IEnumerable<CorreoModel> ListaCorreos { get; set; }

        public IEnumerable<DireccionModel> ListaDirecciones { get; set; }

        public ClienteNaturalModel GetBase()
        {
            return this;
        }

    }
}