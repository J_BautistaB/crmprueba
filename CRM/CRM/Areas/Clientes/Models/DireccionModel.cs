﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Areas.Clientes.Models
{
    public class DireccionModel
    {
        public int ID { get; set; }

        public string NombreDireccion { get; set; }

        public int ID_Departamento { get; set; }

        public int ID_Provincia { get; set; }

        public int ID_Distrito { get; set; }

        public string Nombre_Departamento { get; set; }

        public string Nombre_Provincia { get; set; }

        public string Nombre_Distrito { get; set; }
    }

    public class Departamento
    {

        public Departamento()
        {
        }

        public Departamento(int id, string nombre)
        {
            ID = id;
            Nombre = nombre;
        }

        public int ID { get; set; }

        public string Nombre { get; set; }
    }

    public class Provincia
    {
        public Provincia()
        {
        }

        public Provincia(int id, string nombre)
        {
            ID = id;
            Nombre = nombre;
        }

        public int ID { get; set; }

        public string Nombre { get; set; }
    }

    public class Distrito
    {
        public Distrito()
        {
        }

        public Distrito(int id, string nombre)
        {
            ID = id;
            Nombre = nombre;
        }

        public int ID { get; set; }

        public string Nombre { get; set; }
    }
}