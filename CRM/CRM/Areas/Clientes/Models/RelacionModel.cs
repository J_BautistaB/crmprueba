﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Areas.Clientes.Models
{
    public class RelacionModel
    {
        public int ID { get; set; }

        public string NombreRelacion { get; set; }

        public string NombreRelacionado { get; set; }

        public int IDRelacion { get; set; }

        public int IDRelacionado { get; set; }
    }
}