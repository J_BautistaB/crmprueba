﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CRM.Areas.Clientes.Models
{
    public class InterlocutorComercialModel
    {
        public int ID { get; set; }

        [Display(Name = "Nombre/Razón Social")]
        public string Nombre { get; set; }  //RAZON SOCIAL O NOMBRE COMPLETO

        [Display(Name = "RUC/DNI")]     //SE OBTIENE EL TIPO DE PERSONA SI USA RUC O DNI (importante llenarlo)
        public string Documento { get; set; }

        [Display(Name = "Número de Documento")]
        public string NumDocumento { get; set; }

        [Display(Name = "Nacimiento")]
        public string Nacimiento { get; set; }

        [Display(Name = "Género")]
        public string Genero { get; set; }

        [Display(Name = "Estado Civil")]
        public string EstadoCivil { get; set; }

        [Display(Name = "Rubro")]
        public string RubroEmpresa { get; set; }

        //-----

        [Display(Name = "Dirección(es)")]
        public string Direccion { get; set; }

        [Display(Name = "Teléfono(s)")]
        public string Telefono { get; set; }

        [Display(Name = "Correo(s)")]
        public string Correo { get; set; }

        public int? TipoRol { get; set; }
    }
}