﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using CRM.Areas.Clientes.AccesoDatos;

namespace CRM.Areas.Clientes.Models
{
    public class InteresadoModel
    {
        public InteresadoModel()
        {
            ListaComboTipoPersona = GeneralAccess.Instancia.ListaTipo(2);
            ListaComboTelefono = GeneralAccess.Instancia.ListaTipo(7);
            ListaComboDepartamentos = GeneralAccess.Instancia.ListaTipo(11);
            ListaComboProvincias = new List<SelectListItem>();
            ListaComboDistritos = new List<SelectListItem>();
        }

        public int ID { get; set; }

        public int IdTienda { get; set; }

        [Required(ErrorMessage = "* Debe seleccionar un tipo de persona.")]
        [Display(Name = "Tipo de Persona *")]
        public int? TipoDePersona { get; set; }

        public string TipoDocumento { get; set; }

        [Required(ErrorMessage = "* Este campo es obligatorio.")]
        [Display(Name = "Nombre/Razón *")]
        [StringLength(50, ErrorMessage = "* El número de caracteres debe ser al menos {2}.", MinimumLength = 3)]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "* El campo 'DNI' es obligatorio.")]
        [StringLength(8, MinimumLength = 8, ErrorMessage = "* El DNI debe tener 8 dígitos")]
        [RegularExpression(@"^\d+$", ErrorMessage = "* Este campo solo admite números.")]
        [Display(Name = "DNI *")]
        public string DNI { get; set; }

        [Required(ErrorMessage = "* El campo 'RUC' es obligatorio.")]
        [StringLength(11, MinimumLength = 11, ErrorMessage = "* El RUC debe tener 11 dígitos")]
        [RegularExpression(@"^\d+$", ErrorMessage = "* Este campo solo admite números.")]
        [Display(Name = "RUC *")]
        public string RUC { get; set; }

        //--Dirección--
        public string Direccion { get; set; }

        //[Required(ErrorMessage = "* Debe seleccionar un Departamento.")]
        public int IDDepartamento { get; set; }

        //[Required(ErrorMessage = "* Debe seleccionar una Provincia")]
        public int IDProvincia { get; set; }

        //[Required(ErrorMessage = "* Debe seleccionar un Distrito.")]
        public int IDDistrito { get; set; }


        //--Telefono--
        [RegularExpression(@"^\d+$", ErrorMessage = "* El campo 'Teléfono' solo admite números.")]
        [Display(Name = "Teléfono")]
        public string Telefono { get; set; }

        [Required(ErrorMessage = "* Debe seleccionar un tipo de teléfono.")]
        public int? IDTipoTelefono { get; set; }

        //--Correo--
        public string Correo { get; set; }


        //*** Combos ***
        public IEnumerable<SelectListItem> ListaComboTipoPersona { get; set; }

        public IEnumerable<SelectListItem> ListaComboTelefono { get; set; }

        public IEnumerable<SelectListItem> ListaComboDepartamentos { get; set; }

        public IEnumerable<SelectListItem> ListaComboProvincias { get; set; }

        public IEnumerable<SelectListItem> ListaComboDistritos { get; set; }

        public string IsPopup { get; set; }

    }
}