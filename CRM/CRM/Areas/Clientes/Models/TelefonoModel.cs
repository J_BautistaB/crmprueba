﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Areas.Clientes.Models
{
    public class TelefonoModel
    {
        public int ID { get; set; }

        public string Numero { get; set; }

        public string TipoTelefono { get; set; }

        public int IDTipoTelefono { get; set; }

        public string Operador { get; set; }

        public int IDOperador { get; set; }
    }
}