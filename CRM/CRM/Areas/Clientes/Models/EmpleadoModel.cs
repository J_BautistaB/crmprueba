﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using CRM.Areas.Clientes.AccesoDatos;

namespace CRM.Areas.Clientes.Models
{
    public class EmpleadoModel : ClienteNaturalModel
    {
        public EmpleadoModel()
        {
            ListaComboAreaEmpleado = GeneralAccess.Instancia.ListaTipo(13);
            ListaComboTipoEmpleado = GeneralAccess.Instancia.ListaTipo(3);
        }

        [Required(ErrorMessage = "* Debe seleccionar un Tipo de Empleado")]
        public int? TipoEmpleado { get; set; }

        [Required(ErrorMessage = "* Debe seleccionar el Área del Empleado")]
        public int? AreaEmpleado { get; set; }

        public IEnumerable<SelectListItem> ListaComboTipoEmpleado { get; set; }

        public IEnumerable<SelectListItem> ListaComboAreaEmpleado { get; set; }
    }
}