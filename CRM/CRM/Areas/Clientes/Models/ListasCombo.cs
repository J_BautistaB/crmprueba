﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM.Areas.Clientes.Models
{
    public class ListasCombo
    {
        public string ComboSeleccionTelefono { get; set; }

        public string ComboSeleccionDepartamento { get; set; }

        public string ComboSeleccionProvincia { get; set; }

        public string ComboSeleccionDistrito { get; set; }

        public string ComboSeleccionRelacion { get; set; }

        //-------------------

        public IEnumerable<SelectListItem> ListaComboGenero { get; set; }

        public IEnumerable<SelectListItem> ListaComboTelefono { get; set; }

        public IEnumerable<SelectListItem> ListaComboRelacionCliente { get; set; }

        public IEnumerable<SelectListItem> ListaComboRelacionEmpresa { get; set; }

        public IEnumerable<SelectListItem> ListaComboEstadoCivil { get; set; }

        public IEnumerable<SelectListItem> ListaComboRubroEmpresa { get; set; }

        public IEnumerable<SelectListItem> ListaComboDepartamentos { get; set; }

        public IEnumerable<SelectListItem> ListaComboProvincias { get; set; }

        public IEnumerable<SelectListItem> ListaComboDistritos { get; set; }

        public static IEnumerable<SelectListItem> ObtenerListaComboGenero()
        {
            IEnumerable<SelectListItem> lista = new SelectList(new[]
                {
                    new SelectListItem{ Value = "M", Text = "Masculino" },
                    new SelectListItem{ Value = "F", Text = "Femenino" },
                }, "Value", "Text");
            return lista;
        }
    }
}