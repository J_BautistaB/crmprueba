﻿using CRM.Areas.Clientes.AccesoDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM.Areas.Clientes.Models
{
    public class BusquedaICModel
    {
        public BusquedaICModel()
        {
            ListaComboTipoRol = ObtenerListaComboRol();
            ListaComboTiendaEmpleado = ObtenerListaComboTienda();

            ListaComboAreaEmpleado = GeneralAccess.Instancia.ListaTipo(13);
            ListaComboTipoEmpleado = GeneralAccess.Instancia.ListaTipo(3);
            ListaComboCanalRegistro = GeneralAccess.Instancia.ListaTipo(1);
            ListaComboTipoPersona = GeneralAccess.Instancia.ListaTipo(2);
        }

        public string PalabraClave { get; set; }

        public int? TipoPersona { get; set; }

        public int? TipoRol { get; set; }

        public int? TiendaEmpleado { get; set; }

        public int? AreaEmpleado { get; set; }

        public int? TipoEmpleado { get; set; }

        public int? CanalRegistro { get; set; }

        public IEnumerable<SelectListItem> ListaComboTipoPersona { get; set; }

        public IEnumerable<SelectListItem> ListaComboTipoRol { get; set; }

        public IEnumerable<SelectListItem> ListaComboTiendaEmpleado { get; set; }

        public IEnumerable<SelectListItem> ListaComboAreaEmpleado { get; set; }

        public IEnumerable<SelectListItem> ListaComboTipoEmpleado { get; set; }

        public IEnumerable<SelectListItem> ListaComboCanalRegistro { get; set; }

        public static IEnumerable<SelectListItem> ObtenerListaComboRol()
        {
            IEnumerable<SelectListItem> lista = new SelectList(new[]
                {
                    new SelectListItem{ Value = "1", Text = "Cliente" },
                    new SelectListItem{ Value = "2", Text = "Empleado" },
                    new SelectListItem{ Value = "3", Text = "Interesado" },
                }, "Value", "Text");
            return lista;
        }

        public static IEnumerable<SelectListItem> ObtenerListaComboTienda()
        {
            IEnumerable<SelectListItem> lista = new SelectList(new[]
                {
                    new SelectListItem{ Value = "1", Text = "Tienda #1" },
                }, "Value", "Text");
            return lista;
        }
    }

}