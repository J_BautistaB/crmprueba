﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Areas.Clientes.Models
{
    public class CorreoModel
    {
        public int ID { get; set; }

        public string Email { get; set; }
    }
}