﻿using CRM.Areas.Clientes.AccesoDatos;
using CRM.Areas.Clientes.Models;
using log4net;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM.Areas.Clientes.Controllers
{
    public class MantenerClienteNaturalController : Controller
    {
        public ActionResult CrearCliente()
        {
            ViewData["TipoPersona"] = "Natural";
            ClienteNaturalModel c = new ClienteNaturalModel();
            c.IdTienda = 1;
            return View(c);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CrearCliente(ClienteNaturalModel c)
        {
            string jsonCliente = Request["infoJSON"];
            AgregarDetalles(c, jsonCliente);
            ViewData["TipoPersona"] = "Natural";
            if (ModelState.IsValid)
            {
                if (!c.Importado)
                {
                    ViewData["TituloCrearPost"] = "Nuevo Cliente Natural";
                    int ip = InterlocutorAccess.Instancia.RegistrarInterlocutorPersona(c);

                    if (ip != 0)
                    {
                        ViewData["CodError"] = "0";
                        ViewData["MensajeCrearPost"] = "El cliente <strong>" + c.Nombres + " " + c.ApePaterno + " " + c.ApeMaterno + "</strong> con DNI N° <strong>" + c.DNI + "</strong> se ha registrado correctamente en el sistema.";
                    }
                    else
                    {
                        ViewData["CodError"] = "1";
                        ViewData["MensajeCrearPost"] = "No se pudo guardar el nuevo cliente debido a un error interno.";
                    }
                }
                else
                {
                    ViewData["TituloCrearPost"] = "Cambio de Rol a Persona";
                    int op = InterlocutorAccess.Instancia.EditarInterlocutorPersona(c.ID, c, false, true, null);
                    if (op != 0)
                    {
                        ViewData["CodError"] = "0";
                        ViewData["MensajeCrearPost"] = "La persona <strong>" + c.Nombres + " " + c.ApePaterno + " " + c.ApeMaterno + "</strong> con DNI N° <strong>" + c.DNI + "</strong> se ha modificado correctamente en el sistema como cliente.";
                    }
                    else
                    {
                        ViewData["CodError"] = "1";
                        ViewData["MensajeCrearPost"] = "No se pudo cambiar datos a la persona debido a un error interno.";
                    }

                }
                ViewData["TipoInterlocutor"] = "Cliente";

                return View("CrearPost", c);
            }
            else
            {
                return View("CrearCliente", c);
            }
        }

        public ActionResult CrearClienteDeInteresado()
        {
            ViewData["TipoPersona"] = "Natural";
            ClienteNaturalModel c = TempData["TempCliente"] as ClienteNaturalModel;
            c.IdTienda = 1;
            return View("CrearCliente", c);
        }

        public ActionResult CrearClienteDeEmpleado()
        {
            ViewData["TipoPersona"] = "Natural";
            ClienteNaturalModel c = TempData["TempEmpleado"] as ClienteNaturalModel;
            c.IdTienda = 1;
            c.Importado = true;
            return View("CrearCliente", c);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult BuscarClientes(string clave)
        {
            List<ClienteNaturalModel> clientes = InterlocutorAccess.Instancia.ListaClientesNaturales(clave);
            return Json(clientes, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult BuscarEmpresas(string clave)
        {
            List<InterlocutorComercialModel> empresas = InterlocutorAccess.Instancia.ListaInterlocutor(5, clave);
            return Json(empresas, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditarCliente(int id)
        {
            ClienteNaturalModel c = InterlocutorAccess.Instancia.ObtenerClienteNatural(id);
            if (c != null)
            {
                return View(c);
            }
            else
            {
                ViewData["tituloError"] = "Error al editar cliente";
                ViewData["msjError"] = "No existe un cliente con identificador " + id + ".";
                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarCliente(int id, ClienteNaturalModel cn)
        {
            string jsonCliente = Request["infoJSON"];
            AgregarDetalles(cn, jsonCliente);
            if (ModelState.IsValid)
            {
                int retorno = InterlocutorAccess.Instancia.EditarInterlocutorPersona(id, cn, false, true, false);

                if (retorno != 0)
                {
                    ViewData["CodError"] = "0";
                    ViewData["MensajeEditarPost"] = "Se ha actualizado los datos del cliente <strong>" + cn.Nombres + " " + cn.ApePaterno + " " + cn.ApeMaterno + "</strong> correctamente.";
                }
                else
                {
                    ViewData["CodError"] = "1";
                    ViewData["MensajeEditarPost"] = "No se pudo guardar los cambios debido a un error interno";
                }
                ViewData["TituloEditarPost"] = "Editar Cliente Natural";

                return View("EditarPost", cn);
            }
            else
            {
                return View("EditarCliente", cn);
            }
        }

        public ActionResult DetalleCliente(int id)
        {
            ClienteNaturalModel c = InterlocutorAccess.Instancia.ObtenerClienteNatural(id);
            if (c != null)
            {
                return View(c);
            }
            else
            {
                ViewData["tituloError"] = "Error al ver cliente";
                ViewData["msjError"] = "No existe un cliente con identificador " + id + ".";
                return View("Error");
            }
        }

        //Obtener lista de provincias segun id de departamento.
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ObtenerProvicias(string iddepartamento)
        {
            List<Provincia> provincias = new List<Provincia>();
            try
            {
                int iddepa = Int32.Parse(iddepartamento);
                var prov = GeneralAccess.Instancia.ListaParametro(0, 10, iddepa, 1);
                foreach (var item in prov)
                {
                    provincias.Add(new Provincia(item.ID_PARAMETRO, item.DESCRIPCION));
                }
            }
            catch (FormatException e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return Json(provincias, JsonRequestBehavior.AllowGet);
        }

        //Obtener lista de distritos segun id de provincia.
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ObtenerDistritos(string idprovincia)
        {
            List<Distrito> distritos = new List<Distrito>();
            try
            {
                int idprov = Int32.Parse(idprovincia);
                var dis = GeneralAccess.Instancia.ListaParametro(0, 9, idprov, 1);
                foreach (var item in dis)
                {
                    distritos.Add(new Distrito(item.ID_PARAMETRO, item.DESCRIPCION));
                }
            }
            catch (FormatException e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return Json(distritos, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CambiarRolEmpleado(int id)
        {
            EmpleadoModel e = InterlocutorAccess.Instancia.ObtenerEmpleado(id);
            TempData["TempEmpleado"] = e.GetBase();
            return RedirectToAction("CrearClienteDeEmpleado", "MantenerClienteNatural", new { Area = "Clientes" });
        }

        public ActionResult PopupCrearClienteNatural()
        {
            ViewData["TipoPersona"] = "Natural";
            ClienteNaturalModel c = new ClienteNaturalModel();
            c.IdTienda = 1;
            return PartialView("_CrearCliente", c);
        }

        private void AgregarDetalles(ClienteNaturalModel c, string json)
        {
            List<RelacionModel> relaciones = new List<RelacionModel>();
            List<TelefonoModel> telefonos = new List<TelefonoModel>();
            List<CorreoModel> correos = new List<CorreoModel>();
            List<DireccionModel> direcciones = new List<DireccionModel>();

            JObject cliente = JObject.Parse(json);

            foreach (var relacion in cliente["relaciones"])
            {
                RelacionModel R = new RelacionModel()
                {
                    IDRelacion = (int)relacion["IDRelacion"],
                    IDRelacionado = (int)relacion["IDRelacionado"],
                    NombreRelacion = (string)relacion["NombreRelacion"],
                    NombreRelacionado = (string)relacion["NombreRelacionado"]
                };
                relaciones.Add(R);
            }

            foreach (var telefono in cliente["telefonos"])
            {
                TelefonoModel T = new TelefonoModel()
                {
                    Numero = (string)telefono["Numero"],
                    IDTipoTelefono = (int)telefono["IDTipoTelefono"],
                    TipoTelefono = (string)telefono["TipoTelefono"]
                };
                telefonos.Add(T);
            }

            foreach (var correo in cliente["correos"])
            {
                CorreoModel co = new CorreoModel()
                {
                    Email = (string)correo["Email"]
                };
                correos.Add(co);
            }

            foreach (var direccion in cliente["direcciones"])
            {
                DireccionModel D = new DireccionModel()
                {
                    NombreDireccion = (string)direccion["NombreDireccion"],
                    Nombre_Departamento = (string)direccion["NombreDepartamento"],
                    Nombre_Provincia = (string)direccion["NombreProvincia"],
                    Nombre_Distrito = (string)direccion["NombreDistrito"],
                    ID_Departamento = (int)direccion["IDDepartamento"],
                    ID_Provincia = (int)direccion["IDProvincia"],
                    ID_Distrito = (int)direccion["IDDistrito"]
                };
                direcciones.Add(D);
            }

            c.ListaRelaciones = relaciones;
            c.ListaTelefonos = telefonos;
            c.ListaCorreos = correos;
            c.ListaDirecciones = direcciones;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ObtenerClienteJson(int id)
        {
            ClienteNaturalModel c = InterlocutorAccess.Instancia.ObtenerClienteNatural(id);
            return Json(c, JsonRequestBehavior.AllowGet);
        }
    }

}
