﻿using CRM.Areas.Clientes.AccesoDatos;
using CRM.Areas.Clientes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM.Areas.Clientes.Controllers
{
    public class MantenerInteresadoController : Controller
    {
        public ActionResult CrearInteresado()
        {
            InteresadoModel i = new InteresadoModel();
            i.IsPopup = "0";
            i.IdTienda = 1;
            return View(i);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CrearInteresado(InteresadoModel i)
        {
            ModificarModelState(i, ModelState);

            if (ModelState.IsValid)
            {

                if (i.TipoDePersona == 4)
                {
                    int resp = InteresadoAccess.Instancia.RegistrarInteresadoPersona(i);
                    ViewData["CodError"] = Convert.ToString(resp);
                }
                else
                {
                    int emp = InteresadoAccess.Instancia.RegistrarInteresadoEmpresa(i);
                    ViewData["CodError"] = Convert.ToString(emp);
                }

                if (i.IsPopup.Equals("1"))
                {
                    return PartialView("PopupCrearInteresadoPost", i);
                }
                else
                {
                    return View("CrearInteresadoPost", i);
                }
            }
            else
            {
                if (i.IsPopup.Equals("1"))
                {
                    return PartialView("PopupCrearInteresado", i);
                }
                else
                {
                    return View("CrearInteresado", i);
                }
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult BuscarInteresado(string clave, string tipo)
        {
            List<InteresadoModel> interesados = new List<InteresadoModel>();
            switch (tipo)
            {
                case "Natural":
                    interesados = InteresadoAccess.Instancia.listarInteresadosPersona(clave);
                    break;
                case "Empresa":
                    interesados = InteresadoAccess.Instancia.listarInteresadosEmpresa(clave);
                    break;
                default:
                    break;
            }
            return Json(interesados, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EditarInteresado(int id, string tipo)
        {
            InteresadoModel i = InteresadoAccess.Instancia.ObtenerInteresado(id);

            #region ObtenerDetalles
            List<CorreoModel> correos = new List<CorreoModel>();
            if (i.Correo != null)
            {
                correos.Add(new CorreoModel
                {
                    Email = i.Correo
                });
            }

            List<TelefonoModel> telefonos = new List<TelefonoModel>();
            if (i.Telefono != null)
            {
                telefonos.Add(new TelefonoModel
                {
                    Numero = i.Telefono,
                    IDTipoTelefono = (int)i.IDTipoTelefono
                });
            }

            List<DireccionModel> direcciones = new List<DireccionModel>();
            if (i.Direccion != null)
            {
                direcciones.Add(new DireccionModel
                {
                    NombreDireccion = i.Direccion,
                    ID_Departamento = i.IDDepartamento,
                    ID_Provincia = i.IDProvincia,
                    ID_Distrito = i.IDDistrito
                });
            }
            #endregion
            
            switch (tipo)
            {
                case "Natural":
                    ClienteNaturalModel c = new ClienteNaturalModel
                    {
                        ID = i.ID,
                        Nombres = i.Nombre,
                        IdTienda = i.IdTienda,
                        DNI = i.DNI,
                        ListaCorreos = correos,
                        ListaDirecciones = direcciones,
                        ListaTelefonos = telefonos
                    };
                    c.Importado = true;
                    TempData["TempCliente"] = c;
                    return RedirectToAction("CrearClienteDeInteresado", "MantenerClienteNatural", new { Area = "Clientes" });
                case "Empresa":

                    EmpresaModel e = new EmpresaModel
                    {
                        ID = i.ID,
                        RazonSocial = i.Nombre,
                        IdTienda = i.IdTienda,
                        RUC = i.RUC,
                        ListaCorreos = correos,
                        ListaDirecciones = direcciones,
                        ListaTelefonos = telefonos
                    };
                    e.Importado = true;
                    TempData["TempEmpresa"] = e;
                    return RedirectToAction("CrearEmpresaDeInteresado", "MantenerEmpresa", new { Area = "Clientes" });
                default:
                    return RedirectToAction("Index");
            }
        }

        public ActionResult DetalleInteresado(int id)
        {
            InteresadoModel i = InteresadoAccess.Instancia.ObtenerInteresado(id);
            if (i != null)
            {
                return View(i);
            }
            else
            {
                ViewData["tituloError"] = "Error al ver interesado";
                ViewData["msjError"] = "No existe un interesado con identificador " + id + ".";
                return View("Error");
            }
        }

        public ActionResult PopupCrearInteresado()
        {
            InteresadoModel i = new InteresadoModel();
            i.IsPopup = "1";
            i.IdTienda = 1;
            return PartialView(i);
        }

        public ActionResult PopupImportarInteresado()
        {
            return View();
        }

        private void ModificarModelState(InteresadoModel i, ModelStateDictionary ms)
        {
            if (i.TipoDePersona != null)
            {
                switch (i.TipoDePersona)
                {
                    case 4:
                        i.TipoDocumento = "DNI";
                        ModelState.Remove("RUC");
                        break;
                    case 5:
                        i.TipoDocumento = "RUC";
                        ModelState.Remove("DNI");
                        break;
                    default:
                        break;
                }
                if (i.Telefono == null)
                {
                    ModelState.Remove("IDTipoTelefono");
                }

                if (string.IsNullOrWhiteSpace(i.Direccion))
                {
                    ModelState.Remove("IDDepartamento");
                    ModelState.Remove("IDProvincia");
                    ModelState.Remove("IDDistrito");
                }
            }
            else
            {
                ModelState.Remove("IDTipoTelefono");
                ModelState.Remove("IDDepartamento");
                ModelState.Remove("IDProvincia");
                ModelState.Remove("IDDistrito");
            }
        }
    }
}
