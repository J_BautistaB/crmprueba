﻿using CRM.Areas.Clientes.AccesoDatos;
using CRM.Areas.Clientes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM.Areas.Clientes.Controllers
{
    public class ConsultarInterlocutorController : Controller
    {
        public ActionResult Index()
        {
            InterlocutorComercialModel ic = new InterlocutorComercialModel();
            return View(ic);
        }

        [HttpPost]
        public ActionResult ResultadoBusqueda(BusquedaICModel B)
        {
            List<InterlocutorComercialModel> listaIC = new List<InterlocutorComercialModel>();

            string clave = B.PalabraClave;

            if (clave == null)
            {
                ViewData["CantResultados"] = "-1";
                ViewData["MensajeResultado"] = "Debe ingresar una palabra clave para buscar.";
            }
            else
            {
                clave = clave.Trim();
                if (clave.Length == 0)
                {
                    B.PalabraClave = null;
                    ViewData["CantResultados"] = "-1";
                    ViewData["MensajeResultado"] = "Debe ingresar una palabra clave para buscar..";
                }
                else
                {
                    if (B.TipoPersona == null)
                    {
                        ViewData["CantResultados"] = "-1";
                        ViewData["MensajeResultado"] = "Debe seleccionar un TIPO DE PERSONA.";
                    }
                    else
                    {
                        if (B.TipoPersona == 4 && B.TipoRol == null)
                        {
                            ViewData["CantResultados"] = "-1";
                            ViewData["MensajeResultado"] = "Debe seleccionar un Rol para buscar por persona natural.";
                        }
                        else
                        {
                            //int tipo = Convert.ToInt32(B.TipoPersona);
                            listaIC = InterlocutorAccess.Instancia.ListaInterlocutor(B);

                            if (listaIC.Count == 0)
                            {
                                ViewData["CantResultados"] = "0";
                                ViewData["MensajeResultado"] = "No se han encontrado resultados con palabra clave '" + clave + "' y con los criterios de búsqueda seleccionados.";
                            }
                            else
                            {
                                ViewData["CantResultados"] = listaIC.Count.ToString();
                                ViewData["MensajeResultado"] = "Se han encontrado " + listaIC.Count + " resultado(s):";
                            }
                        }
                    }
                }
            }

            ViewData["objBusqueda"] = B;

            return View(listaIC);
        }
    }
}
