﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM.Areas.Clientes.Controllers
{
    public class InicioController : Controller
    {
        //private static readonly ILog log = log4net.LogManager.GetLogger(typeof(InicioController));

        public ActionResult Index()
        {
            //log.Info("Se ha ingresado a la página principal del módulo clientes");
            return View();
        }
    }
}
