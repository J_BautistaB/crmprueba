﻿using CRM.Areas.Clientes.AccesoDatos;
using CRM.Areas.Clientes.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM.Areas.Clientes.Controllers
{
    public class MantenerEmpresaController : Controller
    {
        public ActionResult CrearEmpresa()
        {
            ViewData["TipoPersona"] = "Empresa";
            EmpresaModel e = new EmpresaModel();
            e.IdTienda = 1;
            return View(e);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CrearEmpresa(EmpresaModel e)
        {
            string jsonEmpresa = Request["infoJSON"];
            AgregarDetalles(e, jsonEmpresa);
            ViewData["TipoPersona"] = "Empresa";
            if (ModelState.IsValid)
            {
                if (!e.Importado)
                {
                    ViewData["TituloCrearPost"] = "Nueva Empresa";
                    int retorno = InterlocutorAccess.Instancia.RegistrarInterlocutorEmpresa(e);

                    if (retorno != 0)
                    {
                        ViewData["CodError"] = "0";
                        ViewData["MensajeCrearPost"] = "La empresa <strong>" + e.RazonSocial + "</strong> con RUC N° <strong>" + e.RUC + "</strong> se ha registrado correctamente en el sistema.";
                    }
                    else
                    {
                        ViewData["CodError"] = "1";
                        ViewData["MensajeCrearPost"] = "No se pudo guardar los datos de la empresa debido a un error interno.";
                    }
                }
                else
                {
                    ViewData["TituloCrearPost"] = "Cambio de Rol a Empresa";
                    int op = InterlocutorAccess.Instancia.EditarInterlocutorEmpresa(e.ID, e, false, true);
                    if (op != 0)
                    {
                        ViewData["CodError"] = "0";
                        ViewData["MensajeCrearPost"] = "La empresa interesada <strong>" + e.RazonSocial + "</strong> con RUC N° <strong>" + e.RUC + "</strong> se ha modificado correctamente en el sistema como cliente.";
                    }
                    else
                    {
                        ViewData["CodError"] = "1";
                        ViewData["MensajeCrearPost"] = "No se pudo cambiar datos a la empresa debido a un error interno.";
                    }
                }
                ViewData["TipoInterlocutor"] = "Empresa";

                return View("CrearPost", e);
            }
            else
            {
                return View("CrearEmpresa", e);
            }
        }

        public ActionResult CrearEmpresaDeInteresado()
        {
            ViewData["TipoPersona"] = "Empresa";
            EmpresaModel e = TempData["TempEmpresa"] as EmpresaModel;
            e.IdTienda = 1;
            return View("CrearEmpresa", e);
        }

        public ActionResult EditarEmpresa(int id)
        {
            EmpresaModel e = InterlocutorAccess.Instancia.ObtenerEmpresa(id);
            return View(e);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarEmpresa(int id, EmpresaModel e)
        {
            string jsonEmpresa = Request["infoJSON"];
            AgregarDetalles(e, jsonEmpresa);
            if (ModelState.IsValid)
            {
                int retorno = InterlocutorAccess.Instancia.EditarInterlocutorEmpresa(id, e, false, true);

                if (retorno != 0)
                {
                    ViewData["CodError"] = "0";
                    ViewData["MensajeEditarPost"] = "Se ha actualizado los datos de la empresa <strong>" + e.RazonSocial + "</strong> correctamente.";
                }
                else
                {
                    ViewData["CodError"] = "1";
                    ViewData["MensajeEditarPost"] = "No se pudo guardar los cambios debido a un error interno";
                }
                ViewData["TituloEditarPost"] = "Editar Empresa";

                return View("EditarPost", e);
            }
            else
            {
                return View("EditarEmpresa", e);
            }
        }

        public ActionResult DetalleEmpresa(int id)
        {
            EmpresaModel e = InterlocutorAccess.Instancia.ObtenerEmpresa(id);

            if (e != null)
            {
                return View(e);
            }
            else
            {
                ViewData["tituloError"] = "Error al ver empresa";
                ViewData["msjError"] = "No existe una empresa con identificador " + id + ".";
                return View("Error");
            }
        }

        public ActionResult PopupCrearEmpresa()
        {
            ViewData["TipoPersona"] = "Empresa";
            EmpresaModel e = new EmpresaModel();
            e.IdTienda = 1;
            return PartialView("_CrearEmpresa", e);
        }

        private void AgregarDetalles(EmpresaModel e, string json)
        {
            List<RelacionModel> relaciones = new List<RelacionModel>();
            List<TelefonoModel> telefonos = new List<TelefonoModel>();
            List<CorreoModel> correos = new List<CorreoModel>();
            List<DireccionModel> direcciones = new List<DireccionModel>();

            JObject cliente = JObject.Parse(json);

            foreach (var relacion in cliente["relaciones"])
            {
                RelacionModel R = new RelacionModel()
                {
                    IDRelacion = (int)relacion["IDRelacion"],
                    IDRelacionado = (int)relacion["IDRelacionado"],
                    NombreRelacion = (string)relacion["NombreRelacion"],
                    NombreRelacionado = (string)relacion["NombreRelacionado"]
                };
                relaciones.Add(R);
            }

            foreach (var telefono in cliente["telefonos"])
            {
                TelefonoModel T = new TelefonoModel()
                {
                    Numero = (string)telefono["Numero"],
                    IDTipoTelefono = (int)telefono["IDTipoTelefono"],
                    TipoTelefono = (string)telefono["TipoTelefono"]
                };
                telefonos.Add(T);
            }

            foreach (var correo in cliente["correos"])
            {
                CorreoModel co = new CorreoModel()
                {
                    Email = (string)correo["Email"]
                };
                correos.Add(co);
            }

            foreach (var direccion in cliente["direcciones"])
            {
                DireccionModel D = new DireccionModel()
                {
                    NombreDireccion = (string)direccion["NombreDireccion"],
                    Nombre_Departamento = (string)direccion["NombreDepartamento"],
                    Nombre_Provincia = (string)direccion["NombreProvincia"],
                    Nombre_Distrito = (string)direccion["NombreDistrito"],
                    ID_Departamento = (int)direccion["IDDepartamento"],
                    ID_Provincia = (int)direccion["IDProvincia"],
                    ID_Distrito = (int)direccion["IDDistrito"]
                };
                direcciones.Add(D);
            }

            e.ListaRelaciones = relaciones;
            e.ListaTelefonos = telefonos;
            e.ListaCorreos = correos;
            e.ListaDirecciones = direcciones;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ObtenerEmpresaJson(int id)
        {
            EmpresaModel e = InterlocutorAccess.Instancia.ObtenerEmpresa(id);
            return Json(e, JsonRequestBehavior.AllowGet);
        }
    }
}
