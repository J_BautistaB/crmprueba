﻿using CRM.Areas.Clientes.AccesoDatos;
using CRM.Areas.Clientes.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM.Areas.Clientes.Controllers
{
    public class MantenerEmpleadoController : Controller
    {
        public ActionResult CrearEmpleado()
        {
            EmpleadoModel e = new EmpleadoModel();
            e.IdTienda = 1;
            return View(e);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CrearEmpleado(EmpleadoModel e)
        {
            string jsonEmpleado = Request["infoJSON"];
            AgregarDetalles(e, jsonEmpleado);

            if (ModelState.IsValid)
            {
                int retorno = InterlocutorAccess.Instancia.RegistrarInterlocutorEmpleado(e);

                if (retorno != 0)
                {
                    ViewData["CodError"] = "0";
                    ViewData["MensajeCrearPost"] = "El empleado <strong>" + e.Nombres + " " + e.ApePaterno + " " + e.ApeMaterno + "</strong> con DNI N° <strong>" + e.DNI + "</strong> se ha registrado correctamente en el sistema.";
                }
                else
                {
                    ViewData["CodError"] = "1";
                    ViewData["MensajeCrearPost"] = "No se pudo guardar los datos del empleado debido a un error interno.";
                }
                ViewData["TituloCrearPost"] = "Nuevo Empleado";
                ViewData["TipoInterlocutor"] = "Empleado";
                return View("CrearPost", e);
            }
            else
            {
                return View("CrearEmpleado", e);
            }
        }

        public ActionResult EditarEmpleado(int id)
        {
            EmpleadoModel e = InterlocutorAccess.Instancia.ObtenerEmpleado(id);
            if (e != null)
            {
                return View(e);
            }
            else
            {
                ViewData["tituloError"] = "Error al editar empleado";
                ViewData["msjError"] = "No existe un empleado con identificador " + id + ".";
                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarEmpleado(int id, EmpleadoModel e)
        {
            string jsonEmpleado = Request["infoJSON"];
            AgregarDetalles(e, jsonEmpleado);
            if (ModelState.IsValid)
            {
                int retorno = InterlocutorAccess.Instancia.EditarInterlocutorEmpleado(id, e);

                if (retorno != 0)
                {
                    ViewData["CodError"] = "0";
                    ViewData["MensajeEditarPost"] = "Se ha actualizado los datos del empleado <strong>" + e.Nombres + " " + e.ApePaterno + " " + e.ApeMaterno + "</strong> correctamente.";
                }
                else
                {
                    ViewData["CodError"] = "1";
                    ViewData["MensajeEditarPost"] = "No se pudo guardar los cambios debido a un error interno";
                }
                ViewData["TituloEditarPost"] = "Editar Empleado";
                return View("EditarPost", e);
            }
            else
            {
                return View("EditarEmpleado", e);
            }
        }

        public ActionResult DetalleEmpleado(int id)
        {
            EmpleadoModel e = InterlocutorAccess.Instancia.ObtenerEmpleado(id);
            if (e != null)
            {
                return View(e);
            }
            else
            {
                ViewData["tituloError"] = "Error al ver empleado";
                ViewData["msjError"] = "No existe un empleado con identificador " + id + ".";
                return View("Error");
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult BuscarEmpleado(string clave)
        {
            List<EmpleadoModel> empleados = InterlocutorAccess.Instancia.ListaEmpleados(clave);
            return Json(empleados, JsonRequestBehavior.AllowGet);
        }

        private void AgregarDetalles(EmpleadoModel e, string json)
        {
            List<RelacionModel> relaciones = new List<RelacionModel>();
            List<TelefonoModel> telefonos = new List<TelefonoModel>();
            List<CorreoModel> correos = new List<CorreoModel>();
            List<DireccionModel> direcciones = new List<DireccionModel>();

            JObject cliente = JObject.Parse(json);

            foreach (var relacion in cliente["relaciones"])
            {
                RelacionModel R = new RelacionModel()
                {
                    IDRelacion = (int)relacion["IDRelacion"],
                    IDRelacionado = (int)relacion["IDRelacionado"],
                    NombreRelacion = (string)relacion["NombreRelacion"],
                    NombreRelacionado = (string)relacion["NombreRelacionado"]
                };
                relaciones.Add(R);
            }

            foreach (var telefono in cliente["telefonos"])
            {
                TelefonoModel T = new TelefonoModel()
                {
                    Numero = (string)telefono["Numero"],
                    IDTipoTelefono = (int)telefono["IDTipoTelefono"],
                    TipoTelefono = (string)telefono["TipoTelefono"]
                };
                telefonos.Add(T);
            }

            foreach (var correo in cliente["correos"])
            {
                CorreoModel co = new CorreoModel()
                {
                    Email = (string)correo["Email"]
                };
                correos.Add(co);
            }

            foreach (var direccion in cliente["direcciones"])
            {
                DireccionModel D = new DireccionModel()
                {
                    NombreDireccion = (string)direccion["NombreDireccion"],
                    Nombre_Departamento = (string)direccion["NombreDepartamento"],
                    Nombre_Provincia = (string)direccion["NombreProvincia"],
                    Nombre_Distrito = (string)direccion["NombreDistrito"],
                    ID_Departamento = (int)direccion["IDDepartamento"],
                    ID_Provincia = (int)direccion["IDProvincia"],
                    ID_Distrito = (int)direccion["IDDistrito"]
                };
                direcciones.Add(D);
            }

            e.ListaRelaciones = relaciones;
            e.ListaTelefonos = telefonos;
            e.ListaCorreos = correos;
            e.ListaDirecciones = direcciones;
        }
    }

}
