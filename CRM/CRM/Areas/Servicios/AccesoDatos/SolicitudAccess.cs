﻿using CRM.Areas.Servicios.Models;
using CRM.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Areas.Servicios.AccesoDatos
{
    public class SolicitudAccess
    {
        #region Singleton

        static SolicitudAccess() { }
        private SolicitudAccess() { }
        private static SolicitudAccess _instancia = new SolicitudAccess();
        public static SolicitudAccess Instancia { get { return _instancia; } }

        #endregion

        public int RegistrarSolicitud(OPERACIONES c)
        {
            int ident = 0;
            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    conexion.OPERACIONES.Add(c);
                    conexion.SaveChanges();
                    ident = c.ID_OPERACIÓN;
                }
                catch (Exception e)
                {

                }
            }

            return ident;
        }


        // Obtener Solicitud
        public ReclamoViewModel ObtenerSolicitud(int idr)
        {
            List<ReclamoViewModel> solicitudes = new List<ReclamoViewModel>();

            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    var lista = (from ope in conexion.OPERACIONES
                                 join cat in conexion.CATEGORÍA_OPERACIÓN on ope.ID_CATEGORÍA equals cat.ID_CATEGORÍA
                                 join tip in conexion.TIPO_OPERACIÓN on ope.ID_TIPO equals tip.ID_TIPO
                                 where (ope.ID_TIPO == 3)
                                 && (ope.ID_OPERACIÓN == idr)
                                 select new
                                 {
                                     ope.ID_OPERACIÓN,
                                     ope.ID_PEDIDO,
                                     ope.DESCRIPCIÓN,
                                     ope.FECHA_REGISTRO,
                                     ope.ESTADO_OPERACIÓN,
                                     ope.SOLUCIÓN,
                                     ope.NOM_CLIENTE,
                                     ope.TELÉFONO,
                                     ope.ASESOR_RESPONSABLE,
                                     ope.DIRECCIÓN,
                                     ope.FECHA_REMISIÓN,
                                     ope.FECHA_PLAZO,
                                     ope.FECHA_RESPUESTA,
                                     ope.ESTADO_RESPUESTA,
                                     ope.R_MONTO_RECLAMADO,
                                     ope.R_CÓDIGO_RECLAMADO,
                                     ope.DNI_RUC,
                                     ope.AREA_INVOLUCRADA,
                                     ope.SEDE_INVOLUCRADA,
                                     ope.ID_TIPO,
                                     ope.ID_CATEGORÍA,
                                     cat.NOMBRE_CATEGORÍA,
                                     cat.DESCRIPCIÓN_CATEGORÍA
                                 }).ToList();

                    foreach (var item in lista)
                    {
                        ReclamoViewModel CL = new ReclamoViewModel();

                        CL.ID_OPERACIÓN = item.ID_OPERACIÓN;
                        CL.ID_PEDIDO = item.ID_PEDIDO;
                        CL.DESCRIPCIÓN = item.DESCRIPCIÓN;
                        CL.FECHA_REGISTRO = item.FECHA_REGISTRO;
                        CL.ESTADO_OPERACIÓN = item.ESTADO_OPERACIÓN;
                        CL.SOLUCIÓN = item.SOLUCIÓN;
                        CL.NOM_CLIENTE = item.NOM_CLIENTE;
                        CL.TELÉFONO = item.TELÉFONO;
                        CL.ASESOR_RESPONSABLE = item.ASESOR_RESPONSABLE;
                        CL.DIRECCIÓN = item.DIRECCIÓN;
                        CL.FECHA_REMISIÓN = item.FECHA_REMISIÓN;
                        CL.FECHA_PLAZO = item.FECHA_PLAZO;
                        CL.FECHA_RESPUESTA = item.FECHA_RESPUESTA;
                        CL.ESTADO_RESPUESTA = item.ESTADO_RESPUESTA;
                        CL.R_MONTO_RECLAMADO = item.R_MONTO_RECLAMADO;
                        CL.R_CÓDIGO_RECLAMADO = item.R_CÓDIGO_RECLAMADO;
                        CL.DNI_RUC = item.DNI_RUC;
                        CL.AREA_INVOLUCRADA = item.AREA_INVOLUCRADA;
                        CL.SEDE_INVOLUCRADA = item.SEDE_INVOLUCRADA;
                        CL.ID_TIPO = item.ID_TIPO;
                        CL.ID_CATEGORÍA = item.ID_CATEGORÍA;
                        CL.nombre_categoria = item.NOMBRE_CATEGORÍA;
                        CL.descripcion_nombre_categoria = item.DESCRIPCIÓN_CATEGORÍA;

                        solicitudes.Add(CL);
                    }

                }
                catch (Exception e)
                {

                }
            }

            return solicitudes.ElementAt(0);


        }

        public int TratarSolicitud(OPERACIONES op)
        {
            int ide = 0;
            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    var actualizar = conexion.OPERACIONES.Where(cambio => cambio.ID_OPERACIÓN == op.ID_OPERACIÓN).FirstOrDefault();
                    actualizar.ASESOR_RESPONSABLE = op.ASESOR_RESPONSABLE;
                    actualizar.SOLUCIÓN = op.SOLUCIÓN;
                    actualizar.ESTADO_OPERACIÓN = op.ESTADO_OPERACIÓN;
                    conexion.SaveChanges();
                    ide = op.ID_OPERACIÓN;
                }
                catch (Exception e)
                {

                }
            }

            return ide;
        }

        // Listar categorias de Solicitudes
        public List<CATEGORÍA_OPERACIÓN> ListaCategoria(string busqueda)
        {
            List<CATEGORÍA_OPERACIÓN> parametros = new List<CATEGORÍA_OPERACIÓN>();
            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    var lista = (from p in conexion.CATEGORÍA_OPERACIÓN
                                 where ((p.NOMBRE_CATEGORÍA).Contains(busqueda))
                                 select new
                                 {
                                     p.ID_CATEGORÍA,
                                     p.NOMBRE_CATEGORÍA,
                                     p.DESCRIPCIÓN_CATEGORÍA
                                 }).ToList();

                    foreach (var item in lista)
                    {
                        CATEGORÍA_OPERACIÓN par = new CATEGORÍA_OPERACIÓN();
                        par.ID_CATEGORÍA = item.ID_CATEGORÍA;
                        par.NOMBRE_CATEGORÍA = item.NOMBRE_CATEGORÍA;
                        par.DESCRIPCIÓN_CATEGORÍA = item.DESCRIPCIÓN_CATEGORÍA;
                        parametros.Add(par);
                    }

                }
                catch (Exception e)
                {

                }
            }
            return parametros;
        }

        // Listar Datos sin filtro Solicitudes

        public List<ReclamoViewModel> ListarSolicitudes()
        {
            List<ReclamoViewModel> solicitudes = new List<ReclamoViewModel>();

            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    var lista = (from ope in conexion.OPERACIONES
                                 join cat in conexion.CATEGORÍA_OPERACIÓN on ope.ID_CATEGORÍA equals cat.ID_CATEGORÍA
                                 join tip in conexion.TIPO_OPERACIÓN on ope.ID_TIPO equals tip.ID_TIPO
                                 where (ope.ID_TIPO == 3)

                                 select new
                                 {
                                     ope.ID_OPERACIÓN,
                                     ope.ID_PEDIDO,
                                     ope.DESCRIPCIÓN,
                                     ope.FECHA_REGISTRO,
                                     ope.ESTADO_OPERACIÓN,
                                     ope.SOLUCIÓN,
                                     ope.NOM_CLIENTE,
                                     ope.TELÉFONO,
                                     ope.ASESOR_RESPONSABLE,
                                     ope.DIRECCIÓN,
                                     ope.FECHA_REMISIÓN,
                                     ope.FECHA_PLAZO,
                                     ope.FECHA_RESPUESTA,
                                     ope.ESTADO_RESPUESTA,
                                     ope.R_MONTO_RECLAMADO,
                                     ope.R_CÓDIGO_RECLAMADO,
                                     ope.DNI_RUC,
                                     ope.AREA_INVOLUCRADA,
                                     ope.SEDE_INVOLUCRADA,
                                     ope.ID_TIPO,
                                     ope.ID_CATEGORÍA,
                                     cat.NOMBRE_CATEGORÍA
                                 }).ToList();

                    foreach (var item in lista)
                    {
                        ReclamoViewModel CL = new ReclamoViewModel();

                        CL.ID_OPERACIÓN = item.ID_OPERACIÓN;
                        CL.ID_PEDIDO = item.ID_PEDIDO;
                        CL.DESCRIPCIÓN = item.DESCRIPCIÓN;
                        CL.FECHA_REGISTRO = item.FECHA_REGISTRO;
                        CL.ESTADO_OPERACIÓN = item.ESTADO_OPERACIÓN;
                        CL.SOLUCIÓN = item.SOLUCIÓN;
                        CL.NOM_CLIENTE = item.NOM_CLIENTE;
                        CL.TELÉFONO = item.TELÉFONO;
                        CL.ASESOR_RESPONSABLE = item.ASESOR_RESPONSABLE;
                        CL.DIRECCIÓN = item.DIRECCIÓN;
                        CL.FECHA_REMISIÓN = item.FECHA_REMISIÓN;
                        CL.FECHA_PLAZO = item.FECHA_PLAZO;
                        CL.FECHA_RESPUESTA = item.FECHA_RESPUESTA;
                        CL.ESTADO_RESPUESTA = item.ESTADO_RESPUESTA;
                        CL.R_MONTO_RECLAMADO = item.R_MONTO_RECLAMADO;
                        CL.R_CÓDIGO_RECLAMADO = item.R_CÓDIGO_RECLAMADO;
                        CL.DNI_RUC = item.DNI_RUC;
                        CL.AREA_INVOLUCRADA = item.AREA_INVOLUCRADA;
                        CL.SEDE_INVOLUCRADA = item.SEDE_INVOLUCRADA;
                        CL.ID_TIPO = item.ID_TIPO;
                        CL.ID_CATEGORÍA = item.ID_CATEGORÍA;
                        CL.nombre_categoria = item.NOMBRE_CATEGORÍA;

                        solicitudes.Add(CL);
                    }

                }
                catch (Exception e)
                {

                }
            }

            return solicitudes;
        }



        // Listar Datos sin filtro Solicitudes

        public List<ReclamoViewModel> ListarSolicitudFiltro(string busqueda)
        {

            if (busqueda == null)
            {
                busqueda = "Solicitud";
            }
            else
            {
                // Queda con el mismo filtro;
            }

            List<ReclamoViewModel> solicitudes = new List<ReclamoViewModel>();

            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    var lista = (from ope in conexion.OPERACIONES
                                 join cat in conexion.CATEGORÍA_OPERACIÓN on ope.ID_CATEGORÍA equals cat.ID_CATEGORÍA
                                 join tip in conexion.TIPO_OPERACIÓN on ope.ID_TIPO equals tip.ID_TIPO
                                 where (ope.ID_TIPO == 3)
                                 && ((ope.NOM_CLIENTE + " " + ope.SEDE_INVOLUCRADA + " " + cat.NOMBRE_CATEGORÍA).Contains(busqueda) || busqueda == "" || tip.NOMBRE_TIPO == busqueda)
                                 select new
                                 {
                                     ope.ID_OPERACIÓN,
                                     ope.ID_PEDIDO,
                                     ope.DESCRIPCIÓN,
                                     ope.FECHA_REGISTRO,
                                     ope.ESTADO_OPERACIÓN,
                                     ope.SOLUCIÓN,
                                     ope.NOM_CLIENTE,
                                     ope.TELÉFONO,
                                     ope.ASESOR_RESPONSABLE,
                                     ope.DIRECCIÓN,
                                     ope.FECHA_REMISIÓN,
                                     ope.FECHA_PLAZO,
                                     ope.FECHA_RESPUESTA,
                                     ope.ESTADO_RESPUESTA,
                                     ope.R_MONTO_RECLAMADO,
                                     ope.R_CÓDIGO_RECLAMADO,
                                     ope.DNI_RUC,
                                     ope.AREA_INVOLUCRADA,
                                     ope.SEDE_INVOLUCRADA,
                                     ope.ID_TIPO,
                                     ope.ID_CATEGORÍA,
                                     cat.NOMBRE_CATEGORÍA
                                 }).ToList();

                    foreach (var item in lista)
                    {
                        ReclamoViewModel CL = new ReclamoViewModel();

                        CL.ID_OPERACIÓN = item.ID_OPERACIÓN;
                        CL.ID_PEDIDO = item.ID_PEDIDO;
                        CL.DESCRIPCIÓN = item.DESCRIPCIÓN;
                        CL.FECHA_REGISTRO = item.FECHA_REGISTRO;
                        CL.ESTADO_OPERACIÓN = item.ESTADO_OPERACIÓN;
                        CL.SOLUCIÓN = item.SOLUCIÓN;
                        CL.NOM_CLIENTE = item.NOM_CLIENTE;
                        CL.TELÉFONO = item.TELÉFONO;
                        CL.ASESOR_RESPONSABLE = item.ASESOR_RESPONSABLE;
                        CL.DIRECCIÓN = item.DIRECCIÓN;
                        CL.FECHA_REMISIÓN = item.FECHA_REMISIÓN;
                        CL.FECHA_PLAZO = item.FECHA_PLAZO;
                        CL.FECHA_RESPUESTA = item.FECHA_RESPUESTA;
                        CL.ESTADO_RESPUESTA = item.ESTADO_RESPUESTA;
                        CL.R_MONTO_RECLAMADO = item.R_MONTO_RECLAMADO;
                        CL.R_CÓDIGO_RECLAMADO = item.R_CÓDIGO_RECLAMADO;
                        CL.DNI_RUC = item.DNI_RUC;
                        CL.AREA_INVOLUCRADA = item.AREA_INVOLUCRADA;
                        CL.SEDE_INVOLUCRADA = item.SEDE_INVOLUCRADA;
                        CL.ID_TIPO = item.ID_TIPO;
                        CL.ID_CATEGORÍA = item.ID_CATEGORÍA;
                        CL.nombre_categoria = item.NOMBRE_CATEGORÍA;

                        solicitudes.Add(CL);
                    }

                }
                catch (Exception e)
                {

                }
            }

            return solicitudes;
        }

        public List<string> lista_nombre_asesor(int i)
        {
            List<string> lista_nombres = new List<string>();

            using (CRMEntities conexion = new CRMEntities())
            {
                try
                {
                    var lista = (from p in conexion.PERSONA
                                 join cat in conexion.INTERLOCUTOR_COMERCIAL on p.ID_PERSONA equals cat.ID_PERSONA
                                 where cat.IDP_AREA_EMPLEADO == i && cat.ES_EMPLEADO == 1
                                 select new
                                 {
                                     p.NOMBRES,
                                     p.AP_PATERNO,
                                     p.AP_MATERNO
                                 }).ToList();

                    foreach (var item in lista)
                    {
                        string nombre = "";
                        nombre = item.NOMBRES + " " + item.AP_PATERNO + " " + item.AP_MATERNO;
                        lista_nombres.Add(nombre);
                    }

                }
                catch (Exception e)
                {

                }
            }
            return lista_nombres;
        }

    }
}