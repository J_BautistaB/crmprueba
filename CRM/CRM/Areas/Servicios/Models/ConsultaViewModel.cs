﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CRM.Entity;

namespace CRM.Areas.Servicios.Models
{
    public class ConsultaViewModel
    {
        public int ID_OPERACION { get; set; }
        public string NOMBRE { get; set; }
        public int DNI_RUC { get; set; }
        public string CATEGORIA { get; set; }
        public System.DateTime FECHA_REGISTRO { get; set; }
        public string ESTADO_OPERACION { get; set; }
        public string SOLUCION { get; set; }
        public System.DateTime FECHA_RESPUESTA { get; set; }
        public string AREA { get; set; }
        public string SEDE { get; set; }
        public string DESCRIPCION { get; set; }
        public int ID_CATEGORIA { get; set; }
        public System.DateTime FECHA_REMISION { get; set; }
        public string ASESOR { get; set; }
        public string ESTADO_RESPUESTA { get; set; }

        public List<CATEGORÍA_OPERACIÓN> lista_categoria { get; set; }

        public List<string> lista_areas { get; set; }

        public List<string> lista_sedes { get; set; }

    }
}