﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Areas.Servicios.Models
{
    public class ListaReclamoViewModel
    {
        public List<ReclamoViewModel> lista { get; set; }

        public string textobuscar { get; set; }

        public string filtro_venta { get; set; }

        public int filtro_cod_reclamo { get; set; }

        public string filtro_nom_cat { get; set; }
    }
}