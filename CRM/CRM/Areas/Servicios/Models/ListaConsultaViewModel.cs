﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Areas.Servicios.Models
{
    public class ListaConsultaViewModel
    {
        public List<ConsultaViewModel> lista { get; set; }

        public string textobuscar { get; set; }

        public string filtro_nombre { get; set; }

        public string filtro_cod_consulta { get; set; }

        public string filtro_nom_cat { get; set; }
    }
}