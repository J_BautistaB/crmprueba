﻿using CRM.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Areas.Servicios.Models
{
    public class ReclamoViewModel
    {
        public int ID_OPERACIÓN { get; set; }
        public Nullable<int> ID_PEDIDO { get; set; }
        public Nullable<int> ID_INTERLOCUTOR { get; set; }
        public Nullable<int> ID_POST_VENTA { get; set; }
        public Nullable<int> DNI_RUC { get; set; }
        public string DESCRIPCIÓN { get; set; }
        public System.DateTime FECHA_REGISTRO { get; set; }
        public string ESTADO_OPERACIÓN { get; set; }
        public string SOLUCIÓN { get; set; }
        public string NOM_CLIENTE { get; set; }
        public Nullable<decimal> TELÉFONO { get; set; }
        public string ASESOR_RESPONSABLE { get; set; }
        public string DIRECCIÓN { get; set; }
        public Nullable<System.DateTime> FECHA_REMISIÓN { get; set; }
        public Nullable<System.DateTime> FECHA_PLAZO { get; set; }
        public Nullable<System.DateTime> FECHA_RESPUESTA { get; set; }
        public string ESTADO_RESPUESTA { get; set; }
        public Nullable<int> R_MONTO_RECLAMADO { get; set; }
        public string R_CÓDIGO_RECLAMADO { get; set; }
        public Nullable<int> ID_PRODUCTO { get; set; }
        public int S_PRECIO_SOLICITUD { get; set; }
        public string S_GARANTÍA { get; set; }
        public string S_TIPO_INMUEBLE { get; set; }
        public string AREA_INVOLUCRADA { get; set; }
        public string SEDE_INVOLUCRADA { get; set; }
        public int ID_TIPO { get; set; }
        public int ID_CATEGORÍA { get; set; }
        public Nullable<int> ID_CONTACTO { get; set; }

        public List<CATEGORÍA_OPERACIÓN> lista_categoria { get; set; }

        public List<string> lista_areas { get; set; }

        public List<string> lista_sedes { get; set; }

        public string nombre_categoria { get; set; }

        public List<string> lista_nombre_asesor { get; set; }

        public string descripcion_nombre_categoria { get; set; }
    }
}