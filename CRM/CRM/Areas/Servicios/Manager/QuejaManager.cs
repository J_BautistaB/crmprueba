﻿using CRM.Areas.Servicios.Models;
using CRM.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Areas.Servicios.Manager
{
    public class QuejaManager
    {
        #region Singleton

        static QuejaManager() { }
        private QuejaManager() { }
        private static QuejaManager _instancia = new QuejaManager();
        public static QuejaManager Instancia { get { return _instancia; } }

        #endregion


        public int RegistrarQueja(OPERACIONES c)
        {
            return AccesoDatos.QuejaAccess.Instancia.RegistrarQueja(c);
        }

        public ReclamoViewModel ObtenerQueja(int idr)
        {
            return AccesoDatos.QuejaAccess.Instancia.ObtenerQueja(idr);

        }

        public int TratarQueja(OPERACIONES op)
        {
            return AccesoDatos.QuejaAccess.Instancia.TratarQueja(op);
        }

        public List<string> lista_nombres_asesores(int i)
        {
            return AccesoDatos.QuejaAccess.Instancia.lista_nombre_asesor(i);
        }

        // Listar categoria segun tipo de operacion
        public List<CATEGORÍA_OPERACIÓN> ListarCategorias(string busqueda)
        {
            return AccesoDatos.ConsultaAccess.Instancia.ListaCategoria(busqueda);
        }

        public List<ReclamoViewModel> ListarQuejas()
        {
            return AccesoDatos.QuejaAccess.Instancia.ListarQuejas();
        }

        public List<ReclamoViewModel> ListarQuejasFiltro(string busqueda)
        {
            return AccesoDatos.QuejaAccess.Instancia.ListarQuejaFiltro(busqueda);
        }

        public List<string> ListaAreas()
        {
            List<string> lista = new List<string>();

            lista.Add("Administrativo");
            lista.Add("CallCenter");
            lista.Add("Clientes");
            lista.Add("Marketing");
            lista.Add("PostVenta");
            lista.Add("Productos");
            lista.Add("Servicios");
            lista.Add("Ventas");

            return lista;

        }

        public List<string> ListaSedes()
        {
            List<string> lista = new List<string>();

            lista.Add("San Isidro");
            lista.Add("Cercado de Lima");
            lista.Add("Ate");
            lista.Add("Lince");
            lista.Add("San Juan");
            lista.Add("Rimac");
            lista.Add("Miraflores");
            lista.Add("Los Olivos");

            return lista;

        }



    }
}