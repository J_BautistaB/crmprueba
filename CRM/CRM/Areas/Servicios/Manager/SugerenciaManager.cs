﻿using CRM.Areas.Servicios.Models;
using CRM.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Areas.Servicios.Manager
{
    public class SugerenciaManager
    {
        #region Singleton

        static SugerenciaManager() { }
        private SugerenciaManager() { }
        private static SugerenciaManager _instancia = new SugerenciaManager();
        public static SugerenciaManager Instancia { get { return _instancia; } }

        #endregion


        public int RegistrarSugerencia(OPERACIONES c)
        {
            return AccesoDatos.SugerenciaAccess.Instancia.RegistrarSugerencia(c);
        }

        public ReclamoViewModel ObtenerSugerencia(int idr)
        {
            return AccesoDatos.SugerenciaAccess.Instancia.ObtenerSugerencia(idr);

        }

        public int TratarSugerencia(OPERACIONES op)
        {
            return AccesoDatos.SugerenciaAccess.Instancia.TratarSugerencia(op);
        }

        // Listar categoria segun tipo de operacion
        public List<CATEGORÍA_OPERACIÓN> ListarCategorias(string busqueda)
        {
            return AccesoDatos.SugerenciaAccess.Instancia.ListaCategoria(busqueda);
        }

        public List<string> lista_nombres_asesores(int i)
        {
            return AccesoDatos.SugerenciaAccess.Instancia.lista_nombre_asesor(i);
        }

        public List<ReclamoViewModel> ListarSugerencias()
        {
            return AccesoDatos.SugerenciaAccess.Instancia.ListarSugerencias();
        }

        public List<ReclamoViewModel> ListarSugerenciasFiltro(string busqueda)
        {
            return AccesoDatos.SugerenciaAccess.Instancia.ListarSugerenciaFiltro(busqueda);
        }

        public List<string> ListaAreas()
        {
            List<string> lista = new List<string>();

            lista.Add("Administrativo");
            lista.Add("CallCenter");
            lista.Add("Clientes");
            lista.Add("Marketing");
            lista.Add("PostVenta");
            lista.Add("Productos");
            lista.Add("Servicios");
            lista.Add("Ventas");

            return lista;

        }

        public List<string> ListaSedes()
        {
            List<string> lista = new List<string>();

            lista.Add("San Isidro");
            lista.Add("Cercado de Lima");
            lista.Add("Ate");
            lista.Add("Lince");
            lista.Add("San Juan");
            lista.Add("Rimac");
            lista.Add("Miraflores");
            lista.Add("Los Olivos");

            return lista;

        }



    }
}