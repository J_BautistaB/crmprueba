﻿using CRM.Areas.Servicios.Models;
using CRM.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Areas.Servicios.Manager
{
    public class ReclamoManager
    {
        #region Singleton

        static ReclamoManager() { }
        private ReclamoManager() { }
        private static ReclamoManager _instancia = new ReclamoManager();
        public static ReclamoManager Instancia { get { return _instancia; } }

        #endregion


        public int RegistrarReclamo(OPERACIONES c)
        {
            return AccesoDatos.ReclamoAccess.Instancia.RegistrarReclamo(c);
        }

        public ReclamoViewModel ObtenerReclamo(int idr) 
        {
            return AccesoDatos.ReclamoAccess.Instancia.ObtenerReclamo(idr);
        
        }

        public ReclamoViewModel ObtenerOperacion(int id, int idt)
        {
            return AccesoDatos.ReclamoAccess.Instancia.ObtenerOperacion(id, idt);
        }

        public int TratarRemitido(OPERACIONES op)
        {
            return AccesoDatos.ReclamoAccess.Instancia.TratarRemitido(op);
        }

        public int TratarReclamo(OPERACIONES op)
        {
            return AccesoDatos.ReclamoAccess.Instancia.TratarReclamo(op);
        }

        public List<string> lista_nombres_asesores(int i)
        {
            return AccesoDatos.ReclamoAccess.Instancia.lista_nombre_asesor(i);
        }

        // Listar categoria segun tipo de operacion
        public List<CATEGORÍA_OPERACIÓN> ListarCategorias(string busqueda)
        {
            return AccesoDatos.ReclamoAccess.Instancia.ListaCategoria(busqueda);
        }

        public List<ReclamoViewModel> ListarReclamos()
        {
            return AccesoDatos.ReclamoAccess.Instancia.ListarReclamos();
        }

        public List<ReclamoViewModel> ListarReclamosFiltro(string busqueda)
        {
            return AccesoDatos.ReclamoAccess.Instancia.ListarReclamoFiltro(busqueda);
        }

        public List<ReclamoViewModel> ListaOperacionesRemitidas(string id)
        {
            return AccesoDatos.ReclamoAccess.Instancia.ListarOperacionesRemitidas(id);
        }

        public List<ReclamoViewModel> ListarRemitidosFiltro(string busqueda)
        {
            return AccesoDatos.ReclamoAccess.Instancia.ListarRemitidasFiltro(busqueda);
        }

        public List<string> ListaAreas() {
            List<string> lista = new List<string>();

            lista.Add("Administrativo");
            lista.Add("CallCenter");
            lista.Add("Clientes");
            lista.Add("Marketing");
            lista.Add("PostVenta");
            lista.Add("Productos");
            lista.Add("Servicios");
            lista.Add("Ventas");

            return lista;
            
        }

        public List<string> ListaSedes()
        {
            List<string> lista = new List<string>();

            lista.Add("San Isidro");
            lista.Add("Cercado de Lima");
            lista.Add("Ate");
            lista.Add("Lince");
            lista.Add("San Juan");
            lista.Add("Rimac");
            lista.Add("Miraflores");
            lista.Add("Los Olivos");

            return lista;

        }



    }
}