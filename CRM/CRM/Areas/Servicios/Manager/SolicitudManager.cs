﻿using CRM.Areas.Servicios.Models;
using CRM.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Areas.Servicios.Manager
{
    public class SolicitudManager
    {
        #region Singleton

        static SolicitudManager() { }
        private SolicitudManager() { }
        private static SolicitudManager _instancia = new SolicitudManager();
        public static SolicitudManager Instancia { get { return _instancia; } }

        #endregion


        public int RegistrarSolicitud(OPERACIONES c)
        {
            return AccesoDatos.SolicitudAccess.Instancia.RegistrarSolicitud(c);
        }

        public ReclamoViewModel ObtenerSolicitud(int idr)
        {
            return AccesoDatos.SolicitudAccess.Instancia.ObtenerSolicitud(idr);

        }

        public int TratarSolicitud(OPERACIONES op)
        {
            return AccesoDatos.SolicitudAccess.Instancia.TratarSolicitud(op);
        }

        public List<string> lista_nombres_asesores(int i)
        {
            return AccesoDatos.SolicitudAccess.Instancia.lista_nombre_asesor(i);
        }

        // Listar categoria segun tipo de operacion
        public List<CATEGORÍA_OPERACIÓN> ListarCategorias(string busqueda)
        {
            return AccesoDatos.SolicitudAccess.Instancia.ListaCategoria(busqueda);
        }

        public List<ReclamoViewModel> ListarSolicitudes()
        {
            return AccesoDatos.SolicitudAccess.Instancia.ListarSolicitudes();
        }

        public List<ReclamoViewModel> ListarSolicitudesFiltro(string busqueda)
        {
            return AccesoDatos.SolicitudAccess.Instancia.ListarSolicitudFiltro(busqueda);
        }

        public List<string> ListaAreas()
        {
            List<string> lista = new List<string>();

            lista.Add("Administrativo");
            lista.Add("CallCenter");
            lista.Add("Clientes");
            lista.Add("Marketing");
            lista.Add("PostVenta");
            lista.Add("Productos");
            lista.Add("Servicios");
            lista.Add("Ventas");

            return lista;

        }

        public List<string> ListaSedes()
        {
            List<string> lista = new List<string>();

            lista.Add("San Isidro");
            lista.Add("Cercado de Lima");
            lista.Add("Ate");
            lista.Add("Lince");
            lista.Add("San Juan");
            lista.Add("Rimac");
            lista.Add("Miraflores");
            lista.Add("Los Olivos");

            return lista;

        }



    }
}