﻿using CRM.Areas.Servicios.Models;
using CRM.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM.Areas.Servicios.Controllers
{
    public class SolicitudController : Controller
    {
        private CRMEntities db = new CRMEntities();

        //
        // GET: /Solicitud/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult ListarSolicitud(int identMensaje = 0, string mensaje = "")
        {
            if (identMensaje == 0)
            {
            }
            else
            {
                ViewData["Mensaje"] = ObtenerMensaje(identMensaje) + mensaje;
            }
            ListaReclamoViewModel crv = new ListaReclamoViewModel();
            crv.lista = Manager.SolicitudManager.Instancia.ListarSolicitudes();

            return View(crv);

        }

        //
        // POST: /Cuota/Create

        [HttpPost]
        public ActionResult ListarSolicitud(ListaReclamoViewModel cvm)
        {
            cvm.lista = Manager.ReclamoManager.Instancia.ListarReclamosFiltro(cvm.textobuscar);
            ListarSolicitud(0, "");
            return View(cvm);
        }

        //Registrar Reclamo
        public ActionResult RegistrarSolicitud()
        {

            ReclamoViewModel rcv = new ReclamoViewModel();
            // Inicializamos combos box de registro
            rcv.lista_categoria = Manager.SolicitudManager.Instancia.ListarCategorias("Solicitud");
            rcv.lista_areas = Manager.SolicitudManager.Instancia.ListaAreas();
            rcv.lista_sedes = Manager.SolicitudManager.Instancia.ListaSedes();

            return View(rcv);
        }


        //
        // POST: /Solicitud/Create

        [HttpPost]
        public ActionResult RegistrarSolicitud(ReclamoViewModel solicitud)
        {

            DateTime thisDay = DateTime.Today;

            OPERACIONES ope = new OPERACIONES();

            if (ModelState.IsValid)
            {
                ope.DNI_RUC = solicitud.DNI_RUC;
                ope.DESCRIPCIÓN = solicitud.DESCRIPCIÓN;
                ope.FECHA_REGISTRO = solicitud.FECHA_REGISTRO;
                ope.ESTADO_OPERACIÓN = solicitud.ESTADO_OPERACIÓN;
                ope.NOM_CLIENTE = solicitud.NOM_CLIENTE;
                ope.TELÉFONO = solicitud.TELÉFONO;
                ope.DIRECCIÓN = solicitud.DIRECCIÓN;
                ope.FECHA_REMISIÓN = solicitud.FECHA_REMISIÓN;
                ope.FECHA_PLAZO = solicitud.FECHA_PLAZO;
                ope.FECHA_RESPUESTA = solicitud.FECHA_RESPUESTA;
                ope.ESTADO_RESPUESTA = solicitud.ESTADO_RESPUESTA;
                ope.R_MONTO_RECLAMADO = solicitud.R_MONTO_RECLAMADO;
                ope.R_CÓDIGO_RECLAMADO = solicitud.R_CÓDIGO_RECLAMADO;
                ope.AREA_INVOLUCRADA = solicitud.AREA_INVOLUCRADA;
                ope.SEDE_INVOLUCRADA = solicitud.SEDE_INVOLUCRADA;
                ope.ID_CATEGORÍA = solicitud.ID_CATEGORÍA;



                ope.ID_PEDIDO = null;
                ope.ID_INTERLOCUTOR = solicitud.ID_INTERLOCUTOR;
                ope.ID_POST_VENTA = solicitud.ID_POST_VENTA;

                // Aun sin responder
                ope.FECHA_REMISIÓN = null;
                ope.FECHA_RESPUESTA = null;

                ope.SOLUCIÓN = "NO SE REGISTRA SOLUCIÓN";
                ope.ASESOR_RESPONSABLE = "NO SE REGISTRA ASESOR";

                ope.NOM_INTERESADO = null;

                ope.ID_PRODUCTO = null;
                ope.S_PRECIO_SOLICITUD=solicitud.S_PRECIO_SOLICITUD;
                ope.S_GARANTÍA = solicitud.S_GARANTÍA;
                ope.S_TIPO_INMUEBLE = solicitud.S_TIPO_INMUEBLE;
                ope.ID_TIPO = 3;



                ope.FECHA_REGISTRO = thisDay;
                // Fecha de Plazo
                ope.FECHA_PLAZO = thisDay.AddDays(7);

                ope.ESTADO_OPERACIÓN = "REGISTRADO";
                ope.ESTADO_RESPUESTA = "NO ENVIADO";

                //reclamo.ID_CATEGORÍA = 1;
                ope.ID_CONTACTO = null;

                int exito = Manager.SolicitudManager.Instancia.RegistrarSolicitud(ope);

                ListarSolicitud(1, " - OK");
            }
            return View("ListarSolicitud");
        }

        public ActionResult TratarSolicitud(int idr)
        {
            ReclamoViewModel SOLICITUD = new ReclamoViewModel();

            SOLICITUD = Manager.SolicitudManager.Instancia.ObtenerSolicitud(idr);
            SOLICITUD.lista_nombre_asesor = Manager.SolicitudManager.Instancia.lista_nombres_asesores(7);

            return View(SOLICITUD);
        }

        [HttpPost]
        public ActionResult TratarSolicitud(ReclamoViewModel solicitud)
        {
            DateTime thisDay = DateTime.Today;

            OPERACIONES ope = new OPERACIONES();

            if (Request["btn"].Equals("Responder"))
            {
                if (ModelState.IsValid)
                {
                    ope.ID_OPERACIÓN = solicitud.ID_OPERACIÓN;
                    ope.DNI_RUC = solicitud.DNI_RUC;
                    ope.DESCRIPCIÓN = solicitud.DESCRIPCIÓN;
                    ope.FECHA_REGISTRO = solicitud.FECHA_REGISTRO;
                    ope.ESTADO_OPERACIÓN = solicitud.ESTADO_OPERACIÓN;
                    ope.NOM_CLIENTE = solicitud.NOM_CLIENTE;
                    ope.TELÉFONO = solicitud.TELÉFONO;
                    ope.DIRECCIÓN = solicitud.DIRECCIÓN;
                    ope.FECHA_REMISIÓN = solicitud.FECHA_REMISIÓN;
                    ope.FECHA_PLAZO = solicitud.FECHA_PLAZO;
                    ope.FECHA_RESPUESTA = solicitud.FECHA_RESPUESTA;
                    ope.ESTADO_RESPUESTA = solicitud.ESTADO_RESPUESTA;
                    ope.R_MONTO_RECLAMADO = solicitud.R_MONTO_RECLAMADO;
                    ope.R_CÓDIGO_RECLAMADO = solicitud.R_CÓDIGO_RECLAMADO;
                    ope.AREA_INVOLUCRADA = solicitud.AREA_INVOLUCRADA;
                    ope.SEDE_INVOLUCRADA = solicitud.SEDE_INVOLUCRADA;
                    ope.ID_CATEGORÍA = solicitud.ID_CATEGORÍA;



                    ope.ID_PEDIDO = null;
                    ope.ID_INTERLOCUTOR = solicitud.ID_INTERLOCUTOR;
                    ope.ID_POST_VENTA = solicitud.ID_POST_VENTA;

                    // Aun sin responder
                    ope.FECHA_REMISIÓN = null;
                    ope.FECHA_RESPUESTA = null;

                    ope.SOLUCIÓN = solicitud.SOLUCIÓN;
                    ope.ASESOR_RESPONSABLE = solicitud.ASESOR_RESPONSABLE;

                    ope.NOM_INTERESADO = null;

                    ope.ID_PRODUCTO = null;
                    ope.S_PRECIO_SOLICITUD = solicitud.S_PRECIO_SOLICITUD;
                    ope.S_GARANTÍA = solicitud.S_GARANTÍA;
                    ope.S_TIPO_INMUEBLE = solicitud.S_TIPO_INMUEBLE;
                    ope.ID_TIPO = 3;



                    ope.FECHA_REGISTRO = thisDay;
                    // Fecha de Plazo
                    ope.FECHA_PLAZO = thisDay.AddDays(7);

                    ope.ESTADO_OPERACIÓN = "TRATADO";
                    ope.ESTADO_RESPUESTA = solicitud.ESTADO_RESPUESTA;

                    //reclamo.ID_CATEGORÍA = 1;
                    ope.ID_CONTACTO = null;

                    int exito = Manager.SolicitudManager.Instancia.TratarSolicitud(ope);

                    ListarSolicitud(1, " - OK");
                }

                return RedirectToAction("ListarSolicitud");
            }

            if (Request["btn"].Equals("Remitir"))
            {
                if (ModelState.IsValid)
                {
                    ope.ID_OPERACIÓN = solicitud.ID_OPERACIÓN;
                    ope.DNI_RUC = solicitud.DNI_RUC;
                    ope.DESCRIPCIÓN = solicitud.DESCRIPCIÓN;
                    ope.FECHA_REGISTRO = solicitud.FECHA_REGISTRO;
                    ope.ESTADO_OPERACIÓN = solicitud.ESTADO_OPERACIÓN;
                    ope.NOM_CLIENTE = solicitud.NOM_CLIENTE;
                    ope.TELÉFONO = solicitud.TELÉFONO;
                    ope.DIRECCIÓN = solicitud.DIRECCIÓN;
                    ope.FECHA_REMISIÓN = solicitud.FECHA_REMISIÓN;
                    ope.FECHA_PLAZO = solicitud.FECHA_PLAZO;
                    ope.FECHA_RESPUESTA = solicitud.FECHA_RESPUESTA;
                    ope.ESTADO_RESPUESTA = solicitud.ESTADO_RESPUESTA;
                    ope.R_MONTO_RECLAMADO = solicitud.R_MONTO_RECLAMADO;
                    ope.R_CÓDIGO_RECLAMADO = solicitud.R_CÓDIGO_RECLAMADO;
                    ope.AREA_INVOLUCRADA = solicitud.AREA_INVOLUCRADA;
                    ope.SEDE_INVOLUCRADA = solicitud.SEDE_INVOLUCRADA;
                    ope.ID_CATEGORÍA = solicitud.ID_CATEGORÍA;



                    ope.ID_PEDIDO = null;
                    ope.ID_INTERLOCUTOR = solicitud.ID_INTERLOCUTOR;
                    ope.ID_POST_VENTA = solicitud.ID_POST_VENTA;

                    // Aun sin responder
                    ope.FECHA_REMISIÓN = null;
                    ope.FECHA_RESPUESTA = null;

                    ope.SOLUCIÓN = solicitud.SOLUCIÓN;
                    ope.ASESOR_RESPONSABLE = solicitud.ASESOR_RESPONSABLE;

                    ope.NOM_INTERESADO = null;

                    ope.ID_PRODUCTO = null;
                    ope.S_PRECIO_SOLICITUD = solicitud.S_PRECIO_SOLICITUD;
                    ope.S_GARANTÍA = solicitud.S_GARANTÍA;
                    ope.S_TIPO_INMUEBLE = solicitud.S_TIPO_INMUEBLE;
                    ope.ID_TIPO = 3;



                    ope.FECHA_REGISTRO = thisDay;
                    // Fecha de Plazo
                    ope.FECHA_PLAZO = thisDay.AddDays(7);

                    ope.ESTADO_OPERACIÓN = "REMITIDO";
                    ope.ESTADO_RESPUESTA = solicitud.ESTADO_RESPUESTA;

                    //reclamo.ID_CATEGORÍA = 1;
                    ope.ID_CONTACTO = null;

                    int exito = Manager.SolicitudManager.Instancia.TratarSolicitud(ope);

                    ListarSolicitud(1, " - OK");
                }

                return RedirectToAction("ListarSolicitud");
            }
            return View(solicitud);
        }


        //mensajes generales para todas las vista de clientes
        public string ObtenerMensaje(int identMensaje)
        {
            switch (identMensaje)
            {
                case 0: return "";
                case 1: return "Solicitud registrada con exito";
                case 2: return "No se registro solicitud";
            }
            return "Mensaje no configurado";
        }

        public ActionResult DetalleSolicitud(int id = 0)
        {
            ReclamoViewModel SOLICITUD = new ReclamoViewModel();

            SOLICITUD = Manager.SolicitudManager.Instancia.ObtenerSolicitud(id);

            return View(SOLICITUD);
        }

    }
}

