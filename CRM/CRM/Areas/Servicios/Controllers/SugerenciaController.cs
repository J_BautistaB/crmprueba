﻿using CRM.Areas.Servicios.Models;
using CRM.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM.Areas.Servicios.Controllers
{
    public class SugerenciaController : Controller
    {
        private CRMEntities db = new CRMEntities();

        //
        // GET: /Consulta/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult ListarSugerencia(int identMensaje = 0, string mensaje = "")
        {
            if (identMensaje == 0)
            {
            }
            else
            {
                ViewData["Mensaje"] = ObtenerMensaje(identMensaje) + mensaje;
            }
            ListaReclamoViewModel crv = new ListaReclamoViewModel();
            crv.lista = Manager.SugerenciaManager.Instancia.ListarSugerencias();

            return View(crv);

        }

        //
        // POST: /Cuota/Create

        [HttpPost]
        public ActionResult ListarSugerencia(ListaReclamoViewModel cvm)
        {
            cvm.lista = Manager.SugerenciaManager.Instancia.ListarSugerenciasFiltro(cvm.textobuscar);
            ListarSugerencia(0, "");
            return View(cvm);
        }

        //Registrar Sugerencia
        public ActionResult RegistrarSugerencia()
        {

            ReclamoViewModel rcv = new ReclamoViewModel();
            // Inicializamos combos box de registro
            rcv.lista_categoria = Manager.SugerenciaManager.Instancia.ListarCategorias("Sugerencia");
            rcv.lista_areas = Manager.SugerenciaManager.Instancia.ListaAreas();
            rcv.lista_sedes = Manager.SugerenciaManager.Instancia.ListaSedes();

            return View(rcv);
        }


        //
        // POST: /Consulta/Create

        [HttpPost]
        public ActionResult RegistrarSugerencia(ReclamoViewModel sugerencia)
        {

            DateTime thisDay = DateTime.Today;

            OPERACIONES ope = new OPERACIONES();

            if (ModelState.IsValid)
            {
                ope.DNI_RUC = sugerencia.DNI_RUC;
                ope.DESCRIPCIÓN = sugerencia.DESCRIPCIÓN;
                ope.FECHA_REGISTRO = sugerencia.FECHA_REGISTRO;
                ope.NOM_CLIENTE = sugerencia.NOM_CLIENTE;
                ope.TELÉFONO = sugerencia.TELÉFONO;
                ope.DIRECCIÓN = sugerencia.DIRECCIÓN;
                ope.FECHA_REMISIÓN = null;
                ope.FECHA_PLAZO = null;
                ope.FECHA_RESPUESTA = null;
                ope.R_MONTO_RECLAMADO = null;
                ope.R_CÓDIGO_RECLAMADO = null;
                ope.AREA_INVOLUCRADA = sugerencia.AREA_INVOLUCRADA;
                ope.SEDE_INVOLUCRADA = sugerencia.SEDE_INVOLUCRADA;
                ope.ID_CATEGORÍA = sugerencia.ID_CATEGORÍA;



                ope.ID_PEDIDO = null;
                ope.ID_INTERLOCUTOR = sugerencia.ID_INTERLOCUTOR;
                ope.ID_POST_VENTA = null;

                // Aun sin responder
                ope.FECHA_REMISIÓN = null;
                ope.FECHA_RESPUESTA = null;

                ope.SOLUCIÓN = "NO SE REGISTRA SOLUCIÓN";
                ope.ASESOR_RESPONSABLE = "NO SE REGISTRA ASESOR";

                ope.NOM_INTERESADO = null;

                ope.ID_PRODUCTO = null;
                ope.S_PRECIO_SOLICITUD = 0;
                ope.S_GARANTÍA = null;
                ope.S_TIPO_INMUEBLE = null;
                ope.ID_TIPO = 4;



                ope.FECHA_REGISTRO = thisDay;

                ope.ESTADO_OPERACIÓN = "REGISTRADO";
                ope.ESTADO_RESPUESTA = "NO ENVIADO";

                //reclamo.ID_CATEGORÍA = 1;
                ope.ID_CONTACTO = null;

                int exito = Manager.SugerenciaManager.Instancia.RegistrarSugerencia(ope);

                ListarSugerencia(1, " - OK");
            }
            return View("ListarSugerencia");
        }

        public ActionResult TratarSugerencia(int idr)
        {
            ReclamoViewModel SUGERENCIA = new ReclamoViewModel();
            SUGERENCIA = Manager.SugerenciaManager.Instancia.ObtenerSugerencia(idr);
            SUGERENCIA.lista_nombre_asesor = Manager.SugerenciaManager.Instancia.lista_nombres_asesores(7);
            return View(SUGERENCIA);
        }

        [HttpPost]
        public ActionResult TratarSugerencia(ReclamoViewModel sugerencia)
        {
            DateTime thisDay = DateTime.Today;

            OPERACIONES ope = new OPERACIONES();

            if (Request["btn"].Equals("Responder"))
            {
                if (ModelState.IsValid)
                {
                    ope.ID_OPERACIÓN = sugerencia.ID_OPERACIÓN;
                    ope.DNI_RUC = sugerencia.DNI_RUC;
                    ope.DESCRIPCIÓN = sugerencia.DESCRIPCIÓN;
                    ope.FECHA_REGISTRO = sugerencia.FECHA_REGISTRO;
                    ope.NOM_CLIENTE = sugerencia.NOM_CLIENTE;
                    ope.TELÉFONO = sugerencia.TELÉFONO;
                    ope.DIRECCIÓN = sugerencia.DIRECCIÓN;
                    ope.FECHA_REMISIÓN = null;
                    ope.FECHA_PLAZO = null;
                    ope.FECHA_RESPUESTA = null;
                    ope.R_MONTO_RECLAMADO = null;
                    ope.R_CÓDIGO_RECLAMADO = null;
                    ope.AREA_INVOLUCRADA = sugerencia.AREA_INVOLUCRADA;
                    ope.SEDE_INVOLUCRADA = sugerencia.SEDE_INVOLUCRADA;
                    ope.ID_CATEGORÍA = sugerencia.ID_CATEGORÍA;



                    ope.ID_PEDIDO = null;
                    ope.ID_INTERLOCUTOR = sugerencia.ID_INTERLOCUTOR;
                    ope.ID_POST_VENTA = null;

                    // Aun sin responder
                    ope.FECHA_REMISIÓN = null;
                    ope.FECHA_RESPUESTA = null;

                    ope.SOLUCIÓN = sugerencia.SOLUCIÓN;
                    ope.ASESOR_RESPONSABLE = sugerencia.ASESOR_RESPONSABLE;

                    ope.NOM_INTERESADO = null;

                    ope.ID_PRODUCTO = null;
                    ope.S_PRECIO_SOLICITUD = 0;
                    ope.S_GARANTÍA = null;
                    ope.S_TIPO_INMUEBLE = null;
                    ope.ID_TIPO = 4;



                    ope.FECHA_REGISTRO = thisDay;

                    ope.ESTADO_OPERACIÓN = "TRATADO";
                    ope.ESTADO_RESPUESTA = "NO ENVIADO";

                    //reclamo.ID_CATEGORÍA = 1;
                    ope.ID_CONTACTO = null;

                    int exito = Manager.SugerenciaManager.Instancia.TratarSugerencia(ope);

                    ListarSugerencia(1, " - OK");
                }

                return RedirectToAction("ListarSugerencia");
            }

            if (Request["btn"].Equals("Remitir"))
            {
                if (ModelState.IsValid)
                {
                    ope.ID_OPERACIÓN = sugerencia.ID_OPERACIÓN;
                    ope.DNI_RUC = sugerencia.DNI_RUC;
                    ope.DESCRIPCIÓN = sugerencia.DESCRIPCIÓN;
                    ope.FECHA_REGISTRO = sugerencia.FECHA_REGISTRO;
                    ope.NOM_CLIENTE = sugerencia.NOM_CLIENTE;
                    ope.TELÉFONO = sugerencia.TELÉFONO;
                    ope.DIRECCIÓN = sugerencia.DIRECCIÓN;
                    ope.FECHA_REMISIÓN = null;
                    ope.FECHA_PLAZO = null;
                    ope.FECHA_RESPUESTA = null;
                    ope.R_MONTO_RECLAMADO = null;
                    ope.R_CÓDIGO_RECLAMADO = null;
                    ope.AREA_INVOLUCRADA = sugerencia.AREA_INVOLUCRADA;
                    ope.SEDE_INVOLUCRADA = sugerencia.SEDE_INVOLUCRADA;
                    ope.ID_CATEGORÍA = sugerencia.ID_CATEGORÍA;



                    ope.ID_PEDIDO = null;
                    ope.ID_INTERLOCUTOR = sugerencia.ID_INTERLOCUTOR;
                    ope.ID_POST_VENTA = null;

                    // Aun sin responder
                    ope.FECHA_REMISIÓN = null;
                    ope.FECHA_RESPUESTA = null;

                    ope.SOLUCIÓN = sugerencia.SOLUCIÓN;
                    ope.ASESOR_RESPONSABLE = sugerencia.ASESOR_RESPONSABLE;

                    ope.NOM_INTERESADO = null;

                    ope.ID_PRODUCTO = null;
                    ope.S_PRECIO_SOLICITUD = 0;
                    ope.S_GARANTÍA = null;
                    ope.S_TIPO_INMUEBLE = null;
                    ope.ID_TIPO = 4;



                    ope.FECHA_REGISTRO = thisDay;

                    ope.ESTADO_OPERACIÓN = "REMITIDO";
                    ope.ESTADO_RESPUESTA = "NO ENVIADO";

                    //reclamo.ID_CATEGORÍA = 1;
                    ope.ID_CONTACTO = null;

                    int exito = Manager.SugerenciaManager.Instancia.TratarSugerencia(ope);

                    ListarSugerencia(1, " - OK");
                }

                return RedirectToAction("ListarSugerencia");
            }
            return View(sugerencia);
        }


        //mensajes generales para todas las vista de clientes
        public string ObtenerMensaje(int identMensaje)
        {
            switch (identMensaje)
            {
                case 0: return "";
                case 1: return "Sugerencia registrada con exito";
                case 2: return "No se registro sugerencia";
            }
            return "Mensaje no configurado";
        }

        public ActionResult DetalleSugerencia(int id = 0)
        {
            ReclamoViewModel SUGERENCIA = new ReclamoViewModel();

            SUGERENCIA = Manager.SugerenciaManager.Instancia.ObtenerSugerencia(id);

            return View(SUGERENCIA);
        }

    }
}

