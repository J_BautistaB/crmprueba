﻿using CRM.Areas.Servicios.Models;
using CRM.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM.Areas.Servicios.Controllers
{
    public class ReclamoController : Controller
    {
        private CRMEntities db = new CRMEntities();

        //
        // GET: /Reclamo/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult ListarReclamo(int identMensaje = 0, string mensaje = "")
        {
            if (identMensaje == 0)
            {
            }
            else
            {
                ViewData["Mensaje"] = ObtenerMensaje(identMensaje) + mensaje;
            }
            ListaReclamoViewModel crv = new ListaReclamoViewModel();
            crv.lista = Manager.ReclamoManager.Instancia.ListarReclamos();

            return View(crv);

        }

        //
        // POST: /Cuota/Create

        [HttpPost]
        public ActionResult ListarReclamo(ListaReclamoViewModel cvm)
        {
            cvm.lista = Manager.ReclamoManager.Instancia.ListarReclamosFiltro(cvm.textobuscar);
            ListarReclamo(0, "");
            return View(cvm);
        }

        //Registrar Reclamo
        public ActionResult RegistrarReclamo()
        {

            ReclamoViewModel rcv = new ReclamoViewModel();
            // Inicializamos combos box de registro
            rcv.lista_categoria = Manager.ReclamoManager.Instancia.ListarCategorias("Reclamo");
            rcv.lista_areas = Manager.ReclamoManager.Instancia.ListaAreas();
            rcv.lista_sedes = Manager.ReclamoManager.Instancia.ListaSedes(); 

            return View(rcv);
        }


        //
        // POST: /Reclamo/Create

        [HttpPost]
        public ActionResult RegistrarReclamo(ReclamoViewModel reclamo)
        {

            DateTime thisDay = DateTime.Today;

            OPERACIONES ope = new OPERACIONES();

            if (ModelState.IsValid)
            {
                ope.DNI_RUC = reclamo.DNI_RUC;
                ope.DESCRIPCIÓN = reclamo.DESCRIPCIÓN;
                ope.FECHA_REGISTRO = reclamo.FECHA_REGISTRO;
                ope.ESTADO_OPERACIÓN = reclamo.ESTADO_OPERACIÓN;
                ope.NOM_CLIENTE = reclamo.NOM_CLIENTE;
                ope.TELÉFONO = reclamo.TELÉFONO;
                ope.DIRECCIÓN = reclamo.DIRECCIÓN;
                ope.FECHA_REMISIÓN = reclamo.FECHA_REMISIÓN;
                ope.FECHA_PLAZO = reclamo.FECHA_PLAZO;
                ope.FECHA_RESPUESTA = reclamo.FECHA_RESPUESTA;
                ope.ESTADO_RESPUESTA = reclamo.ESTADO_RESPUESTA;
                ope.R_MONTO_RECLAMADO = reclamo.R_MONTO_RECLAMADO;
                ope.R_CÓDIGO_RECLAMADO = reclamo.R_CÓDIGO_RECLAMADO;
                ope.AREA_INVOLUCRADA = reclamo.AREA_INVOLUCRADA;
                ope.SEDE_INVOLUCRADA = reclamo.SEDE_INVOLUCRADA;
                ope.ID_CATEGORÍA = reclamo.ID_CATEGORÍA;



                ope.ID_PEDIDO = null;
                ope.ID_INTERLOCUTOR = null;
                ope.ID_POST_VENTA = null;

                // Aun sin responder
                ope.FECHA_REMISIÓN = null;
                ope.FECHA_RESPUESTA = null;

                ope.SOLUCIÓN = "NO SE REGISTRA SOLUCIÓN";
                ope.ASESOR_RESPONSABLE = "NO SE REGISTRA ASESOR";

                ope.NOM_INTERESADO = null;

                ope.ID_PRODUCTO = null;
                ope.S_PRECIO_SOLICITUD = 0;
                ope.S_GARANTÍA = null;
                ope.S_TIPO_INMUEBLE = null;
                ope.ID_TIPO = 1;

               

                ope.FECHA_REGISTRO = thisDay;
                // Fecha de Plazo
                ope.FECHA_PLAZO = thisDay.AddDays(7);

                ope.ESTADO_OPERACIÓN = "REGISTRADO";
                ope.ESTADO_RESPUESTA = "NO ENVIADO";

                //reclamo.ID_CATEGORÍA = 1;
                ope.ID_CONTACTO = null;

                int exito = Manager.ReclamoManager.Instancia.RegistrarReclamo(ope);

                ListarReclamo(1, " - OK");
            }
            return View("ListarReclamo");
        }



        //mensajes generales para todas las vista de clientes
        public string ObtenerMensaje(int identMensaje)
        {
            switch (identMensaje)
            {
                case 0: return "";
                case 1: return "Reclamo registrado con exito";
                case 2: return "No se registro reclamo";
            }
            return "Mensaje no configurado";
        }




        public ActionResult TratarReclamo(int idr = 0) 
        {
            ReclamoViewModel RECLAMO = new ReclamoViewModel();

            RECLAMO = Manager.ReclamoManager.Instancia.ObtenerReclamo(idr);
            RECLAMO.lista_nombre_asesor = Manager.ReclamoManager.Instancia.lista_nombres_asesores(7);

            return View(RECLAMO);
        }

        [HttpPost]
        public ActionResult TratarReclamo(OPERACIONES reclamo)
        {
            DateTime thisDay = DateTime.Today;

            OPERACIONES ope = new OPERACIONES();

            if (Request["btn"].Equals("Responder"))
            {
                if (ModelState.IsValid)
                {
                    ope.ID_OPERACIÓN = reclamo.ID_OPERACIÓN;
                    ope.DNI_RUC = reclamo.DNI_RUC;
                    ope.DESCRIPCIÓN = reclamo.DESCRIPCIÓN;
                    ope.FECHA_REGISTRO = reclamo.FECHA_REGISTRO;
                    ope.ESTADO_OPERACIÓN = reclamo.ESTADO_OPERACIÓN;
                    ope.NOM_CLIENTE = reclamo.NOM_CLIENTE;
                    ope.TELÉFONO = reclamo.TELÉFONO;
                    ope.DIRECCIÓN = reclamo.DIRECCIÓN;
                    ope.FECHA_REMISIÓN = reclamo.FECHA_REMISIÓN;
                    ope.FECHA_PLAZO = reclamo.FECHA_PLAZO;
                    ope.FECHA_RESPUESTA = reclamo.FECHA_RESPUESTA;
                    ope.ESTADO_RESPUESTA = reclamo.ESTADO_RESPUESTA;
                    ope.R_MONTO_RECLAMADO = reclamo.R_MONTO_RECLAMADO;
                    ope.R_CÓDIGO_RECLAMADO = reclamo.R_CÓDIGO_RECLAMADO;
                    ope.AREA_INVOLUCRADA = reclamo.AREA_INVOLUCRADA;
                    ope.SEDE_INVOLUCRADA = reclamo.SEDE_INVOLUCRADA;
                    ope.ID_CATEGORÍA = reclamo.ID_CATEGORÍA;



                    ope.ID_PEDIDO = null;
                    ope.ID_INTERLOCUTOR = reclamo.ID_INTERLOCUTOR;
                    ope.ID_POST_VENTA = null;

                    // Aun sin responder
                    ope.FECHA_REMISIÓN = null;
                    ope.FECHA_RESPUESTA = null;

                    ope.SOLUCIÓN = reclamo.SOLUCIÓN;
                    ope.ASESOR_RESPONSABLE = reclamo.ASESOR_RESPONSABLE;

                    ope.NOM_INTERESADO = null;

                    ope.ID_PRODUCTO = null;
                    ope.S_PRECIO_SOLICITUD = 0;
                    ope.S_GARANTÍA = null;
                    ope.S_TIPO_INMUEBLE = null;
                    ope.ID_TIPO = 1;



                    ope.FECHA_REGISTRO = thisDay;
                    // Fecha de Plazo
                    ope.FECHA_PLAZO = thisDay.AddDays(7);

                    ope.ESTADO_OPERACIÓN = "TRATADO";
                    ope.ESTADO_RESPUESTA = "NO ENVIADO";

                    //reclamo.ID_CATEGORÍA = 1;
                    ope.ID_CONTACTO = null;

                    int exito = Manager.ReclamoManager.Instancia.TratarReclamo(ope);

                    ListarReclamo(1, " - OK");
                }
                return RedirectToAction("ListarReclamo");
            }

            if (Request["btn"].Equals("Remitir"))
            {
                if (ModelState.IsValid)
                {
                    ope.ID_OPERACIÓN = reclamo.ID_OPERACIÓN;
                    ope.DNI_RUC = reclamo.DNI_RUC;
                    ope.DESCRIPCIÓN = reclamo.DESCRIPCIÓN;
                    ope.FECHA_REGISTRO = reclamo.FECHA_REGISTRO;
                    ope.ESTADO_OPERACIÓN = reclamo.ESTADO_OPERACIÓN;
                    ope.NOM_CLIENTE = reclamo.NOM_CLIENTE;
                    ope.TELÉFONO = reclamo.TELÉFONO;
                    ope.DIRECCIÓN = reclamo.DIRECCIÓN;
                    ope.FECHA_REMISIÓN = reclamo.FECHA_REMISIÓN;
                    ope.FECHA_PLAZO = reclamo.FECHA_PLAZO;
                    ope.FECHA_RESPUESTA = reclamo.FECHA_RESPUESTA;
                    ope.ESTADO_RESPUESTA = reclamo.ESTADO_RESPUESTA;
                    ope.R_MONTO_RECLAMADO = reclamo.R_MONTO_RECLAMADO;
                    ope.R_CÓDIGO_RECLAMADO = reclamo.R_CÓDIGO_RECLAMADO;
                    ope.AREA_INVOLUCRADA = reclamo.AREA_INVOLUCRADA;
                    ope.SEDE_INVOLUCRADA = reclamo.SEDE_INVOLUCRADA;
                    ope.ID_CATEGORÍA = reclamo.ID_CATEGORÍA;



                    ope.ID_PEDIDO = null;
                    ope.ID_INTERLOCUTOR = reclamo.ID_INTERLOCUTOR;
                    ope.ID_POST_VENTA = null;

                    // Aun sin responder
                    ope.FECHA_REMISIÓN = null;
                    ope.FECHA_RESPUESTA = null;

                    ope.SOLUCIÓN = reclamo.SOLUCIÓN;
                    ope.ASESOR_RESPONSABLE = reclamo.ASESOR_RESPONSABLE;

                    ope.NOM_INTERESADO = null;

                    ope.ID_PRODUCTO = null;
                    ope.S_PRECIO_SOLICITUD = 0;
                    ope.S_GARANTÍA = null;
                    ope.S_TIPO_INMUEBLE = null;
                    ope.ID_TIPO = 1;



                    ope.FECHA_REGISTRO = thisDay;
                    // Fecha de Plazo
                    ope.FECHA_PLAZO = thisDay.AddDays(7);

                    ope.ESTADO_OPERACIÓN = "REMITIDO";
                    ope.ESTADO_RESPUESTA = "NO ENVIADO";

                    //reclamo.ID_CATEGORÍA = 1;
                    ope.ID_CONTACTO = null;

                    int exito = Manager.ReclamoManager.Instancia.TratarReclamo(ope);

                    ListarReclamo(1, " - OK");
                }
                return RedirectToAction("ListarReclamo");
            }
            return View(reclamo);
            
        }

        public ActionResult ListarRemitidos(string id)
        {
            ListaReclamoViewModel remitidos = new ListaReclamoViewModel();

            if (id == "Marketing")
            {
                ViewData["AreaDestino"] = "~/Areas/Marketing/Views/MasterMarketing.cshtml";
            }
            else if (id == "Administrativo")
            {
                ViewData["AreaDestino"] = "~/Areas/Administrativo/Views/MasterAdministrativo.cshtml"; 
            }
            else if (id == "CallCenter")
            {
                ViewData["AreaDestino"] = "~/Areas/CallCenter/Views/MasterCallCenter.cshtml"; 
            }
            else if (id == "Clientes")
            {
                ViewData["AreaDestino"] = "~/Areas/Clientes/Views/MasterClientes.cshtml";
            }
            else if (id == "PostVenta")
            {
                ViewData["AreaDestino"] = "~/Areas/PostVenta/Views/MasterPostVenta.cshtml";
            }
            else if (id == "Productos")
            {
                ViewData["AreaDestino"] = "~/Areas/Productos/Views/MasterProductos.cshtml"; 
            }
            else if(id=="Ventas")
            {
                ViewData["AreaDestino"] = "~/Areas/Ventas/Views/MasterVentas.cshtml"; 
            }

            remitidos.lista = Manager.ReclamoManager.Instancia.ListaOperacionesRemitidas(id);

            return PartialView(remitidos);
        }

        [HttpPost]
        public ActionResult ListarRemitidosFiltro(ListaReclamoViewModel operacion)
        {
            operacion.lista = Manager.ReclamoManager.Instancia.ListarRemitidosFiltro(operacion.textobuscar);
            ListarReclamo(0, "");
            return PartialView(operacion);
        }

        public ActionResult TratarRemitidos(int id, int idt)
        {
            ReclamoViewModel RECLAMO = new ReclamoViewModel();
            string area = null;

            RECLAMO = Manager.ReclamoManager.Instancia.ObtenerOperacion(id, idt);

            area = RECLAMO.AREA_INVOLUCRADA;

            if (area == "Marketing")
            {
                ViewData["AreaDestino"] = "~/Areas/Marketing/Views/MasterMarketing.cshtml";
            }
            else if (area == "Administrativo")
            {
                ViewData["AreaDestino"] = "~/Areas/Administrativo/Views/MasterAdministrativo.cshtml";
            }
            else if (area == "CallCenter")
            {
                ViewData["AreaDestino"] = "~/Areas/CallCenter/Views/MasterCallCenter.cshtml";
            }
            else if (area == "Clientes")
            {
                ViewData["AreaDestino"] = "~/Areas/Clientes/Views/MasterClientes.cshtml";
            }
            else if (area == "PostVenta")
            {
                ViewData["AreaDestino"] = "~/Areas/PostVenta/Views/MasterPostVenta.cshtml";
            }
            else if (area == "Productos")
            {
                ViewData["AreaDestino"] = "~/Areas/Productos/Views/MasterProductos.cshtml";
            }
            else if (area == "Ventas")
            {
                ViewData["AreaDestino"] = "~/Areas/Ventas/Views/MasterVentas.cshtml";
            }


            return PartialView(RECLAMO);
        }

        [HttpPost]
        public ActionResult TratarRemitidos(ReclamoViewModel operacion)
        {
            DateTime thisDay = DateTime.Today;

            OPERACIONES ope = new OPERACIONES();
                        
                if (ModelState.IsValid)
                {
                    ope.ID_OPERACIÓN = operacion.ID_OPERACIÓN;
                    ope.DNI_RUC = operacion.DNI_RUC;
                    ope.DESCRIPCIÓN = operacion.DESCRIPCIÓN;
                    ope.FECHA_REGISTRO = operacion.FECHA_REGISTRO;
                    ope.ESTADO_OPERACIÓN = operacion.ESTADO_OPERACIÓN;
                    ope.NOM_CLIENTE = operacion.NOM_CLIENTE;
                    ope.TELÉFONO = operacion.TELÉFONO;
                    ope.DIRECCIÓN = operacion.DIRECCIÓN;
                    ope.FECHA_REMISIÓN = operacion.FECHA_REMISIÓN;
                    ope.FECHA_PLAZO = operacion.FECHA_PLAZO;
                    ope.FECHA_RESPUESTA = operacion.FECHA_RESPUESTA;
                    ope.ESTADO_RESPUESTA = operacion.ESTADO_RESPUESTA;
                    ope.R_MONTO_RECLAMADO = operacion.R_MONTO_RECLAMADO;
                    ope.R_CÓDIGO_RECLAMADO = operacion.R_CÓDIGO_RECLAMADO;
                    ope.AREA_INVOLUCRADA = operacion.AREA_INVOLUCRADA;
                    ope.SEDE_INVOLUCRADA = operacion.SEDE_INVOLUCRADA;
                    ope.ID_CATEGORÍA = operacion.ID_CATEGORÍA;



                    ope.ID_PEDIDO = null;
                    ope.ID_INTERLOCUTOR = null;
                    ope.ID_POST_VENTA = null;

                    // Aun sin responder
                    ope.FECHA_REMISIÓN = null;
                    ope.FECHA_RESPUESTA = null;

                    ope.SOLUCIÓN = operacion.SOLUCIÓN;
                    ope.ASESOR_RESPONSABLE = operacion.ASESOR_RESPONSABLE;

                    ope.NOM_INTERESADO = null;

                    ope.ID_PRODUCTO = null;
                    ope.S_PRECIO_SOLICITUD = 0;
                    ope.S_GARANTÍA = null;
                    ope.S_TIPO_INMUEBLE = null;
                    ope.ID_TIPO = 1;



                    ope.FECHA_REGISTRO = thisDay;
                    // Fecha de Plazo
                    ope.FECHA_PLAZO = thisDay.AddDays(7);

                    ope.ESTADO_OPERACIÓN = "DEVUELTO";
                    ope.ESTADO_RESPUESTA = "NO ENVIADO";

                    //reclamo.ID_CATEGORÍA = 1;
                    ope.ID_CONTACTO = null;

                    int exito = Manager.ReclamoManager.Instancia.TratarRemitido(ope);
                }
                return RedirectToAction("ListarRemitidos");
            }

        public ActionResult DetalleReclamo(int id = 0)
        {
            ReclamoViewModel RECLAMO = new ReclamoViewModel();

            RECLAMO = Manager.ReclamoManager.Instancia.ObtenerReclamo(id);

            return View(RECLAMO);
        }

        public ActionResult DetalleRemitido(int id = 0, int idt=0)
        {
            ReclamoViewModel RECLAMO = new ReclamoViewModel();

            RECLAMO = Manager.ReclamoManager.Instancia.ObtenerOperacion(id,idt);

            string area = RECLAMO.AREA_INVOLUCRADA;

            if (area == "Marketing")
            {
                ViewData["AreaDestino"] = "~/Areas/Marketing/Views/MasterMarketing.cshtml";
            }
            else if (area == "Administrativo")
            {
                ViewData["AreaDestino"] = "~/Areas/Administrativo/Views/MasterAdministrativo.cshtml";
            }
            else if (area == "CallCenter")
            {
                ViewData["AreaDestino"] = "~/Areas/CallCenter/Views/MasterCallCenter.cshtml";
            }
            else if (area == "Clientes")
            {
                ViewData["AreaDestino"] = "~/Areas/Clientes/Views/MasterClientes.cshtml";
            }
            else if (area == "PostVenta")
            {
                ViewData["AreaDestino"] = "~/Areas/PostVenta/Views/MasterPostVenta.cshtml";
            }
            else if (area == "Productos")
            {
                ViewData["AreaDestino"] = "~/Areas/Productos/Views/MasterProductos.cshtml";
            }
            else if (area == "Ventas")
            {
                ViewData["AreaDestino"] = "~/Areas/Ventas/Views/MasterVentas.cshtml";
            }

            return View(RECLAMO);
        }
            

        }
        
    }

