﻿using CRM.Areas.Servicios.Models;
using CRM.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM.Areas.Servicios.Controllers
{
    public class QuejaController : Controller
    {
        private CRMEntities db = new CRMEntities();

        //
        // GET: /Queja/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult ListarQueja(int identMensaje = 0, string mensaje = "")
        {
            if (identMensaje == 0)
            {
            }
            else
            {
                ViewData["Mensaje"] = ObtenerMensaje(identMensaje) + mensaje;
            }
            ListaReclamoViewModel crv = new ListaReclamoViewModel();
            crv.lista = Manager.QuejaManager.Instancia.ListarQuejas();

            return View(crv);

        }

        //
        // POST: /Cuota/Create

        [HttpPost]
        public ActionResult ListarQueja(ListaReclamoViewModel cvm)
        {
            cvm.lista = Manager.QuejaManager.Instancia.ListarQuejasFiltro(cvm.textobuscar);
            ListarQueja(0, "");
            return View(cvm);
        }

        //Registrar Queja
        public ActionResult RegistrarQueja()
        {

            ReclamoViewModel rcv = new ReclamoViewModel();
            // Inicializamos combos box de registro
            rcv.lista_categoria = Manager.QuejaManager.Instancia.ListarCategorias("Queja");
            rcv.lista_areas = Manager.QuejaManager.Instancia.ListaAreas();
            rcv.lista_sedes = Manager.QuejaManager.Instancia.ListaSedes();

            return View(rcv);
        }


        //
        // POST: /Consulta/Create

        [HttpPost]
        public ActionResult RegistrarQueja(ReclamoViewModel queja)
        {

            DateTime thisDay = DateTime.Today;

            OPERACIONES ope = new OPERACIONES();

            if (ModelState.IsValid)
            {
                ope.DNI_RUC = queja.DNI_RUC;
                ope.DESCRIPCIÓN = queja.DESCRIPCIÓN;
                ope.FECHA_REGISTRO = queja.FECHA_REGISTRO;
                ope.NOM_CLIENTE = queja.NOM_CLIENTE;
                ope.TELÉFONO = queja.TELÉFONO;
                ope.DIRECCIÓN = queja.DIRECCIÓN;
                ope.FECHA_REMISIÓN = null;
                ope.FECHA_PLAZO = null;
                ope.FECHA_RESPUESTA = null;
                ope.R_MONTO_RECLAMADO = null;
                ope.R_CÓDIGO_RECLAMADO = null;
                ope.AREA_INVOLUCRADA = queja.AREA_INVOLUCRADA;
                ope.SEDE_INVOLUCRADA = queja.SEDE_INVOLUCRADA;
                ope.ID_CATEGORÍA = queja.ID_CATEGORÍA;



                ope.ID_PEDIDO = null;
                ope.ID_INTERLOCUTOR = queja.ID_INTERLOCUTOR;
                ope.ID_POST_VENTA = null;

                // Aun sin responder
                ope.FECHA_REMISIÓN = null;
                ope.FECHA_RESPUESTA = null;

                ope.SOLUCIÓN = "NO SE REGISTRA SOLUCIÓN";
                ope.ASESOR_RESPONSABLE = "NO SE REGISTRA ASESOR";

                ope.NOM_INTERESADO = null;

                ope.ID_PRODUCTO = null;
                ope.S_PRECIO_SOLICITUD = 0;
                ope.S_GARANTÍA = null;
                ope.S_TIPO_INMUEBLE = null;
                ope.ID_TIPO = 2;



                ope.FECHA_REGISTRO = thisDay;

                ope.ESTADO_OPERACIÓN = "REGISTRADO";
                ope.ESTADO_RESPUESTA = "NO ENVIADO";

                //reclamo.ID_CATEGORÍA = 1;
                ope.ID_CONTACTO = null;

                int exito = Manager.QuejaManager.Instancia.RegistrarQueja(ope);

                ListarQueja(1, " - OK");
            }
            return View("ListarQueja");
        }

    

        //mensajes generales para todas las vista de clientes
        public string ObtenerMensaje(int identMensaje)
        {
            switch (identMensaje)
            {
                case 0: return "";
                case 1: return "Queja registrada con exito";
                case 2: return "No se registro queja";
            }
            return "Mensaje no configurado";
        }




        public ActionResult TratarQueja(int idr = 0)
        {
            ReclamoViewModel QUEJA = new ReclamoViewModel();

            QUEJA = Manager.QuejaManager.Instancia.ObtenerQueja(idr);
            QUEJA.lista_nombre_asesor = Manager.QuejaManager.Instancia.lista_nombres_asesores(1);

            return View(QUEJA);
        }

        [HttpPost]
        public ActionResult TratarQueja(ReclamoViewModel queja)
        {
            DateTime thisDay = DateTime.Today;

            OPERACIONES ope = new OPERACIONES();

            if (Request["btn"].Equals("Responder"))
            {
                if (ModelState.IsValid)
                {
                    ope.ID_OPERACIÓN = queja.ID_OPERACIÓN;
                    ope.DNI_RUC = queja.DNI_RUC;
                    ope.DESCRIPCIÓN = queja.DESCRIPCIÓN;
                    ope.FECHA_REGISTRO = queja.FECHA_REGISTRO;
                    ope.NOM_CLIENTE = queja.NOM_CLIENTE;
                    ope.TELÉFONO = queja.TELÉFONO;
                    ope.DIRECCIÓN = queja.DIRECCIÓN;
                    ope.FECHA_REMISIÓN = null;
                    ope.FECHA_PLAZO = null;
                    ope.FECHA_RESPUESTA = null;
                    ope.R_MONTO_RECLAMADO = null;
                    ope.R_CÓDIGO_RECLAMADO = null;
                    ope.AREA_INVOLUCRADA = queja.AREA_INVOLUCRADA;
                    ope.SEDE_INVOLUCRADA = queja.SEDE_INVOLUCRADA;
                    ope.ID_CATEGORÍA = queja.ID_CATEGORÍA;



                    ope.ID_PEDIDO = null;
                    ope.ID_INTERLOCUTOR = queja.ID_INTERLOCUTOR;
                    ope.ID_POST_VENTA = null;

                    // Aun sin responder
                    ope.FECHA_REMISIÓN = null;
                    ope.FECHA_RESPUESTA = null;

                    ope.SOLUCIÓN = queja.SOLUCIÓN;
                    ope.ASESOR_RESPONSABLE = queja.ASESOR_RESPONSABLE;

                    ope.NOM_INTERESADO = null;

                    ope.ID_PRODUCTO = null;
                    ope.S_PRECIO_SOLICITUD = 0;
                    ope.S_GARANTÍA = null;
                    ope.S_TIPO_INMUEBLE = null;
                    ope.ID_TIPO = 2;



                    ope.FECHA_REGISTRO = thisDay;

                    ope.ESTADO_OPERACIÓN = "TRATADO";
                    ope.ESTADO_RESPUESTA = "NO ENVIADO";

                    //reclamo.ID_CATEGORÍA = 1;
                    ope.ID_CONTACTO = null;

                    int exito = Manager.QuejaManager.Instancia.TratarQueja(ope);

                    ListarQueja(1, " - OK");
                }

                return RedirectToAction("ListarQueja");
            }

            if (Request["btn"].Equals("Remitir"))
            {
                if (ModelState.IsValid)
                {
                    ope.ID_OPERACIÓN = queja.ID_OPERACIÓN;
                    ope.DNI_RUC = queja.DNI_RUC;
                    ope.DESCRIPCIÓN = queja.DESCRIPCIÓN;
                    ope.FECHA_REGISTRO = queja.FECHA_REGISTRO;
                    ope.NOM_CLIENTE = queja.NOM_CLIENTE;
                    ope.TELÉFONO = queja.TELÉFONO;
                    ope.DIRECCIÓN = queja.DIRECCIÓN;
                    ope.FECHA_REMISIÓN = null;
                    ope.FECHA_PLAZO = null;
                    ope.FECHA_RESPUESTA = null;
                    ope.R_MONTO_RECLAMADO = null;
                    ope.R_CÓDIGO_RECLAMADO = null;
                    ope.AREA_INVOLUCRADA = queja.AREA_INVOLUCRADA;
                    ope.SEDE_INVOLUCRADA = queja.SEDE_INVOLUCRADA;
                    ope.ID_CATEGORÍA = queja.ID_CATEGORÍA;



                    ope.ID_PEDIDO = null;
                    ope.ID_INTERLOCUTOR = queja.ID_INTERLOCUTOR;
                    ope.ID_POST_VENTA = null;

                    // Aun sin responder
                    ope.FECHA_REMISIÓN = null;
                    ope.FECHA_RESPUESTA = null;

                    ope.SOLUCIÓN = queja.SOLUCIÓN;
                    ope.ASESOR_RESPONSABLE = queja.ASESOR_RESPONSABLE;

                    ope.NOM_INTERESADO = null;

                    ope.ID_PRODUCTO = null;
                    ope.S_PRECIO_SOLICITUD = 0;
                    ope.S_GARANTÍA = null;
                    ope.S_TIPO_INMUEBLE = null;
                    ope.ID_TIPO = 2;



                    ope.FECHA_REGISTRO = thisDay;

                    ope.ESTADO_OPERACIÓN = "REMITIDO";
                    ope.ESTADO_RESPUESTA = "NO ENVIADO";

                    //reclamo.ID_CATEGORÍA = 1;
                    ope.ID_CONTACTO = null;

                    int exito = Manager.QuejaManager.Instancia.TratarQueja(ope);

                    ListarQueja(1, " - OK");
                }

                return RedirectToAction("ListarQueja");
            }
            return View(queja);
        }

        public ActionResult DetalleQueja(int id = 0)
        {
            ReclamoViewModel QUEJA = new ReclamoViewModel();

            QUEJA = Manager.QuejaManager.Instancia.ObtenerQueja(id);
            return View(QUEJA);
        }

    }
}
