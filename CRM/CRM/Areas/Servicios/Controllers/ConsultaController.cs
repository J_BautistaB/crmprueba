﻿using CRM.Areas.Servicios.Models;
using CRM.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM.Areas.Servicios.Controllers
{
    public class ConsultaController : Controller
    {
        private CRMEntities db = new CRMEntities();

        //
        // GET: /Consulta/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult ListarConsulta(int identMensaje = 0, string mensaje = "")
        {
            if (identMensaje == 0)
            {
            }
            else
            {
                ViewData["Mensaje"] = ObtenerMensaje(identMensaje) + mensaje;
            }
            ListaReclamoViewModel crv = new ListaReclamoViewModel();
            crv.lista = Manager.ConsultaManager.Instancia.ListarConsultas();

            return View(crv);

        }

        //
        // POST: /Cuota/Create

        [HttpPost]
        public ActionResult ListarConsulta(ListaReclamoViewModel cvm)
        {
            cvm.lista = Manager.ConsultaManager.Instancia.ListarConsultasFiltro(cvm.textobuscar);
            ListarConsulta(0, "");
            return View(cvm);
        }

        //Registrar Consulta
        public ActionResult RegistrarConsulta()
        {

            ReclamoViewModel rcv = new ReclamoViewModel();
            // Inicializamos combos box de registro
            rcv.lista_categoria = Manager.ConsultaManager.Instancia.ListarCategorias("Consulta");
            rcv.lista_areas = Manager.ConsultaManager.Instancia.ListaAreas();
            rcv.lista_sedes = Manager.ConsultaManager.Instancia.ListaSedes();

            return View(rcv);
        }


        //
        // POST: /Consulta/Create

        [HttpPost]
        public ActionResult RegistrarConsulta(ReclamoViewModel consulta)
        {

            DateTime thisDay = DateTime.Today;

            OPERACIONES ope = new OPERACIONES();

            if (ModelState.IsValid)
            {
                ope.DNI_RUC = consulta.DNI_RUC;
                ope.DESCRIPCIÓN = consulta.DESCRIPCIÓN;
                ope.FECHA_REGISTRO = consulta.FECHA_REGISTRO;
                ope.NOM_CLIENTE = consulta.NOM_CLIENTE;
                ope.TELÉFONO = consulta.TELÉFONO;
                ope.DIRECCIÓN = consulta.DIRECCIÓN;
                ope.FECHA_REMISIÓN = null;
                ope.FECHA_PLAZO = null;
                ope.FECHA_RESPUESTA = null;
                ope.R_MONTO_RECLAMADO = null;
                ope.R_CÓDIGO_RECLAMADO = null;
                ope.AREA_INVOLUCRADA = consulta.AREA_INVOLUCRADA;
                ope.SEDE_INVOLUCRADA = consulta.SEDE_INVOLUCRADA;
                ope.ID_CATEGORÍA = consulta.ID_CATEGORÍA;



                ope.ID_PEDIDO = null;
                ope.ID_INTERLOCUTOR = consulta.ID_INTERLOCUTOR;
                ope.ID_POST_VENTA = null;

                // Aun sin responder
                ope.FECHA_REMISIÓN = null;
                ope.FECHA_RESPUESTA = null;

                ope.SOLUCIÓN = "NO SE REGISTRA SOLUCIÓN";
                ope.ASESOR_RESPONSABLE = "NO SE REGISTRA ASESOR";

                ope.NOM_INTERESADO = null;

                ope.ID_PRODUCTO = null;
                ope.S_PRECIO_SOLICITUD = 0;
                ope.S_GARANTÍA = null;
                ope.S_TIPO_INMUEBLE = null;
                ope.ID_TIPO = 5;



                ope.FECHA_REGISTRO = thisDay;

                ope.ESTADO_OPERACIÓN = "REGISTRADO";
                ope.ESTADO_RESPUESTA = "NO ENVIADO";

                //reclamo.ID_CATEGORÍA = 1;
                ope.ID_CONTACTO = null;

                int exito = Manager.ConsultaManager.Instancia.RegistrarConsulta(ope);

                ListarConsulta(1, " - OK");
            }
            return View("ListarConsulta");
        }



        //mensajes generales para todas las vista de clientes
        public string ObtenerMensaje(int identMensaje)
        {
            switch (identMensaje)
            {
                case 0: return "";
                case 1: return "Consulta registrada con exito";
                case 2: return "No se registro consulta";
            }
            return "Mensaje no configurado";
        }




        public ActionResult TratarConsulta(int idr)
        {
            ReclamoViewModel CONSULTA = new ReclamoViewModel();

            CONSULTA = Manager.ConsultaManager.Instancia.ObtenerConsulta(idr);
            CONSULTA.lista_nombre_asesor = Manager.ConsultaManager.Instancia.lista_nombres_asesores(1);

            return View(CONSULTA);
        }


        [HttpPost]
        public ActionResult TratarConsulta(ReclamoViewModel consulta)
        {
            DateTime thisDay = DateTime.Today;
            OPERACIONES ope = new OPERACIONES();

            if (Request["btn"].Equals("Responder"))
            {
                if (ModelState.IsValid)
                {
                    ope.ID_OPERACIÓN = consulta.ID_OPERACIÓN;
                    ope.DNI_RUC = consulta.DNI_RUC;
                    ope.DESCRIPCIÓN = consulta.DESCRIPCIÓN;
                    ope.FECHA_REGISTRO = consulta.FECHA_REGISTRO;
                    ope.NOM_CLIENTE = consulta.NOM_CLIENTE;
                    ope.TELÉFONO = consulta.TELÉFONO;
                    ope.DIRECCIÓN = consulta.DIRECCIÓN;
                    ope.FECHA_REMISIÓN = null;
                    ope.FECHA_PLAZO = null;
                    ope.FECHA_RESPUESTA = null;
                    ope.R_MONTO_RECLAMADO = null;
                    ope.R_CÓDIGO_RECLAMADO = null;
                    ope.AREA_INVOLUCRADA = consulta.AREA_INVOLUCRADA;
                    ope.SEDE_INVOLUCRADA = consulta.SEDE_INVOLUCRADA;
                    ope.ID_CATEGORÍA = consulta.ID_CATEGORÍA;



                    ope.ID_PEDIDO = null;
                    ope.ID_INTERLOCUTOR = consulta.ID_INTERLOCUTOR;
                    ope.ID_POST_VENTA = null;

                    // Aun sin responder
                    ope.FECHA_REMISIÓN = null;
                    ope.FECHA_RESPUESTA = null;

                    ope.SOLUCIÓN = consulta.SOLUCIÓN;
                    ope.ASESOR_RESPONSABLE = consulta.ASESOR_RESPONSABLE;

                    ope.NOM_INTERESADO = null;

                    ope.ID_PRODUCTO = null;
                    ope.S_PRECIO_SOLICITUD = 0;
                    ope.S_GARANTÍA = null;
                    ope.S_TIPO_INMUEBLE = null;
                    ope.ID_TIPO = 5;



                    ope.FECHA_REGISTRO = thisDay;

                    ope.ESTADO_OPERACIÓN = "TRATADO";
                    ope.ESTADO_RESPUESTA = "NO ENVIADO";

                    //reclamo.ID_CATEGORÍA = 1;
                    ope.ID_CONTACTO = null;

                    int exito = Manager.ConsultaManager.Instancia.TratarConsulta(ope);

                    ListarConsulta(1, " - OK");
                }

                return RedirectToAction("ListarConsulta");
            }

            if (Request["btn"].Equals("Remitir"))
            {
                if (ModelState.IsValid)
                {
                    ope.ID_OPERACIÓN = consulta.ID_OPERACIÓN;
                    ope.DNI_RUC = consulta.DNI_RUC;
                    ope.DESCRIPCIÓN = consulta.DESCRIPCIÓN;
                    ope.FECHA_REGISTRO = consulta.FECHA_REGISTRO;
                    ope.NOM_CLIENTE = consulta.NOM_CLIENTE;
                    ope.TELÉFONO = consulta.TELÉFONO;
                    ope.DIRECCIÓN = consulta.DIRECCIÓN;
                    ope.FECHA_REMISIÓN = null;
                    ope.FECHA_PLAZO = null;
                    ope.FECHA_RESPUESTA = null;
                    ope.R_MONTO_RECLAMADO = null;
                    ope.R_CÓDIGO_RECLAMADO = null;
                    ope.AREA_INVOLUCRADA = consulta.AREA_INVOLUCRADA;
                    ope.SEDE_INVOLUCRADA = consulta.SEDE_INVOLUCRADA;
                    ope.ID_CATEGORÍA = consulta.ID_CATEGORÍA;



                    ope.ID_PEDIDO = null;
                    ope.ID_INTERLOCUTOR = consulta.ID_INTERLOCUTOR;
                    ope.ID_POST_VENTA = null;

                    // Aun sin responder
                    ope.FECHA_REMISIÓN = null;
                    ope.FECHA_RESPUESTA = null;

                    ope.SOLUCIÓN = consulta.SOLUCIÓN;
                    ope.ASESOR_RESPONSABLE = consulta.ASESOR_RESPONSABLE;

                    ope.NOM_INTERESADO = null;

                    ope.ID_PRODUCTO = null;
                    ope.S_PRECIO_SOLICITUD = 0;
                    ope.S_GARANTÍA = null;
                    ope.S_TIPO_INMUEBLE = null;
                    ope.ID_TIPO = 5;



                    ope.FECHA_REGISTRO = thisDay;

                    ope.ESTADO_OPERACIÓN = "REMITIDO";
                    ope.ESTADO_RESPUESTA = "NO ENVIADO";

                    //reclamo.ID_CATEGORÍA = 1;
                    ope.ID_CONTACTO = null;

                    int exito = Manager.ConsultaManager.Instancia.TratarConsulta(ope);

                    ListarConsulta(1, " - OK");
                }

                return RedirectToAction("ListarConsulta");
            }

            return View(consulta);
        }

        public ActionResult DetalleConsulta(int id= 0)
        {
            ReclamoViewModel CONSULTA = new ReclamoViewModel();

            CONSULTA = Manager.ConsultaManager.Instancia.ObtenerConsulta(id);

            return View(CONSULTA);
        }

    }
}
