﻿using System.Web.Mvc;

namespace CRM.Areas.PostVenta
{
    public class PostVentaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PostVenta";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PostVenta_default",
                "PostVenta/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
