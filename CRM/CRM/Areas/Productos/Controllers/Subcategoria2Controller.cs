﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM.Entity;

namespace CRM.Areas.Productos.Controllers
{
    public class Subcategoria2Controller : Controller
    {
        private CRMEntities db = new CRMEntities();

        //
        // GET: /Productos/Subcategoria2/

        public ActionResult Index()
        {
            var subcategoria2 = db.SUBCATEGORIA2.Include(s => s.SUBCATEGORIA1);
            return View(subcategoria2.ToList());
        }

        //
        // GET: /Productos/Subcategoria2/Details/5

        public ActionResult Details(int id = 0)
        {
            SUBCATEGORIA2 subcategoria2 = db.SUBCATEGORIA2.Find(id);
            if (subcategoria2 == null)
            {
                return HttpNotFound();
            }
            return View(subcategoria2);
        }

        //
        // GET: /Productos/Subcategoria2/Create

        public ActionResult Create()
        {
            ViewBag.ID_SUBCATEGORIA1 = new SelectList(db.SUBCATEGORIA1, "ID_SUBCATEGORIA1", "NOMBRE_SUBCATEGORIA1");
            return View();
        }

        //
        // POST: /Productos/Subcategoria2/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SUBCATEGORIA2 subcategoria2)
        {
            if (ModelState.IsValid)
            {
                db.SUBCATEGORIA2.Add(subcategoria2);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ID_SUBCATEGORIA1 = new SelectList(db.SUBCATEGORIA1, "ID_SUBCATEGORIA1", "NOMBRE_SUBCATEGORIA1", subcategoria2.ID_SUBCATEGORIA1);
            return View(subcategoria2);
        }

        //
        // GET: /Productos/Subcategoria2/Edit/5

        public ActionResult Edit(int id = 0)
        {
            SUBCATEGORIA2 subcategoria2 = db.SUBCATEGORIA2.Find(id);
            if (subcategoria2 == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_SUBCATEGORIA1 = new SelectList(db.SUBCATEGORIA1, "ID_SUBCATEGORIA1", "NOMBRE_SUBCATEGORIA1", subcategoria2.ID_SUBCATEGORIA1);
            return View(subcategoria2);
        }

        //
        // POST: /Productos/Subcategoria2/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SUBCATEGORIA2 subcategoria2)
        {
            if (ModelState.IsValid)
            {
                db.Entry(subcategoria2).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ID_SUBCATEGORIA1 = new SelectList(db.SUBCATEGORIA1, "ID_SUBCATEGORIA1", "NOMBRE_SUBCATEGORIA1", subcategoria2.ID_SUBCATEGORIA1);
            return View(subcategoria2);
        }

        //
        // GET: /Productos/Subcategoria2/Delete/5

        public ActionResult Delete(int id = 0)
        {
            SUBCATEGORIA2 subcategoria2 = db.SUBCATEGORIA2.Find(id);
            if (subcategoria2 == null)
            {
                return HttpNotFound();
            }
            return View(subcategoria2);
        }

        //
        // POST: /Productos/Subcategoria2/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SUBCATEGORIA2 subcategoria2 = db.SUBCATEGORIA2.Find(id);
            db.SUBCATEGORIA2.Remove(subcategoria2);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}