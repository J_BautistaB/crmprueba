﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM.Entity;
using System.Data;
using System.Data.OleDb;
using System.Xml;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using Excel = Microsoft.Office.Interop.Excel;
using CRM.Areas.Productos.Models;
using System.Runtime.InteropServices;

namespace CRM.Areas.Productos.Controllers
{
    public class ProductoController : Controller
    {
        private CRMEntities db = new CRMEntities();

        //
        // GET: /Productos/Producto/

        //public ActionResult Index()
        //{
        //    var producto = db.PRODUCTO.Include(p => p.CATEGORIA).Include(p => p.PROVEEDOR).Include(p => p.SUBCATEGORIA1).Include(p => p.SUBCATEGORIA2);
        //    return View(producto.ToList());
        //}

        //[HttpPost]
        public ActionResult Index(string busc = "")
        {
            var lista=(from p in db.PRODUCTO where p.NOMBRE_PRODUCTO.Contains(busc) select p).ToList();
            return View(lista);
        }

        public ActionResult Export()
        {
            try
            {
                Excel.Application application = new Excel.Application();
                Excel.Workbook workbook = application.Workbooks.Add(System.Reflection.Missing.Value);
                Excel.Worksheet worksheet = workbook.ActiveSheet;
                ProductModel pm = new ProductModel();
                worksheet.Cells[1, 1] = "ID_PRODUCTO";
                worksheet.Cells[1, 2] = "CODIGO_PRODUCTO";
                worksheet.Cells[1, 3] = "NOMBRE_PRODUCTO";
                worksheet.Cells[1, 4] = "MODELO_PRODUCTO";
                worksheet.Cells[1, 5] = "ID_CATEGORIA";
                worksheet.Cells[1, 6] = "ID_SUBCATEGORIA1";
                worksheet.Cells[1, 7] = "ID_SUBCATEGORIA2";
                worksheet.Cells[1, 8] = "MARCA";
                worksheet.Cells[1, 9] = "FECHA_PRODUCCION";
                worksheet.Cells[1, 10] = "PRECIO_PRODUCTO";
                worksheet.Cells[1, 11] = "GARANTIA";
                worksheet.Cells[1, 12] = "STOCK";
                worksheet.Cells[1, 13] = "STOCK_MINIMO";
                worksheet.Cells[1, 14] = "ID_PROVEEDOR";
                worksheet.Cells[1, 15] = "OFERTA";
                worksheet.Cells[1, 16] = "DIRECCION";
                worksheet.Cells[1, 17] = "FECHA_INGRESO";
                worksheet.Cells[1, 18] = "ESTADO_PRODUCTO";
                int row = 2;
                foreach (PRODUCTO p in pm.findAll())
                {
                    worksheet.Cells[row, 1] = p.ID_PRODUCTO;
                    worksheet.Cells[row, 2] = p.CODIGO_PRODUCTO;
                    worksheet.Cells[row, 3] = p.NOMBRE_PRODUCTO;
                    worksheet.Cells[row, 4] = p.MODELO_PRODUCTO;
                    worksheet.Cells[row, 5] = p.ID_CATEGORIA;
                    worksheet.Cells[row, 6] = p.ID_SUBCATEGORIA1;
                    worksheet.Cells[row, 7] = p.ID_SUBCATEGORIA2;
                    worksheet.Cells[row, 8] = p.MARCA;
                    worksheet.Cells[row, 9] = p.FECHA_PRODUCCION;
                    worksheet.Cells[row, 10] = p.PRECIO_PRODUCTO;
                    worksheet.Cells[row, 11] = p.GARANTIA;
                    worksheet.Cells[row, 12] = p.STOCK;
                    worksheet.Cells[row, 13] = p.STOCK_MINIMO;
                    worksheet.Cells[row, 14] = p.ID_PROVEEDOR;
                    worksheet.Cells[row, 15] = p.OFERTA;
                    worksheet.Cells[row, 16] = p.DIRECCION;
                    worksheet.Cells[row, 17] = p.FECHA_INGRESO;
                    worksheet.Cells[row, 18] = p.ESTADO_PRODUCTO;
                    row++;
                }
                workbook.SaveAs("f:\\myproduct.xls");
                workbook.Close();
                Marshal.ReleaseComObject(workbook);

                application.Quit();
                ViewBag.Resul = "Done";

            }
            catch (Exception ex)
            {
                ViewBag.Resul = ex.Message;
            }
            return View("Success");
        }


        public ActionResult CargaMasiva()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CargaMasiva(HttpPostedFileBase file)
        {
            DataSet ds = new DataSet();
            if (Request.Files["file"].ContentLength > 0)
            {
                string fileExtension = System.IO.Path.GetExtension(Request.Files["file"].FileName);

                if (fileExtension == ".xls" || fileExtension == ".xlsx")
                {
                    string fileLocation = Server.MapPath("~/Content/") + Request.Files["file"].FileName;
                    if (System.IO.File.Exists(fileLocation))
                    {

                        System.IO.File.Delete(fileLocation);
                    }
                    Request.Files["file"].SaveAs(fileLocation);
                    string excelConnectionString = string.Empty;
                    excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                    fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +
                        fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {
                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                        fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }
                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }
                }
                if (fileExtension.ToString().ToLower().Equals(".xml"))
                {
                    string fileLocation = Server.MapPath("~/Content/") + Request.Files["FileUpload"].FileName;
                    if (System.IO.File.Exists(fileLocation))
                    {
                        System.IO.File.Delete(fileLocation);
                    }

                    Request.Files["FileUpload"].SaveAs(fileLocation);
                    XmlTextReader xmlreader = new XmlTextReader(fileLocation);
                    // DataSet ds = new DataSet();
                    ds.ReadXml(xmlreader);
                    xmlreader.Close();
                }

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string conn = ConfigurationManager.ConnectionStrings["conexion"].ConnectionString;
                    SqlConnection con = new SqlConnection(conn);
                    string query = "Insert into producto(nombre_producto,modelo_producto,id_categoria,id_subcategoria1,id_subcategoria2,marca,fecha_produccion,precio_producto,garantia,stock,stock_minimo,id_proveedor,oferta,direccion,fecha_ingreso,estado_producto) Values('" +
                    ds.Tables[0].Rows[i][0].ToString() + "','" + ds.Tables[0].Rows[i][1].ToString() +
                    "','" + ds.Tables[0].Rows[i][2].ToString() + "','" + ds.Tables[0].Rows[i][3].ToString() + "','" + ds.Tables[0].Rows[i][4].ToString() +
                    "','" + ds.Tables[0].Rows[i][5].ToString() + "','" + ds.Tables[0].Rows[i][6].ToString() + "','" + ds.Tables[0].Rows[i][7].ToString() +
                    "','" + ds.Tables[0].Rows[i][8].ToString() + "','" + ds.Tables[0].Rows[i][9].ToString() + "','" + ds.Tables[0].Rows[i][10].ToString() +
                    "','" + ds.Tables[0].Rows[i][11].ToString() + "','" + ds.Tables[0].Rows[i][12].ToString() + "','" + ds.Tables[0].Rows[i][13].ToString() +
                    "','" + ds.Tables[0].Rows[i][14].ToString() + "','" + ds.Tables[0].Rows[i][15].ToString() + "')";
                    con.Open();
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                
            }
            return View();
        }




        //
        // GET: /Productos/Producto/Details/5

        public ActionResult Details(int id = 0)
        {
            PRODUCTO producto = db.PRODUCTO.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        //
        // GET: /Productos/Producto/Create

        public ActionResult Create()
        {
            ViewBag.ID_CATEGORIA = new SelectList(db.CATEGORIA, "ID_CATEGORIA", "NOMBRE_CATEGORIA");
            ViewBag.ID_PROVEEDOR = new SelectList(db.PROVEEDOR, "ID_PROVEEDOR", "NOMBRE_PROVEEDOR");
            ViewBag.ID_SUBCATEGORIA1 = new SelectList(db.SUBCATEGORIA1, "ID_SUBCATEGORIA1", "NOMBRE_SUBCATEGORIA1");
            ViewBag.ID_SUBCATEGORIA2 = new SelectList(db.SUBCATEGORIA2, "ID_SUBCATEGORIA2", "NOMBRE_SUBCATEGORIA2");
            return View();
        }

        //
        // POST: /Productos/Producto/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PRODUCTO producto)
        {
            if (ModelState.IsValid)
            {
                db.PRODUCTO.Add(producto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ID_CATEGORIA = new SelectList(db.CATEGORIA, "ID_CATEGORIA", "NOMBRE_CATEGORIA", producto.ID_CATEGORIA);
            ViewBag.ID_PROVEEDOR = new SelectList(db.PROVEEDOR, "ID_PROVEEDOR", "NOMBRE_PROVEEDOR", producto.ID_PROVEEDOR);
            ViewBag.ID_SUBCATEGORIA1 = new SelectList(db.SUBCATEGORIA1, "ID_SUBCATEGORIA1", "NOMBRE_SUBCATEGORIA1", producto.ID_SUBCATEGORIA1);
            ViewBag.ID_SUBCATEGORIA2 = new SelectList(db.SUBCATEGORIA2, "ID_SUBCATEGORIA2", "NOMBRE_SUBCATEGORIA2", producto.ID_SUBCATEGORIA2);
            return View(producto);
        }

        //
        // GET: /Productos/Producto/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PRODUCTO producto = db.PRODUCTO.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_CATEGORIA = new SelectList(db.CATEGORIA, "ID_CATEGORIA", "NOMBRE_CATEGORIA", producto.ID_CATEGORIA);
            ViewBag.ID_PROVEEDOR = new SelectList(db.PROVEEDOR, "ID_PROVEEDOR", "NOMBRE_PROVEEDOR", producto.ID_PROVEEDOR);
            ViewBag.ID_SUBCATEGORIA1 = new SelectList(db.SUBCATEGORIA1, "ID_SUBCATEGORIA1", "NOMBRE_SUBCATEGORIA1", producto.ID_SUBCATEGORIA1);
            ViewBag.ID_SUBCATEGORIA2 = new SelectList(db.SUBCATEGORIA2, "ID_SUBCATEGORIA2", "NOMBRE_SUBCATEGORIA2", producto.ID_SUBCATEGORIA2);
            return View(producto);
        }

        //
        // POST: /Productos/Producto/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PRODUCTO producto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(producto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ID_CATEGORIA = new SelectList(db.CATEGORIA, "ID_CATEGORIA", "NOMBRE_CATEGORIA", producto.ID_CATEGORIA);
            ViewBag.ID_PROVEEDOR = new SelectList(db.PROVEEDOR, "ID_PROVEEDOR", "NOMBRE_PROVEEDOR", producto.ID_PROVEEDOR);
            ViewBag.ID_SUBCATEGORIA1 = new SelectList(db.SUBCATEGORIA1, "ID_SUBCATEGORIA1", "NOMBRE_SUBCATEGORIA1", producto.ID_SUBCATEGORIA1);
            ViewBag.ID_SUBCATEGORIA2 = new SelectList(db.SUBCATEGORIA2, "ID_SUBCATEGORIA2", "NOMBRE_SUBCATEGORIA2", producto.ID_SUBCATEGORIA2);
            return View(producto);
        }

        //
        // GET: /Productos/Producto/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PRODUCTO producto = db.PRODUCTO.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        //
        // POST: /Productos/Producto/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PRODUCTO producto = db.PRODUCTO.Find(id);
            db.PRODUCTO.Remove(producto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}