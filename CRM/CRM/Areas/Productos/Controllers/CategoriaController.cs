﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM.Entity;

namespace CRM.Areas.Productos.Controllers
{
    public class CategoriaController : Controller
    {
        private CRMEntities db = new CRMEntities();

        //
        // GET: /Productos/Categoria/

        public ActionResult Index()
        {
            return View(db.CATEGORIA.ToList());
        }

        //
        // GET: /Productos/Categoria/Details/5

        public ActionResult Details(int id = 0)
        {
            CATEGORIA categoria = db.CATEGORIA.Find(id);
            if (categoria == null)
            {
                return HttpNotFound();
            }
            return View(categoria);
        }

        //
        // GET: /Productos/Categoria/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Productos/Categoria/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CATEGORIA categoria)
        {
            if (ModelState.IsValid)
            {
                db.CATEGORIA.Add(categoria);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(categoria);
        }

        //
        // GET: /Productos/Categoria/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CATEGORIA categoria = db.CATEGORIA.Find(id);
            if (categoria == null)
            {
                return HttpNotFound();
            }
            return View(categoria);
        }

        //
        // POST: /Productos/Categoria/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CATEGORIA categoria)
        {
            if (ModelState.IsValid)
            {
                db.Entry(categoria).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(categoria);
        }

        //
        // GET: /Productos/Categoria/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CATEGORIA categoria = db.CATEGORIA.Find(id);
            if (categoria == null)
            {
                return HttpNotFound();
            }
            return View(categoria);
        }

        //
        // POST: /Productos/Categoria/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CATEGORIA categoria = db.CATEGORIA.Find(id);
            db.CATEGORIA.Remove(categoria);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}