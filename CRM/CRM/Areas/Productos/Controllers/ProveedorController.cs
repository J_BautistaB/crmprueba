﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM.Entity;

namespace CRM.Areas.Productos.Controllers
{
    public class ProveedorController : Controller
    {
        private CRMEntities db = new CRMEntities();

        //
        // GET: /Productos/Proveedor/

        public ActionResult Index()
        {
            return View(db.PROVEEDOR.ToList());
        }

        //
        // GET: /Productos/Proveedor/Details/5

        public ActionResult Details(int id = 0)
        {
            PROVEEDOR proveedor = db.PROVEEDOR.Find(id);
            if (proveedor == null)
            {
                return HttpNotFound();
            }
            return View(proveedor);
        }

        //
        // GET: /Productos/Proveedor/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Productos/Proveedor/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PROVEEDOR proveedor)
        {
            if (ModelState.IsValid)
            {
                db.PROVEEDOR.Add(proveedor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(proveedor);
        }

        //
        // GET: /Productos/Proveedor/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PROVEEDOR proveedor = db.PROVEEDOR.Find(id);
            if (proveedor == null)
            {
                return HttpNotFound();
            }
            return View(proveedor);
        }

        //
        // POST: /Productos/Proveedor/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PROVEEDOR proveedor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(proveedor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(proveedor);
        }

        //
        // GET: /Productos/Proveedor/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PROVEEDOR proveedor = db.PROVEEDOR.Find(id);
            if (proveedor == null)
            {
                return HttpNotFound();
            }
            return View(proveedor);
        }

        //
        // POST: /Productos/Proveedor/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PROVEEDOR proveedor = db.PROVEEDOR.Find(id);
            db.PROVEEDOR.Remove(proveedor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}