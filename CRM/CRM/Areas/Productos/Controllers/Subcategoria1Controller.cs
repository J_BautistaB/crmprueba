﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM.Entity;

namespace CRM.Areas.Productos.Controllers
{
    public class Subcategoria1Controller : Controller
    {
        private CRMEntities db = new CRMEntities();

        //
        // GET: /Productos/Subcategoria1/

        public ActionResult Index()
        {
            var subcategoria1 = db.SUBCATEGORIA1.Include(s => s.CATEGORIA);
            return View(subcategoria1.ToList());
        }

        //
        // GET: /Productos/Subcategoria1/Details/5

        public ActionResult Details(int id = 0)
        {
            SUBCATEGORIA1 subcategoria1 = db.SUBCATEGORIA1.Find(id);
            if (subcategoria1 == null)
            {
                return HttpNotFound();
            }
            return View(subcategoria1);
        }

        //
        // GET: /Productos/Subcategoria1/Create

        public ActionResult Create()
        {
            ViewBag.ID_CATEGORIA = new SelectList(db.CATEGORIA, "ID_CATEGORIA", "NOMBRE_CATEGORIA");
            return View();
        }

        //
        // POST: /Productos/Subcategoria1/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SUBCATEGORIA1 subcategoria1)
        {
            if (ModelState.IsValid)
            {
                db.SUBCATEGORIA1.Add(subcategoria1);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ID_CATEGORIA = new SelectList(db.CATEGORIA, "ID_CATEGORIA", "NOMBRE_CATEGORIA", subcategoria1.ID_CATEGORIA);
            return View(subcategoria1);
        }

        //
        // GET: /Productos/Subcategoria1/Edit/5

        public ActionResult Edit(int id = 0)
        {
            SUBCATEGORIA1 subcategoria1 = db.SUBCATEGORIA1.Find(id);
            if (subcategoria1 == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_CATEGORIA = new SelectList(db.CATEGORIA, "ID_CATEGORIA", "NOMBRE_CATEGORIA", subcategoria1.ID_CATEGORIA);
            return View(subcategoria1);
        }

        //
        // POST: /Productos/Subcategoria1/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SUBCATEGORIA1 subcategoria1)
        {
            if (ModelState.IsValid)
            {
                db.Entry(subcategoria1).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ID_CATEGORIA = new SelectList(db.CATEGORIA, "ID_CATEGORIA", "NOMBRE_CATEGORIA", subcategoria1.ID_CATEGORIA);
            return View(subcategoria1);
        }

        //
        // GET: /Productos/Subcategoria1/Delete/5

        public ActionResult Delete(int id = 0)
        {
            SUBCATEGORIA1 subcategoria1 = db.SUBCATEGORIA1.Find(id);
            if (subcategoria1 == null)
            {
                return HttpNotFound();
            }
            return View(subcategoria1);
        }

        //
        // POST: /Productos/Subcategoria1/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SUBCATEGORIA1 subcategoria1 = db.SUBCATEGORIA1.Find(id);
            db.SUBCATEGORIA1.Remove(subcategoria1);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}