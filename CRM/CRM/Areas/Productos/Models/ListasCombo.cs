﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM.Areas.Productos.Models
{
    public class ListasCombo
    {
        public IEnumerable<SelectListItem> ListaComboCategoria { get; set; }
    }
}