﻿using CRM.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace CRM.Areas.Productos.Models
{
    public class ProductModel
    {
        private CRMEntities db = new CRMEntities();
        public List<PRODUCTO> findAll() {
            List<PRODUCTO> listProducts = new List<PRODUCTO>();
            var lista = (from p in db.PRODUCTO select p).ToList();
            foreach (PRODUCTO s in lista) {
                listProducts.Add(s);
            }
            
            return listProducts;
        }
    }
}