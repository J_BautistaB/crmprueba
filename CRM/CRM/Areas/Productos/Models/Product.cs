﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Areas.Productos.Models
{
    public class Product
    {
        public int ID_PRODUCTO { get; set; }
        public string CODIGO_PRODUCTO { get; set; }
        public string NOMBRE_PRODUCTO { get; set; }
        public string MODELO_PRODUCTO { get; set; }
        public int ID_CATEGORIA { get; set; }
        public int ID_SUBCATEGORIA1 { get; set; }
        public int ID_SUBCATEGORIA2 { get; set; }
        public string MARCA { get; set; }
        public string FECHA_PRODUCCION { get; set; }
        public decimal PRECIO_PRODUCTO { get; set; }
        public int GARANTIA { get; set; }
        public int STOCK { get; set; }
        public int STOCK_MINIMO { get; set; }
        public int ID_PROVEEDOR { get; set; }
        public bool OFERTA { get; set; }
        public string DIRECCION { get; set; }
        public string FECHA_INGRESO { get; set; }
        public bool ESTADO_PRODUCTO { get; set; }
    }
}