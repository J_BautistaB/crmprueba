//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CRM.Entity
{
    using System;
    
    public partial class USP_RELACION_OBTENER_Result
    {
        public int ID_RELACION { get; set; }
        public int ID_INTERLOCUTOR_RELACIONADO { get; set; }
        public int ID_INTERLOCUTOR { get; set; }
        public int IDP_RELACION { get; set; }
        public string DESCRIPCION { get; set; }
        public string NOMBRE_RELACIONADO { get; set; }
    }
}
