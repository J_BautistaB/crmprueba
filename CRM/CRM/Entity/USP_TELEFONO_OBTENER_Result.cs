//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CRM.Entity
{
    using System;
    
    public partial class USP_TELEFONO_OBTENER_Result
    {
        public int ID_TELEFONO { get; set; }
        public int ID_INTERLOCUTOR { get; set; }
        public int IDP_OPERADOR { get; set; }
        public string OPERADOR { get; set; }
        public int IDP_TIPO_TELEFONO { get; set; }
        public string TIPO_TELEFONO { get; set; }
        public string NUMERO { get; set; }
    }
}
