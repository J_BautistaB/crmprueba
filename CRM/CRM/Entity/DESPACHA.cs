//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CRM.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class DESPACHA
    {
        public DESPACHA()
        {
            this.INSTALACION = new HashSet<INSTALACION>();
        }
    
        public int ID_DESPACHO { get; set; }
        public int ID_INTERLOCUTOR_D { get; set; }
        public int ID_PEDIDO_D { get; set; }
        public int ID_RUTA_D { get; set; }
        public int ID_PROGRAMACION_D { get; set; }
        public string ESTADO_DESPACHO { get; set; }
        public string PRIORIAD { get; set; }
    
        public virtual INTERLOCUTOR_COMERCIAL INTERLOCUTOR_COMERCIAL { get; set; }
        public virtual PEDIDO PEDIDO { get; set; }
        public virtual RUTA RUTA { get; set; }
        public virtual PROGRAMACION PROGRAMACION { get; set; }
        public virtual ICollection<INSTALACION> INSTALACION { get; set; }
    }
}
