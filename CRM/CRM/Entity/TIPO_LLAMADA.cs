//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CRM.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class TIPO_LLAMADA
    {
        public TIPO_LLAMADA()
        {
            this.CONTACTO = new HashSet<CONTACTO>();
        }
    
        public int ID_TIPO_LLAMADA { get; set; }
        public string DESCRIPCION_TIPO { get; set; }
    
        public virtual ICollection<CONTACTO> CONTACTO { get; set; }
    }
}
