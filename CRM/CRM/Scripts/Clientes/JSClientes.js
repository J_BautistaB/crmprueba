﻿//----Inicio Relaciones-----
function agregarFilaRelacion() {
    var nombreRelacion = $('#txtNombreRelacion').val();
    if (nombreRelacion.length == 0) {
        $('#validaRelacion').text('Debe buscar un cliente para agregar.');
    } else {
        col1 = $('#idguardado').val();
        col2 = nombreRelacion;
        col3 = '<input type="hidden" value="' + $("#ComboSeleccionRelacion option:selected").val() + '">' + $("#ComboSeleccionRelacion option:selected").text();
        col4 = '<button type="button" class="btn btn-danger btn-xs" onclick="eliminarFilaRelacion(this)"><span class="glyphicon glyphicon-remove"></span></button>';
        fila = "<tr><td>" + col1 + "</td><td>" + col2 + "</td><td>" + col3 + "</td><td>" + col4 + "</td></tr>";
        $('#tablaRelaciones > tbody').append(fila);
        $('#txtNombreRelacion').val('');
        $('#validaRelacion').text('');
    }
}

function eliminarFilaRelacion(r) {
    i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tablaRelaciones").deleteRow(i);
}
//----FIN RELACIONES----


//----Inicio Telefonos-----
function agregarFilaTelefono() {
    var numTelefono = $('#txtTelefono').val();
    if (numTelefono.length == 0) {
        $('#validaTelefono').text('Debe ingresar un número a agregar.');
    } else {
        col1 = numTelefono;
        col2 = '<input type="hidden" value="' + $("#ComboSeleccionTelefono option:selected").val() + '">' + $("#ComboSeleccionTelefono option:selected").text();
        col3 = '<button type="button" class="btn btn-danger btn-xs" onclick="eliminarFilaTelefono(this)"><span class="glyphicon glyphicon-remove"></span></button>';
        fila = "<tr><td>" + col1 + "</td><td>" + col2 + "</td><td>" + col3 + "</td></tr>";
        $('#tablaTelefonos > tbody').append(fila);
        $('#txtTelefono').val('');
        $('#validaTelefono').text('');
    }
}

function eliminarFilaTelefono(r) {
    i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tablaTelefonos").deleteRow(i);
}
//----FIN Telefonos----

//----Inicio Correos-----
function agregarFilaCorreo() {
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var nombreCorreo = $('#txtCorreo').val();
    if (nombreCorreo.length == 0) {
        $('#validaCorreo').text('No puede agregar un correo vacío.');
    } else {
        if (expr.test(nombreCorreo)) {
            col1 = nombreCorreo;
            col2 = '<button type="button" class="btn btn-danger btn-xs" onclick="eliminarFilaCorreo(this)"><span class="glyphicon glyphicon-remove"></span></button>';
            fila = "<tr><td>" + col1 + "</td><td>" + col2 + "</td></tr>";
            $('#tablaCorreos > tbody').append(fila);
            $('#txtCorreo').val('');
            $('#validaCorreo').text('');
        } else {
            $('#validaCorreo').text('Formato de correo no válido.');
        }
    }
}

function eliminarFilaCorreo(r) {
    i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tablaCorreos").deleteRow(i);
}
//----FIN Correos----


//----INICIO DIRECCIONES----
function agregarFilaDireccion() {
    var nombreDireccion = $('#txtDireccion').val();
    var nombreDpto = $("#ComboSeleccionDepartamento option:selected").text();
    var nombreProv = $("#ComboSeleccionProvincia option:selected").text();
    var nombreDistr = $("#ComboSeleccionDistrito option:selected").text();
    if (nombreDireccion.length == 0) {
        $('#validaDireccion').text('No puede ingresar una dirección vacía.');
    } else {
        if (nombreDpto.length == 0) {
            $('#validaDireccion').text('Debe seleccionar un departamento.');
        } else {
            if (nombreProv.length == 0) {
                $('#validaDireccion').text('Debe seleccionar una provincia.');
            } else {
                if (nombreDistr.length == 0) {
                    $('#validaDireccion').text('Debe seleccionar un distrito.');
                } else {
                    col1 = nombreDireccion;
                    col2 = '<input type="hidden" value="' + $("#ComboSeleccionDepartamento option:selected").val() + '">' + nombreDpto;
                    col3 = '<input type="hidden" value="' + $("#ComboSeleccionProvincia option:selected").val() + '">' + nombreProv;
                    col4 = '<input type="hidden" value="' + $("#ComboSeleccionDistrito option:selected").val() + '">' + nombreDistr;
                    col5 = '<button type="button" class="btn btn-danger btn-xs" onclick="eliminarFilaDireccion(this)"><span class="glyphicon glyphicon-remove"></span></button>';
                    fila = "<tr><td>" + col1 + "</td><td>" + col2 + "</td><td>" + col3 + "</td><td>" + col4 + "</td><td>" + col5 + "</td></tr>";
                    $('#tablaDirecciones > tbody').append(fila);
                    $('#txtDireccion').val('');
                    $('#validaDireccion').text('');
                }
            }
        }
    }
}

function eliminarFilaDireccion(r) {
    i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tablaDirecciones").deleteRow(i);
}
//----FIN DIRECCIONES----

//---- Inicio agregar estilo a textbox nacimiento ----
function existeClase(obj, cls) {
    return obj.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
}

function addClass(obj, cls) {
    if (!existeClase(obj, cls)) {
        obj.className += " " + cls;
    }
}

var nac = document.getElementById("Nacimiento");
if (nac != null) {
    addClass(nac, "form-control");
}
//---- Fin agregar estilo a textbox nacimiento ----


//----Inicio Crear JSON de detalles-----
function crearJsonDetalles() {
    var detalles = {
        relaciones: [],
        telefonos: [],
        correos: [],
        direcciones: []
    };

    //Extrayendo relaciones...
    $('#tablaRelaciones tbody tr').each(function () {
        idRelacionado = $(this).find("td").eq(0).text();
        nombreRelacionado = $(this).find("td").eq(1).text();
        idRelacion = $(this).find("td").eq(2).find("input").val();
        nombreRelacion = $(this).find("td").eq(2).text();
        detalles.relaciones.push({
            "IDRelacionado": idRelacionado,
            "IDRelacion": idRelacion,
            "NombreRelacionado": nombreRelacionado,
            "NombreRelacion": nombreRelacion
        });
    });

    //Extrayendo telefonos...
    $('#tablaTelefonos tbody tr').each(function () {
        numero = $(this).find("td").eq(0).text();
        idTipoTelefono = $(this).find("td").eq(1).find("input").val();
        tipoTelefono = $(this).find("td").eq(1).text();
        detalles.telefonos.push({
            "Numero": numero,
            "TipoTelefono": tipoTelefono,
            "IDTipoTelefono": idTipoTelefono
        });
    });

    //Extrayendo correos...
    $('#tablaCorreos tbody tr').each(function () {
        email = $(this).find("td").eq(0).text();
        detalles.correos.push({
            "Email": email
        });
    });

    //Extrayendo direcciones...
    $('#tablaDirecciones tbody tr').each(function () {
        nombreDireccion = $(this).find("td").eq(0).text();
        idDepartamento = $(this).find("td").eq(1).find("input").val();
        nombreDepartamento = $(this).find("td").eq(1).text();
        idProvincia = $(this).find("td").eq(2).find("input").val();
        nombreProvincia = $(this).find("td").eq(2).text();
        idDistrito = $(this).find("td").eq(3).find("input").val();
        nombreDistrito = $(this).find("td").eq(3).text();
        detalles.direcciones.push({
            "NombreDireccion": nombreDireccion,
            "IDDepartamento": idDepartamento,
            "NombreDepartamento": nombreDepartamento,
            "IDProvincia": idProvincia,
            "NombreProvincia": nombreProvincia,
            "IDDistrito": idDistrito,
            "NombreDistrito": nombreDistrito
        });
    });

    var strJsonDetalles = JSON.stringify(detalles);
    $('#infoJSON').val(strJsonDetalles);
}

//----Fin Crear JSON de detalles-----

//---- Inicio llenado de DropdownList Ubicaciones----
$(function () {
    $("#ComboSeleccionDepartamento").change(function () {
        var selectedItem = $(this).val();
        var comboProvincias = $("#ComboSeleccionProvincia");
        $.ajax({
            cache: false,
            type: 'GET',
            datatype: 'json',
            contentType: 'application/json',
            url: '/MantenerClienteNatural/ObtenerProvicias',
            data: { "iddepartamento": selectedItem },
            success: function (data) {
                comboProvincias.html('');
                for (var i = 0; i < data.length; i++) {
                    comboProvincias.append($('<option></option>').val(data[i].ID).html(data[i].Nombre));
                }
                $('#ComboSeleccionProvincia').trigger('change');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log('Error al cargar provincias.');
            }
        });
    });
});

$(function () {
    $("#ComboSeleccionProvincia").change(function () {
        var selectedItem = $(this).val();
        var comboDistrito = $("#ComboSeleccionDistrito");
        $.ajax({
            cache: false,
            type: 'GET',
            datatype: 'json',
            contentType: 'application/json',
            url: '/MantenerClienteNatural/ObtenerDistritos',
            data: { "idprovincia": selectedItem },
            success: function (data) {
                comboDistrito.html('');
                for (var i = 0; i < data.length; i++) {
                    comboDistrito.append($('<option></option>').val(data[i].ID).html(data[i].Nombre));
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log('Error al cargar distritos.');

            }
        });
    });
});

$(document).ready(function () {
    $('#ComboSeleccionDepartamento option[value=51]').attr('selected', 'selected'); //Seleccionar LIMA por default
    $('#ComboSeleccionDepartamento').trigger('change'); //INICIALIZANDO COMBOS...
});
//---- Fin llenado de DropdownList Ubicaciones----


//### INICIO BUSQUEDA RAPIDA DE INTERESADOS ###
function busquedaRapidaInteresado(tipo) {
    $('#tablaBusquedaInteresado tbody').remove();
    $('#mensajeBusquedaInteresado').html('');
    var palabraclave = $('#txtBusquedaRapidaInteresado').val();
    if (palabraclave.length == 0) {
        $('#mensajeBusquedaInteresado').html('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span> Debe ingresar una palabra a buscar.</div>');
    } else {
        $.ajax({
            cache: false,
            type: 'GET',
            datatype: 'json',
            contentType: 'application/json',
            url: '/MantenerInteresado/BuscarInteresado',
            data: { "clave": palabraclave, "tipo": tipo },
            success: function (data) {
                if (data.length == 0) {
                    $('#mensajeBusquedaInteresado').html('<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="sr-only">Info:</span> No se encontraron interesados con la palabra clave "' + palabraclave + '"</div>');
                } else {
                    var func = "";
                    var fila = "";
                    if (tipo == 'Natural') {
                        for (var i = 0; i < data.length; i++) {
                            func = 'clickFilaTablaInteresado(' + data[i].ID + ', \'Natural\')'
                            fila = '<tr onclick="' + func + '"><td>' + data[i].ID + '</td><td>' + data[i].Nombre + '</td><td>' + data[i].DNI + '</td></tr>';
                            $('#tablaBusquedaInteresado').append(fila);
                        }
                    } else {
                        if (tipo == 'Empresa') {
                            for (var i = 0; i < data.length; i++) {
                                func = 'clickFilaTablaInteresado(' + data[i].ID + ', \'Empresa\')'
                                fila = '<tr onclick="' + func + '"><td>' + data[i].ID + '</td><td>' + data[i].Nombre + '</td><td>' + data[i].RUC + '</td></tr>';
                                $('#tablaBusquedaInteresado').append(fila);
                            }
                        } else {
                            alert("No hay tipo de cliente");
                        }
                    }

                    $('#mensajeBusquedaInteresado').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button><span class="glyphicon glyphicon-ok" aria-hidden="true"></span><span class="sr-only">Success:</span> Se encontraron ' + data.length + ' resultado(s) :</div>');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('#mensajeBusquedaInteresado').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span> Error al cargar información de interesados.</div>');
            }
        });
    }
}

//Agregar evento enter a caja de texto de busqueda rapida interesado
$("#txtBusquedaRapidaInteresado").keyup(function (event) {
    if (event.keyCode == 13) {
        $("#btnBusquedaRapidaInteresado").click();
    }
});

//### FIN BUSQUEDA RAPIDA DE INTERESADOS ###



//### INICIO BUSQUEDA RAPIDA DE EMPLEADOS ###
function busquedaRapidaEmpleado() {
    $('#tablaBusquedaEmpleado tbody').remove();
    $('#mensajeBusquedaEmpleado').html('');
    var palabraclave = $('#txtBusquedaRapidaEmpleado').val();
    if (palabraclave.length == 0) {
        $('#mensajeBusquedaEmpleado').html('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span> Debe ingresar una palabra a buscar.</div>');
    } else {
        $.ajax({
            cache: false,
            type: 'GET',
            datatype: 'json',
            contentType: 'application/json',
            url: '/MantenerEmpleado/BuscarEmpleado',
            data: { "clave": palabraclave },
            success: function (data) {
                if (data.length == 0) {
                    $('#mensajeBusquedaEmpleado').html('<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span class="sr-only">Info:</span> No se encontraron empleados con la palabra clave "' + palabraclave + '"</div>');
                } else {
                    var func = "";
                    var fila = "";
                    for (var i = 0; i < data.length; i++) {
                        func = 'clickFilaTablaEmpleado(' + data[i].ID + ')'
                        fila = '<tr onclick="' + func + '"><td>' + data[i].ID + '</td><td>' + data[i].Nombres + '</td><td>' + data[i].DNI + '</td></tr>';
                        $('#tablaBusquedaEmpleado').append(fila);
                    }
                    $('#mensajeBusquedaEmpleado').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button><span class="glyphicon glyphicon-ok" aria-hidden="true"></span><span class="sr-only">Success:</span> Se encontraron ' + data.length + ' resultado(s) :</div>');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('#mensajeBusquedaEmpleado').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span> Error al cargar información de empleados.</div>');
            }
        });
    }
}

//Agregar evento enter a caja de texto de busqueda rapida empleado
$("#txtBusquedaRapidaEmpleado").keyup(function (event) {
    if (event.keyCode == 13) {
        $("#btnBusquedaRapidaEmpleado").click();
    }
});

//### FIN BUSQUEDA RAPIDA DE EMPLEADOS ###



//----- Inicio Busqueda IC Combos ----
$(function () {
    $("#TipoPersona").change(function () {
        var valSeleccionado = $("#TipoPersona").val();
        var textSeleccionado = $("#TipoPersona option:selected").text();

        switch (textSeleccionado) {
            case "Natural":
                $("#TipoRol").prop("disabled", false);   //Habilita combo de roles
                $('#TipoRol').trigger('change');
                break;
            default:
                $("#TipoRol").prop("disabled", true);   //Deshabilita combo de roles
                $("#divRolEmpleado").hide();   //Ocultar div para rol de empleado
        }
    });
});

$(function () {
    $("#TipoRol").change(function () {
        var valSeleccionado = $("#TipoRol").val();
        var textSeleccionado = $("#TipoRol option:selected").text();
        if (textSeleccionado == "Empleado") {
            $("#divRolEmpleado").show();   //Mostrar div para rol de empleado
        } else {
            $("#divRolEmpleado").hide();   //Ocultar div para rol de empleado
        }
    });
});

$(document).ready(function () {
    $('#TipoPersona').trigger('change');
});
//----- Fin Busqueda IC Combos ----